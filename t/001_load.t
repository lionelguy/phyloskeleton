# t/001_load.t - check module loading

use Test::More tests => 10;
use strict;
use warnings;
use FindBin qw/$Bin/;
use lib "$Bin/../lib";

require_ok('PhyloSkeleton::Files');
require_ok('PhyloSkeleton::Taxa');
require_ok('PhyloSkeleton::TabFiles');
require_ok('PhyloSkeleton::MapFiles');
require_ok('PhyloSkeleton::Taxonomy');
require_ok('PhyloSkeleton::Sequences');

require_ok('PhyloSkeleton::GatherData');
require_ok('PhyloSkeleton::Annotate');
require_ok('PhyloSkeleton::AttributeHMMs');

require_ok('PhyloSkeleton');

