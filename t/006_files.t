# t/006_files.t - test file handling utils
use Test::More tests => 4;
use strict;
use warnings;
use FindBin qw/$Bin/;
use lib "$Bin/../lib";

use File::Copy;
use File::Temp qw/ tempfile tempdir /;

use PhyloSkeleton::Files qw/ downloadIfOlder /;

################################################################################
# downloadIfOlder
my ($fh, $filename) = tempfile();
# File doesn't exist
my $uri = "ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/README.txt";
my $res1 = downloadIfOlder('localFile' => $filename,
			   'remoteFile'=> $uri);
is($res1, 1, "downloadIfOlder - file not yet present");
# File exists but younger (by precaution get a way older age)
my $res2 = downloadIfOlder('localFile' => $filename,
			   'remoteFile'=> $uri,
			   'age'       => 999999999999);
is($res2, 0, "downloadIfOlder - file is newer");
# File exists, but is too old
sleep 2;
my $res3 = downloadIfOlder('localFile' => $filename,
			   'remoteFile'=> $uri,
			   'age'       => 0.1/(24*3600));
is($res3, 1, "downloadIfOlder - file is older");
# File exists, never refresh
my $res4 = downloadIfOlder('localFile' => $filename,
			   'remoteFile'=> $uri,
			   'age'       => -1);
is($res4, 0, "downloadIfOlder - no refresh");
