# t/005_taxonomy.t - Test parseSelectionLevels
use Test::More tests => 6;
use strict;
use warnings;
use FindBin qw/$Bin/;
use lib "$Bin/../lib";

use File::Temp qw/ tempfile tempdir /;

use PhyloSkeleton qw/ parseSelectionLevels /;


################################################################################
# test parseSelectionLevels
my $sl_ref1 = parseSelectionLevels('file' => "$Bin/../share/selLevels.tab");
isa_ok($sl_ref1, 'HASH', "parseSelectionLevels - file - result is hashref");
is($sl_ref1->{'byrow'}{'other'}{'selectionLevel'}, 'class', 
   "parseSelectionLevels - file - byrow");
is($sl_ref1->{'byrow'}{'Legionellaceae'}{'selectionLevel'}, 'species', 
   "parseSelectionLevels - file - byrow");

my $sl_ref2 = parseSelectionLevels('ingroupName'  => 'Legionellales',
				   'ingroupLevel' => 'order',
				   'ingroupSelectionLevel' => 'genus');
isa_ok($sl_ref2, 'HASH', "parseSelectionLevels - args - result is hashref");
is($sl_ref2->{'byrow'}{'Legionellales'}{'selectionLevel'}, 'genus', 
   "parseSelectionLevels - args - byrow");
is($sl_ref2->{'byrow'}{'other'}{'selectionLevel'}, 'family', 
   "parseSelectionLevels - args - byrow");
