# t/009 - test attributeHMM functions, except the main attributeHMMs
# which is tested in 008_gatherAnnotate.t
use Test::More tests => 8;
use strict;
use warnings;
use FindBin qw/$Bin/;
use lib "$Bin/../lib";

use File::Temp qw/ tempfile tempdir /;
use PhyloSkeleton::AttributeHMMs;

# Initiate temp folder
my $tmpfolder = tempdir();
print "TMPFOLDER: $tmpfolder\n";

# A few variables
my $hmm_file = "$Bin/../share/RP15.hmm";
my $proteomefile = "$Bin/../share/genomes/Ca_Hodgkinia_cicadicola_Dsem.faa";
my $r16S_fasta = 
   "$Bin/../share/genomes/Ca_Hodgkinia_cicadicola_Dsem.16S_rRNA.fasta";
my $r23S_fasta = 
   "$Bin/../share/genomes/Ca_Hodgkinia_cicadicola_Dsem.23S_rRNA.fasta";
my $shortName = "Ca_Hodgkinia_cicadicola_Dsem";
my $prefix = "HODCIC";
my $evalThreshold = 1e-6;

################################################################################
# Test _parseHMMs - just getting names
my $arr_ref = PhyloSkeleton::AttributeHMMs::_parseHMMs('file' => $hmm_file);
is(scalar(@$arr_ref), 15, "_parseHMMs - right size");
is($arr_ref->[7], 'Ribosomal_L18p', "_parseHMMs - returns right value");
################################################################################
# Test _runHmmsearch
my $res_file = PhyloSkeleton::AttributeHMMs::_runHmmsearch(
   $proteomefile, $hmm_file, $tmpfolder, $evalThreshold, $shortName);
is(-e $res_file, 1, "_runHmmsearch - returns file");

################################################################################
# Test _parseHMMalign
my %res = ();
my %seen_ids = ();
my ($res_ref, $nhmms) = PhyloSkeleton::AttributeHMMs::_parseHMMalign(
   $res_file, \%res, \%seen_ids, $shortName, $evalThreshold);
isa_ok($res_ref, 'HASH', "_parseHMMalign - returns a hashref");
is($nhmms, 14, "_parseHMMalign - returns the right number of HMMs");
is($res_ref->{'Ribosomal_L18p'}{$shortName}{'CP001226.1_81'}, 3.6e-07, 
   "_parseHMMalign - returns right values");

################################################################################
# Test _getrRNAs
my %rRNAs = ();
my $rRNAs_r = PhyloSkeleton::AttributeHMMs::_getrRNAs(
   $shortName, $r16S_fasta, $r23S_fasta, \%rRNAs);
isa_ok($rRNAs_r, 'HASH', "_getrRNAs - returns a hashref");
is($rRNAs_r->{$shortName}{'23S_rRNA'}, "Ca_Hodgkinia_cicadicola_Dsem.23S_rRNA", 
       "_getrRNAs returns right ids");

