# t/002_network_gatherData - test gatheringData from NCBI and JGI
use Test::More tests => 34;
use strict;
use warnings;
use FindBin qw/$Bin/;
use lib "$Bin/../../lib";

use File::Temp qw/ tempfile tempdir /;
use Cwd qw/ abs_path /;

use PhyloSkeleton qw/ setShortName /;
use PhyloSkeleton::TabFiles qw/ readTable writeTable /;
use PhyloSkeleton::Taxonomy qw/ addTaxonomy /;
use PhyloSkeleton::GatherData qw/initJGI getData/;
#use PhyloSkeleton::Annotate qw/ annotateCDS annotaterRNA annotate /;
use PhyloSkeleton::Annotate qw/ annotate /;
use PhyloSkeleton::AttributeHMMs qw/ attributeHMMs /;
use PhyloSkeleton::Sequences qw/ fastasFromMap /;

use Bio::SeqIO;


# Test jgi? In this case, a '.jgipassword' and '.jgilogin' files are needed 
# in the top folder
# Always skipped for automated tests
my $login_file = "$Bin/../../.jgilogin";
my $password_file = "$Bin/../../.jgipassword";
my $testjgi = (-e $login_file && -e $password_file);
# $testjgi = 0;

# Ouput folder
my $resultFolder = tempdir();
# For local testing and saving
#my $resultFolder = 'testRun';
my $genomicDataFolder = "$resultFolder/genomicData";
print STDERR "RESULTS in $resultFolder\n";

################################################################################
# Open file
my $gref = readTable('file' => "$Bin/../../share/selection_test.tab", 
		     'rowname' => 'assembly');

################################################################################
# Test getData from Genbank
#my %choice = ();
my %choice = ('GCA_001467045.1' => 1,
	      'GCA_001467615.1' => 1,
	      'GCA_000007765.2' => 1);
foreach my $id (@{ $gref->{'rows'} }){
  if ($choice{$id}){
    $gref->{'byrow'}{$id}{'excluded'} = '0_included';
  } else {
    $gref->{'byrow'}{$id}{'excluded'} = '9_test';
  }
}


my $n1;
($gref, $n1) = getData('gref'        => $gref,
		       'genomicData' => $genomicDataFolder);

my $existsFolder = opendir(my $dh, $genomicDataFolder);
is($existsFolder, 1, "getData - folder created");
my @dots = grep { /^[^.]/ && -d "$genomicDataFolder/$_" } readdir($dh);
closedir $dh;

my $pathToData = abs_path($genomicDataFolder . "/" . 
			  $gref->{'byrow'}{'GCA_000007765.2'}{'shortName'});
is($gref->{'byrow'}{'GCA_000007765.2'}{'genomeFile'}, 
   $pathToData . "/GCA_000007765.2_ASM776v2_genomic.fna", 
   "getData - genomefile is OK");
is($gref->{'byrow'}{'GCA_000007765.2'}{'proteomeFile'}, 
   $pathToData . "/GCA_000007765.2_ASM776v2_protein.faa", 
   "gatherData - proteomefile is OK");
is(scalar(@dots), 3, "getData - subfolders present");
is($n1, 3, "getData - number of genomes downloaded");

# Do again, should not redownload
my $n2;
($gref, $n2) = getData('gref'        => $gref,
		       'genomicData' => $genomicDataFolder);
is($n2, 0, "getData - genomes not re-downloaded");

################################################################################
# Test initJGI
# Read pw and login from files '.jgilogin' and '.jgipassword'
# These are NOT included, of course, and must be created by the user.
SKIP: {
  skip "JGI tests not requested", 4 unless $testjgi;

  open my $fh, '<',  $login_file or die "Cannot open $login_file";
  my $login = <$fh>;
  chomp $login;
  close $fh;

  open $fh, '<',  $password_file or die "Cannot open $password_file";
  my $password = <$fh>;
  chomp $password;
  close $fh;

  my $result = initJGI('login' => $login,
  		       'password' => $password,
  		       'cookiesfolder' => '.');
  is($result, 1, "initJGI - successful logon");
  
  ##############################################################################
  # Test retrieve data from JGI
  $choice{'2681812897'}++;
  $choice{'2599185259'}++;
  $choice{'2561511070'}++;
  foreach my $id (@{ $gref->{'rows'} }){
    if ($choice{$id}){
      $gref->{'byrow'}{$id}{'excluded'} = '0_included';
    } else {
      $gref->{'byrow'}{$id}{'excluded'} = '9_test';
    }
  }
  # Give these guys some short names
  $gref = setShortName('gref' => $gref);
  
  my $nj;
  ($gref, $nj) = getData('gref'        => $gref,
			 'genomicData' => $genomicDataFolder,
			 'jgilogin'    => $login,
			 'jgipassword' => $password,
			 'jgicookies'  => '.');
  is($nj, 3, "getData - genomes from JGI downloaded");
  my $shortName = $gref->{'byrow'}{'2561511070'}{'shortName'};
  is($gref->{'byrow'}{'2561511070'}{'genomeFile'}, 
     abs_path($genomicDataFolder) . "/$shortName/$shortName.fna", 
     "getData - genomefile from JGI is OK");
  is($gref->{'byrow'}{'2561511070'}{'proteomeFile'}, 
     abs_path($genomicDataFolder) . "/$shortName/$shortName.genes.faa", 
     "getData - proteomefile from JGI is OK");

  # # Write for future use
  # writeTable('table' => $gref,
  # 	     'file' => "$resultFolder/selection_w_data_test.tab");
}

################################################################################
# Test annotate
my %rmfaa = ('2599185259'      => 1,  # Legionella_quinlivanii_DSM_21
	     'GCA_000007765.2' => 1,  # Coxiella_burnetii_RSA_493
	     'GCA_001467615.1' => 1); # Legionella_erythra_SE_32A_C8

foreach my $id (@{ $gref->{'rows'} }){
  if ($rmfaa{$id}){
    $gref->{'byrow'}{$id}{'proteomeFile'} = '-';
  }
}

my $nannotated;
($gref, $nannotated) = annotate('gref'           => $gref,
				'annotate_rrnas' => 1);

# Test protein from Genbank
# Protein
my $shortName = $gref->{'byrow'}{'GCA_000007765.2'}{'shortName'};
my $proteomefile = "$genomicDataFolder/$shortName/$shortName.faa";
is($gref->{'byrow'}{'GCA_000007765.2'}{'proteomeFile'}, abs_path($proteomefile),
   "annotate - proteome file reannotated from Genbank OK");
is(-e $proteomefile, 1, 
   "annotate - proteome file reannotated from Genbank exists");
my $gbk_prot_in = Bio::SeqIO->new(-file => $proteomefile,
				  -format => 'fasta');
isa_ok($gbk_prot_in, 'Bio::SeqIO', 
       "annotate - proteomefile from Genbank is a fasta file");



# Test 23S
my $rRNA_23S_fasta = $gref->{'byrow'}{'GCA_000007765.2'}{'rRNA23S'};
my $gbk_rRNA_23S_in = Bio::SeqIO->new(-file => $rRNA_23S_fasta,
				      -format => 'fasta');
isa_ok($gbk_rRNA_23S_in, 'Bio::SeqIO', 
       "annotaterRNA - 23SrRNA from Genbank is a fasta file");

# Test protein and rRNA from JGI
SKIP: {
  skip "JGI tests not requested", 4 unless $testjgi;

  # Protein
  my $shortName = $gref->{'byrow'}{'2599185259'}{'shortName'};
  my $proteomefile = "$genomicDataFolder/$shortName/$shortName.faa";
  is($gref->{'byrow'}{'2599185259'}{'proteomeFile'}, abs_path($proteomefile), 
     "annotate - proteome file reannotated from JGI OK");
  is(-e $proteomefile, 1, 
     "annotate - proteome file reannotated from JGI exists");
  my $jgi_prot_in = Bio::SeqIO->new(-file => $proteomefile,
				    -format => 'fasta');
  isa_ok($jgi_prot_in, 'Bio::SeqIO', 
	 "annotate - proteomefile from JGI is a fasta file");



  # Test 23S
  my $rRNA_23S_fasta = $gref->{'byrow'}{'2599185259'}{'rRNA23S'};
  my $jgi_rRNA_23S_in = Bio::SeqIO->new(-file => $rRNA_23S_fasta,
					-format => 'fasta');
  isa_ok($jgi_rRNA_23S_in, 'Bio::SeqIO', 
	 "annotaterRNA - 23SrRNA from JGI is a fasta file");
}

# Number annotated?
is($nannotated, 2 + $testjgi, "annotate - right number of genomes annotated");
# Write for future use
# writeTable('table' => $gref,
# 	     'file'  => "$resultFolder/selection_w_annots_test.tab");


################################################################################
# Test attributeHMMs online
my $resultPrefix = 'testRun';

# Local testing
# $gref = readTable('file' => "$resultFolder/selection_w_annots_test.tab",
# 		    'rowname' => 'assembly');
my $nsearched;
# Search with very low threshold to create gaps
($gref, $nsearched) = attributeHMMs('gref' => $gref,
				    'prefix' => $resultPrefix,
				    'output_folder' => $resultFolder,
				    'hmms' => "$Bin/../../share/RP15.hmm",
				    'annotate_rrnas' => 1,
				    'all_single_gene_maps' => 1,
				    'evalue_threshold' => 1e-18,
				    'completeness_threshold' => 0.95,
				    'cpus' => 3);
isa_ok($gref, 'HASH', "attributeHMMs - result is a hashref");
is($nsearched, 3 + 3*$testjgi, 
   "attributeHMMs - searched the right number of proteomes");
is(-d "$resultFolder/$resultPrefix.hmm_aligns", 1, 
   "attributeHMMs - created a hmm_aligns folder");
is(-d "$resultFolder/$resultPrefix.single_marker_maps", 1, 
   "attributeHMMs - created a single_marker_maps folder");
is(-e "$resultFolder/$resultPrefix.map", 1, 
   "attributeHMMs - created the main map");
is(-e "$resultFolder/$resultPrefix.n.matrix", 1, 
   "attributeHMMs - created main matrix");
is(-e "$resultFolder/$resultPrefix.eval.matrix", 1, 
   "attributeHMMs - created evalue matrix");
is($gref->{'byrow'}{'GCA_000007765.2'}{'completeness'}, 0.933333333333333, 
   "attributeHMMs - attributed completeness values");
is($gref->{'byrow'}{'GCA_001467045.1'}{'excluded'}, '0_included', 
   "attributeHMMs - kept included right");
is($gref->{'byrow'}{'GCA_000007765.2'}{'excluded'}, '3_lowCompleteness', 
   "attributeHMMs - excluded right");


# Write for future use
writeTable('table' => $gref,
	   'file'  => "$resultFolder/selection_w_markers_test.tab");

# Search with very high threshold to create duplicates
($gref, $nsearched) = attributeHMMs('gref' => $gref,
				    'prefix' => "$resultPrefix.highThreshold",
				    'output_folder' => $resultFolder,
				    'hmms' => "$Bin/../../share/RP15.hmm",
				    'annotate_rrnas' => 1,
				    'all_single_gene_maps' => 0,
				    'evalue_threshold' => 1e-2,
				    'completeness_threshold' => 0.7,
				    'cpus' => 3);
is(-e "$resultFolder/$resultPrefix.highThreshold.eval.matrix", 1, 
   "attributeHMMs - created evalue matrix nr 2");
is(-e "$resultFolder/$resultPrefix.single_marker_maps/" . 
   "Ribosomal_S8.map", 1, 
   "attributeHMMs - created a single file in single folder");

################################################################################
# Test fastasFromMap

# Local testing
# $gref = readTable('file' => "$resultFolder/selection_w_annots_test.tab",
# 		    'rowname' => 'assembly');
my $nfiles = fastasFromMap('table'         => $gref,
			   'map' => 
			   "$resultFolder/$resultPrefix.highThreshold.map",
			   'fasta_folder'  => "$resultFolder/fasta",
			   'include_rrnas' => 1);
is($nfiles, 17, "fastaFromMap - created right number of files");
is(-d "$resultFolder/fasta", 1, "fastasFromMap - created fasta folder");
is(-e "$resultFolder/fasta/16S_rRNA.fasta", 1, 
   "fastasFromMap - created fasta file for 16S rRNA");

