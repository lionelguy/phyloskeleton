# t/001_network_taxonomy.t - Test downloading and parsing the taxonomy
use Test::More tests => 4;
use strict;
use warnings;
use FindBin qw/$Bin/;
use lib "$Bin/../../lib";

use File::Temp qw/ tempfile tempdir /;
use PhyloSkeleton::Taxonomy qw/ downloadTaxdump parseTaxonomy /;


################################################################################
# test downloadTaxdump
my $tempdir = tempdir();
print STDERR "TMPDIR: $tempdir\n";
my $ok = downloadTaxdump('localFolder' => $tempdir);
is($ok, 1, "downloadTaxDump - downloaded and extracted");
my $two = downloadTaxdump('localFolder'        => $tempdir, 
			  'refreshIfOlderThan' => 2);
is($two, 2, "downloadTaxDump - not refreshed\n");

################################################################################
# test parseTaxo
open my $fh_taxids, '<', "$Bin/../../share/taxids" 
  or die "Cannot open $Bin/taxids: $!\n";
my @taxids = <$fh_taxids>;
chomp @taxids;
my @ranks = qw/ superkingdom phylum class order family genus species/;
my $tref = parseTaxonomy('nodes' => "$tempdir/nodes.dmp",
			 'names' => "$tempdir/names.dmp",
			 'ranks' => \@ranks,
			 'taxids'=> \@taxids);
# ## For local testing and saving
# my $tref = parseTaxonomy('nodes' => "../data/nodes.dmp",
# 			   'names' => "../data/names.dmp",
# 			   'ranks' => \@ranks,
# 			   'taxids'=> \@taxids);
# PhyloSkeleton::TabFiles::writeTable('table' => $tref,
# 				      'file'  => 'taxids.tab');
isa_ok($tref, 'HASH', "parseTaxonomy - Result is a hashref");
is($tref->{'byrow'}{777}{'genus'}, 'Coxiella', "parseTaxonomy - Value");


