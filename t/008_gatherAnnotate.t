# t/008_gatherData - test gatheringData from NCBI and JGI
use Test::More tests => 10;
use strict;
use warnings;
use FindBin qw/$Bin/;
use lib "$Bin/../lib";

use File::Temp qw/ tempfile tempdir /;
use Cwd qw/ abs_path /;

use PhyloSkeleton::Annotate qw/ annotateCDS annotaterRNA  /;

use Bio::SeqIO;


################################################################################
# Test _generatePrefix
my %prefixes = ();
my $name = 'Candidatus_Strep_pneu_TEST01';
my $species = 'Candidatus Streptococcus pneumoniae';
my $genus = 'Streptococcus';

# Test normal
my $prefix = PhyloSkeleton::Annotate::_generatePrefix(\%prefixes, $name, 
						      $species, $genus);
is($prefix, 'StrPne', "_generatePrefix - normal use");

# Test already seen
my $prefix2 = PhyloSkeleton::Annotate::_generatePrefix(\%prefixes, $name, 
						       $species, $genus);
is($prefix2, 'StrPnu', "_generatePrefix - second use");

# Test with old prefix, not existing
my $prefix3 = PhyloSkeleton::Annotate::_generatePrefix(\%prefixes, $name, 
						       $species, $genus, 
						       'StrPno');
is($prefix3, 'StrPno', "_generatePrefix - use existing");

# Test with old prefix, existing
my $prefix4 = PhyloSkeleton::Annotate::_generatePrefix(\%prefixes, $name, 
						       $species, $genus, 
						       'StrPno');
is($prefix4, 'StrPnm', "_generatePrefix - use existing, already present");


################################################################################
# Test annotateCDS
my $fastaFile = annotateCDS('genomefile' =>"$Bin/../share/genomes/CP001226.fna",
			    'prefix'     => 'HODCIC',
			    'shortName'  => 'Ca_Hodgkinia_cicadicola_Dsem',
			    'gcode'      => 4,
			    'kingdom'    => 'Bacteria',
			    'annotator'  => 'prodigal');
is(-e $fastaFile, 1, "annotateCDS - fastaFile exists");
my $fasta_in = Bio::SeqIO->new(-file => $fastaFile,
			       -format => 'fasta');
isa_ok($fasta_in, 'Bio::SeqIO', "annotateCDS - file is a fasta file");
################################################################################
# Test annotaterRNA
my ($rRNA_16S_fasta, $rRNA_23S_fasta) = 
  annotaterRNA('genomefile' => "$Bin/../share/genomes/CP001226.fna",
	       'prefix'     => 'HODCIC',
	       'shortName'  => 'Ca_Hodgkinia_cicadicola_Dsem',
	       'kingdom'    => 'Bacteria');
is(-e $rRNA_16S_fasta, 1, "annotaterRNA - 16SrRNA file exists");
my $rRNA_16S_in = Bio::SeqIO->new(-file => $rRNA_16S_fasta,
				  -format => 'fasta');
isa_ok($rRNA_16S_in, 'Bio::SeqIO', "annotaterRNA - 16SrRNA is a fasta file");
is(-e $rRNA_23S_fasta, 1, "annotaterRNA - 23SrRNA file exists");
my $rRNA_23S_in = Bio::SeqIO->new(-file => $rRNA_23S_fasta,
				  -format => 'fasta');
isa_ok($rRNA_23S_in, 'Bio::SeqIO', "annotaterRNA - 23SrRNA is a fasta file");


