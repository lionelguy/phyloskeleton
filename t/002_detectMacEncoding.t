# t/002_detectMacEncoding.t - check changing encoding
use Test::More tests => 7;
use strict;
use warnings;
use FindBin qw/$Bin/;
use lib "$Bin/../lib";

use File::Temp qw/ tempfile tempdir /;

use PhyloSkeleton::TabFiles qw/ detectMacEncoding readTable /;

# Print string to tempfile for later reading
my ($fh, $filename) = tempfile();
my $macstr = "a\tb\tc\r2\t5\t7\r8\t3\t3\r";
(my $unixstr = $macstr) =~ tr/\r/\n/;
print $fh $macstr;
close $fh;

################################################################################
# test 1-2 detectMacEncoding - no replace
my $res1 = detectMacEncoding('file'    => $filename,
                             'replace' => 0);
ok($res1, "no replace: return value");
# read
open my $fh2, '<', $filename;
my $teststr = <$fh2>;
is($teststr, $macstr, "no replace: result string"); 

################################################################################
# test 3-6 detectMacEncoding - with replace and backup - print again
($fh, $filename) = tempfile();
print $fh $macstr;
close $fh;

my $ext = '.bak';
my $res2 = detectMacEncoding('file'    => $filename,
                             'replace' => 1, 
                             'backup'  => $ext);

# return value?
ok($res2, "replace: return value");

# read, replaced?
open $fh2, '<', $filename;
my @teststr2 = <$fh2>;
is(join("", @teststr2), $unixstr, "replace: result string"); 

# backup file written?
ok(-s $filename . $ext, "replace: backup file present");

# read backup, not replaced?
open $fh2, '<', $filename . $ext;
$teststr = <$fh2>;
is($teststr, $macstr, "replace: backup string not replaced"); 

# test readFile with mac encoded file
my ($fh2, $filename2) = tempfile();
print $fh2 $macstr;
close $fh2;
my $table = readTable('file' => $filename2);
is($table->{'nrow'}, 2, "readTable - converted mac-encoded file");

