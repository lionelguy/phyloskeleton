# t/003_readTable.t - test read/write tables
use Test::More tests => 73;
use strict;
use warnings;
use FindBin qw/$Bin/;
use lib "$Bin/../lib";

use File::Temp qw/ tempfile tempdir /;

use PhyloSkeleton::TabFiles qw/ newTable readTable writeTable mergeTables csv2tsv /;

# Initiate tables, save 
my $tbl_str_1 = "id\ta\tb\tc\nx\t2\t5\t7\ny\t8\t3\t3\nz\t1\t0\t0\n";
my ($fh, $filename) = tempfile();
print $fh $tbl_str_1;
close $fh;

################################################################################
# readTable- Just defaults
my $res1 = readTable('file' => $filename);
isa_ok($res1, 'HASH', "Defaults - is a hash");
is($res1->{'byrow'}{1}{'a'}, 2, "Defaults - byrow");
is(${ $res1->{'bycol'}{'a'}{1} }, 2, "Defaults - bycol");
isa_ok($res1->{'rows'}, 'ARRAY', "Defaults - rows is an array");
isa_ok($res1->{'columns'}, 'ARRAY', "Defaults - cols is an array");
is($res1->{'nrow'}, 3, "Defaults - nrow");
is($res1->{'ncol'}, 4, "Defaults - ncol");
is($res1->{'rowname'}, undef, "Defaults - rowname not set");
is($res1->{'rowname_idx'}, undef, "Defaults - rowname_idx not set");

################################################################################
# readTable - rowname
my $res2 = readTable('file'    => $filename, 
                     'rowname' => 'id');
isa_ok($res2, 'HASH', "Rowname - is a hash");
is($res2->{'byrow'}{'x'}{'a'}, 2, "Rowname - byrow");
is(${ $res2->{'bycol'}{'a'}{'x'} },  2, "Rowname - bycol");
isa_ok($res2->{'rows'}, 'ARRAY', "Rowname - rows is an array");
isa_ok($res2->{'columns'}, 'ARRAY', "Rowname - cols is an array");
is($res2->{'nrow'}, 3, "Rowname - nrow");
is($res2->{'ncol'}, 4, "Rowname - ncol");
is($res2->{'rowname'}, 'id' , "Rowname - rowname");
is($res2->{'rowname_idx'}, 0, "Rowname - rowname_idx");

################################################################################
# readTable - modifying
$res2->{'byrow'}{'x'}{'a'} = 7;
is(${ $res2->{'bycol'}{'a'}{'x'} }, 7, "Modifying, rows2cols");
${ $res2->{'bycol'}{'a'}{'x'} } = 13;
is($res2->{'byrow'}{'x'}{'a'}, 13, "Modifying, cols2rows");

################################################################################
# readTable - restricting columns
my @cols = qw/ id b c /;
my $res3 = readTable('file'    => $filename,
                     'rowname' => 'id',
                     'columns' => \@cols);
is(@{ $res3->{'columns'} }, @cols, "Column selection");
is($res3->{'byrow'}{'x'}{'a'}, undef, "Column selection - undef if not select");

################################################################################
# readTable - modifying colnames
my %map = ( 'a' => 'alpha',
	    'c' => 'gamma');
my $res3a = readTable('file'    => $filename,
		      'rowname' => 'id',
		      'columnNameMap' => \%map);
#print join("\t", @{ $res3a->{'columns'}}) . "\n";
is((@{ $res3a->{'columns'} })[3], 'gamma', "Column renaming - colnames");
is($res3a->{'byrow'}{'x'}{'alpha'}, 2, "Column renaming - values");


################################################################################
# writeTable - writing
my ($fh_w, $filename_w) = tempfile();
my $res4 = writeTable('table' => $res2,
                      'file'  => $filename_w);
is($res4, 1, "Write table - return value");
# Re-read the written table
my $res5 = readTable('file'    => $filename_w, 
                     'rowname' => 'id');
isa_ok($res5, 'HASH', "Write table - written/read table is a hash");
is($res2->{'byrow'}{'z'}{'c'}, $res5->{'byrow'}{'z'}{'c'}, 
   "Write table - same result when after writing and reading");

################################################################################
# addColumn - adding column to a table
my %col = ( 'x' =>  7, 'y' => 3, 'z' => 9 );
my $res8 = PhyloSkeleton::TabFiles::addColumn('table' => $res2, 'col' => \%col, 
					      'colname' => 'd');
isa_ok($res8, 'HASH', "AddColumn - is a hash");
is($res8->{'byrow'}{'z'}{'d'}, 9, "AddColumn - byrow");
is(${ $res8->{'bycol'}{'d'}{'z'} },  9, "AddColumn - bycol");
is(${ $res8->{'columns'} }[4], 'd', "AddColumn - columns");
is($res8->{'ncol'}, 5, "AddColumn - ncol");

################################################################################
# removeColumn - removing column from a table
my $res9 = PhyloSkeleton::TabFiles::removeColumn('table' => $res8, 
						 'colname' => 'd');
isa_ok($res9, 'HASH', "RemoveColumn - is a hash");
is($res9->{'byrow'}{'z'}{'d'}, undef, "RemoveColumn - byrow");
is($res9->{'bycol'}{'d'}, undef, "RemoveColumn - bycol");
is(${ $res9->{'columns'} }[4], undef, "RemoveColumn - columns");
is($res9->{'ncol'}, 4, "RemoveColumn - ncol");

################################################################################
# addRow - adding row to a table
my %row = ( 'id' =>  'w', 'a' => 11, 'b' => 12, 'c' => 13 );
my $res10 = PhyloSkeleton::TabFiles::addRow('table' => $res2, 'row' => \%row);
isa_ok($res10, 'HASH', "AddRow - is a hash");
is($res10->{'byrow'}{'w'}{'c'}, 13, "AddRow - byrow");
is(${ $res10->{'bycol'}{'c'}{'w'} },  13, "AddRow - bycol");
is(${ $res10->{'rows'} }[3], 'w', "AddRow - rows");
is($res10->{'nrow'}, 4, "AddRow - nrow");

################################################################################
# getColumn
my $col = PhyloSkeleton::TabFiles::getColumn('table' => $res2,
					     'column'=> 'c');
is(scalar @$col, 4, "GetColumn - ncol");
is($col->[3], 13, "GetColumn - values");

################################################################################
# removeRow - removing row to a table
my $res11 = PhyloSkeleton::TabFiles::removeRow('table' => $res2, 
					       'rowname' => 'z');
isa_ok($res10, 'HASH', "removeRow - is a hash");
is($res10->{'byrow'}{'z'}, undef, "removeRow - byrow");
is(${ $res10->{'bycol'}{'c'}{'z'} },  undef, "removeRow - bycol");
is(${ $res10->{'rows'} }[3], undef, "removeRow - rows");
is($res10->{'nrow'}, 3, "removeRow - nrow");

################################################################################
# mergeTables - merging tables one
#my $tbl_str_1 = "id\ta\tb\tc\nx\t2\t5\t7\ny\t8\t3\t3\nz\t1\t0\t0\n";
# tbl1
my ($fh1, $filename1) = tempfile();
print $fh1 $tbl_str_1;
close $fh1;
my $tbl1 = readTable('file'    => $filename1,
                     'rowname' => 'id');
# tbl2
my $tbl_str_2 = "id\ta\td\nj\t5\t8\nk\t8\t3\nl\t1\t0\n";
my ($fh2, $filename2) = tempfile();
print $fh2 $tbl_str_2;
close $fh2;
my $tbl2 = readTable('file'    => $filename2,
                     'rowname' => 'id');
# tbl3
my $tbl_str_3 = "f\tid\ta\n2\tm\t2\n3\tn\t3";
my ($fh3, $filename3) = tempfile();
print $fh3 $tbl_str_3;
close $fh3;
my $tbl3 = readTable('file'    => $filename3,
                     'rowname' => 'id');

# Union
my $merged_tref1 = mergeTables('tables'  => [ $tbl1, $tbl2, $tbl3 ],
			      'columns' => 'union');
isa_ok($merged_tref1, 'HASH', "mergeTables - union - result is a hashref");
is($merged_tref1->{'ncol'}, 6, "mergeTables - union - ncol");
is($merged_tref1->{'nrow'}, 8, "mergeTables - union - nrow");
is($merged_tref1->{'byrow'}{'m'}{'f'}, 2, 
   "mergeTables - union - values by row");
is($merged_tref1->{'byrow'}{'l'}{'d'}, 0, 
   "mergeTables - union - values by row");
is($ {$merged_tref1->{'bycol'}{'d'}{'l'}}, 0, 
   "mergeTables - union - values by col");

# Intersection
my $merged_tref2 = mergeTables('tables'  => [ $tbl1, $tbl2, $tbl3 ],
			      'columns' => 'intersection',
			      'rowname' => 'id');
isa_ok($merged_tref2, 'HASH', 
       "mergeTables - intersection - result is a hashref");
is($merged_tref2->{'ncol'}, 2, "mergeTables - intersection - ncol");
is($merged_tref2->{'nrow'}, 8, "mergeTables - intersection - nrow");
is($merged_tref2->{'byrow'}{'m'}{'a'}, 2, 
   "mergeTables - intersection - values by row");
is($ {$merged_tref2->{'bycol'}{'a'}{'m'}}, 2, 
   "mergeTables - intersection - values by col");

# Reference
my $merged_tref3 = mergeTables('tables'  => [ $tbl1, $tbl2, $tbl3 ],
			      'columns' => 'reference');
isa_ok($merged_tref3, 'HASH', 
       "mergeTables - reference - result is a hashref");
is($merged_tref3->{'ncol'}, 4, "mergeTables - reference - ncol");
is($merged_tref3->{'nrow'}, 8, "mergeTables - reference - nrow");
is($merged_tref3->{'byrow'}{'m'}{'a'}, 2, 
   "mergeTables - reference - values by row");
is($ {$merged_tref3->{'bycol'}{'a'}{'m'}}, 2, 
   "mergeTables - reference - values by col");

################################################################################
# newTable - create a void table
#my $tbl_str_3 = "f\tid\ta\n2\tm\t2\n3\tn\t3";
my @col_new = qw/ f id a /;
my $new_tref = newTable('columns' => \@col_new, 'rowname' => 'id');
my %row1 = ('id' => 'm', 'f' => 2, 'a' => 2);
my %row2 = ('id' => 'n', 'f' => 3, 'a' => 3);
$new_tref = PhyloSkeleton::TabFiles::addRow('table' => $new_tref, 
					    'row' => \%row1);
$new_tref = PhyloSkeleton::TabFiles::addRow('table' => $new_tref, 
					    'row' => \%row2);
isa_ok($new_tref, 'HASH', "newTable - result is a hash");
is($new_tref->{'ncol'}, 3, "newTable - ncol");
is($new_tref->{'nrow'}, 2, "newTable - nrow");
is($new_tref->{'byrow'}{'n'}{'f'}, 3, "newTable - values");
is_deeply($new_tref, $tbl3, "newTable - is deeply as from readTable");

################################################################################
# csv2tbl - converting csv to tsv
(my $csv_str = $tbl_str_1) =~ s/\t/,/g;
my ($fh_csv, $filename_csv) = tempfile();
my ($fh_tsv, $filename_tsv) = tempfile();
print $fh_csv $csv_str;
close $fh_csv;
my $ok = csv2tsv('input' => $filename_csv,
		 'output'=> $filename_tsv);
is($ok, 1, "csv2tsv - Return value");
my $res_tsv = readTable('file'    => $filename_tsv,
			'rowname' => 'id');
my $res_csv = readTable('file'    => $filename_csv,
			'rowname' => 'id',
			'sep'     => ',');
isa_ok($res_tsv, 'HASH', "csv2tsv - written/read table is a hash");
is($res_tsv->{'byrow'}{'z'}{'c'}, $res_csv->{'byrow'}{'z'}{'c'}, 
   "csv2tbl - same result before and after converting");
