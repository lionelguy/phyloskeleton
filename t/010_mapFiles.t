# t/010_mapFiles.t - Test reading map files
use Test::More tests => 6;
use strict;
use warnings;
use FindBin qw/$Bin/;
use lib "$Bin/../lib";

use PhyloSkeleton::MapFiles qw/ readMap /;

################################################################################
# Open file
my ($map, $seqs) = readMap('file' => "$Bin/../share/test.map");

# map
isa_ok($map, 'HASH', "readMap - map is a hashref");
is($map->{'Ribosomal_S8'}{'Fluoribacter_bozemanae_WIGA'}{'KTC66695.1'}{'evalue'}, 0.0029, "readMap - map values");
is(scalar(keys($map)), 17, "readMap - n markers");

# seqs
isa_ok($seqs, 'HASH', "readMap - seqs is a hashref");
is($seqs->{'KTC66695.1'}, 1, "readMap - seqs values");
is(scalar(keys($seqs)), 68, "readMap - n seqs");