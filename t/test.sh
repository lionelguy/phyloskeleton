#!/bin/bash
################################################################################
# Testing phyloSkeleton scripts
################################################################################
# This follows more or less the examples developed in the Tutorial of the 
# README.md file

# Run from phyloSkeleton main folder. 
START_TIME=$SECONDS
echo "TEST: phyloSkeleton: test run. This requires access to internet."
mkdir genomicData taxonomy

#===============================================================================
# phyloSkeleton
#===============================================================================
### Example 0
echo "==================================================================="
echo "Example 0: cyanos"
echo "==================================================================="
# Quick example (cyanos)
perl ../phyloSkeleton.pl \
    --output-folder example0 \
    --result-prefix cyanos \
    --ncbi-selection ../share/cyanos.csv \
    --ingroup Cyanobacteria\
    --ingroup-level phylum \
    --ingroup-selection-level family \
    --best-match-only

### Example 1
echo "==================================================================="
echo "Example 1: NCBI data, user-defined selection table"
echo "==================================================================="
# First oonly select assemblies from NCBI, no data gathering or annotation
perl ../phyloSkeleton.pl \
    --output-folder example1 \
    --result-prefix legio1 \
    --ncbi-selection ../share/legionellales.csv \
    --ncbi-all-projects genomicData/assembly_summary_genbank.txt \
    --taxonomy-path taxonomy \
    --taxonomy-table example1/taxids.tab \
    --levels ../share/selLevels.tab \
    --donotgather
# Exclude species name starting with 'w'
perl -pe 's/(.*)(0_included)(.*Legionella w.*)/${1}9_user$3/' example1/legio1.selection.tab > example1/legio1_edited.selection.tab
# Rerun the pipeline with the modified selection table
perl ../phyloSkeleton.pl \
    --output-folder example1 \
    --result-prefix legio2 \
    --selection-tab example1/legio1_edited.selection.tab \
    --genomic-data-path genomicData \
    --taxonomy-path taxonomy \
    --taxonomy-table example1/taxids.tab \
    --best-match-only \
    --cpus 3 

	
### Example 2
echo "==================================================================="
echo "Example 2: combine with JGI data"
echo "==================================================================="
# Gather data, annotate and search for markers
JGILOGIN=`cat ../.jgilogin`
JGIPW=`cat ../.jgipassword`
perl ../phyloSkeleton.pl \
    --output-folder example2 \
    --result-prefix legio1 \
    --genomic-data-path genomicData \
    --taxonomy-path taxonomy \
    --taxonomy-table example2/taxids.tab \
    --ncbi-selection ../share/legionellales.csv \
    --ncbi-all-projects genomicData/assembly_summary_genbank.txt \
    --jgi-selection ../share/taxontablejgi.xls \
    --jgi-login $JGILOGIN \
    --jgi-password $JGIPW \
    --levels ../share/selLevels.tab \
    --annotate-rrnas \
    --cpus 3
# There was a warning that one marker had two copies, let's investigate
perl -lane 'print if ($F[4] > 1)' example2/legio1.map
# Looks like the gene has been split. Let's remove both copies in the map, to 
# be on the safe side
grep -v -e "Legionella_tunisiensis.*Ribosomal_L14" legio1.map > legio1.edited.map
# Rerun only the last part, with the updated map
perl ../phyloSkeleton.pl \
    --output-folder example2 \
    --result-prefix legio2 \
    --selection-tab example2/legio1.selection.tab \
    --genomic-data-path genomicData \
    --marker-map example2/legio1.edited.map \
    --annotate-rrnas

### Example 3
echo "==================================================================="
echo "Example 3: user data only"
echo "==================================================================="
# Now with user-generated selection file only and a different set of markers
perl ../phyloSkeleton.pl \
    --output-folder example3 \
    --result-prefix userData \
    --taxonomy-path taxonomy \
    --selection-tab ../share/userGenomes.tab \
    --jgi-login $JGILOGIN \
    --jgi-password $JGIPW \
    --hmms Bact109 \
    --completeness 0.3

### Example 4
echo "==================================================================="
echo "Example 4: mixing all"
echo "==================================================================="
# With user-generated tab AND with NCBI AND JGI data. Use --best-match only
# to run through the whole pipeline
perl ../phyloSkeleton.pl \
    --output-folder example4 \
    --result-prefix legionellales \
    --genomic-data-path genomicData \
    --selection-tab ../share/userGenomes.tab \
    --genomic-data-path genomicData \
    --taxonomy-path taxonomy \
    --taxonomy-table example4/taxids.tab \
    --ncbi-selection ../share/legionellales.csv \
    --ncbi-all-projects genomicData/assembly_summary_genbank.txt \
    --jgi-selection ../share/taxontablejgi.xls \
    --jgi-login $JGILOGIN \
    --jgi-password $JGIPW \
    --levels ../share/selLevels.tab \
    --annotate-rrnas \
    --best-match-only \
    --cpus 3

#===============================================================================
# Finished
#===============================================================================
ELAPSED_TIME=$(($SECONDS - $START_TIME))
echo "TEST: Done in $(($ELAPSED_TIME/60)) min $(($ELAPSED_TIME%60)) sec" 
