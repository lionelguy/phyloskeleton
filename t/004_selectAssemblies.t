# t/004_selectAssemblies.t - test selecting assemblies, parsing the input
# files
use Test::More tests => 45;
use strict;
use warnings;
use FindBin qw/$Bin/;
use lib "$Bin/../lib";

use File::Temp qw/ tempfile tempdir /;
use Storable 'dclone';

use PhyloSkeleton qw/ parseNCBIlproks parseNCBIlist parseJGIlist parseSelectionLevels selectAssemblies setShortName sortTable/;
use PhyloSkeleton::Taxonomy qw/ addTaxonomy hasTaxonomy /;
use PhyloSkeleton::TabFiles qw/ writeTable addColumn removeColumn /; 

################################################################################
# test parseNCBIlproks
my $res1 = parseNCBIlproks('file' => "$Bin/../share/legionellales.csv");
isa_ok($res1, 'HASH', "parseNCBIlproks - is a hash");
is($res1->{'byrow'}{'GCA_000008485.1'}{'organism_groups'}, "Bacteria;Proteobacteria;Gammaproteobacteria", 
   "parseNCBIlproks - byrow");
is($res1->{'nrow'}, 788, "parseNCBIlproks - nrow");
is($res1->{'ncol'}, 8, "parseNCBIlproks - ncol");

################################################################################
# test parseNCBIlist
my $res2 = parseNCBIlist(
  'gref' => $res1, 
  'file' => "$Bin/../share/assembly_summary_genbank_test.txt"
);
isa_ok($res2, 'HASH', "parseNCBIlist - is a hash");
is($res2->{'byrow'}{'GCA_000008485.1'}{'assembly_level'}, 
   "Complete Genome", "parseNCBIlist - byrow");
is($res2->{'nrow'}, 788, "parseNCBIlproks - nrow");
is($res2->{'ncol'}, 16, "parseNCBIlproks - ncol");

################################################################################
# test parseJGIlist
my $gref_jgi = parseJGIlist('file' => "$Bin/../share/taxontablejgi.xls");
isa_ok($gref_jgi, 'HASH', "parseJGIlist - is a hash");
is($gref_jgi->{'byrow'}{'2558309002'}{'source'}, 
  "jgi", "parseJGIlist - source");
is($gref_jgi->{'byrow'}{'2558309002'}{'name'}, 
   "Legionella sainthelensi ATCC 35248", "parseJGIlist - byrow");   
is($gref_jgi->{'nrow'}, 14, "parseJGIlist - nrow");
is($gref_jgi->{'ncol'}, 14, "parseJGIlist - ncol");

################################################################################
# test mergeTables on assemblies
my $gref_merged = PhyloSkeleton::TabFiles::mergeTables(
    'tables'  => [ $res2, $gref_jgi ],
    'columns' => 'union');
isa_ok($gref_merged, 'HASH', "mergeTables - is a hash"); 
is($gref_merged->{'nrow'}, 802, "mergeTables - nrow");
is($gref_merged->{'ncol'}, 18, "mergeTables - ncol");

################################################################################
# test addTaxonomy
my $tref = PhyloSkeleton::TabFiles::readTable(
   'file' => "$Bin/../share/taxids.tab", 
   'rowname' => 'taxid'
);
isa_ok($tref, 'HASH', "addTaxonomy - tref a hashref");
is($tref->{'byrow'}{777}{'genus'}, 'Coxiella', "addTaxonomy - reading tref OK");
my $gref_with_taxo = addTaxonomy('gref' => $gref_merged,
				 'tref' => $tref);
isa_ok($gref_with_taxo, 'HASH', "addTaxonomy - result a hashref");
is($gref_with_taxo->{'nrow'}, 736, "addTaxonomy - right number of rows");
is($gref_with_taxo->{'ncol'}, 27, "addTaxonomy - right number of columns");
is($gref_with_taxo->{'byrow'}{'GCA_000168295.1'}{'genus'}, 'Rickettsiella', 
   "addTaxonomy - ranks, here genus, works");
is($gref_with_taxo->{'byrow'}{'GCA_000168295.1'}{'gcode'}, 11, 
   "addTaxonomy - genetic code");

################################################################################
# test hasTaxonomy

# Should not return 1 if taxonomy was not added
is(hasTaxonomy('table' => $res2, 'id' => 'GCA_000168295.1', 'any' => 1), 0,
   "hasTaxonomy - on gref where no taxo was set");
# Should  return 1 if taxonomy was added
is(hasTaxonomy('table' => $gref_with_taxo, 'id' => 'GCA_000168295.1', 
	       'any' => 1), 
   1, "hasTaxonomy - on gref where taxo was set");



################################################################################
# test parseSelectionLevels
my $sl_ref1 = parseSelectionLevels('file' => "$Bin/../share/selLevels.tab");
isa_ok($sl_ref1, 'HASH', "parseSelectionLevels - file - result is hashref");
is($sl_ref1->{'byrow'}{'other'}{'selectionLevel'}, 'class', 
   "parseSelectionLevels - file - byrow");
is($sl_ref1->{'byrow'}{'Legionellaceae'}{'selectionLevel'}, 'species', 
   "parseSelectionLevels - file - byrow");

my $sl_ref2 = parseSelectionLevels('ingroupName'  => 'Legionellales',
				   'ingroupLevel' => 'order',
				   'ingroupSelectionLevel' => 'genus');
isa_ok($sl_ref2, 'HASH', "parseSelectionLevels - args - result is hashref");
is($sl_ref2->{'byrow'}{'Legionellales'}{'selectionLevel'}, 'genus', 
   "parseSelectionLevels - args - byrow");
is($sl_ref2->{'byrow'}{'other'}{'selectionLevel'}, 'family', 
   "parseSelectionLevels - args - byrow");

################################################################################
# test selectAssemblies
my $gref = selectAssemblies('genomes' => $gref_with_taxo,
			  'selectionLevels' => $sl_ref1,
			  'taxonomy' => $tref);
isa_ok($gref, 'HASH', "selectAssemblies - result is hashref");
is($gref->{'byrow'}{'GCA_000953655.1'}{'selectionGroup'}, 'Legionellaceae', 
  "selectAssemblies - values in selectionGroup");

################################################################################
# test setShortName
$gref = setShortName('gref' => $gref, 'maxchar' => 40);
isa_ok($gref, 'HASH', "setShortName - result is hashref");
is($gref->{'byrow'}{'GCA_000826165.2'}{'shortName'}, '-', 
   "setShortName - unselected value");
is($gref->{'byrow'}{'GCA_001467505.1'}{'shortName'}, 
   'Legionella_birminghamensis_CDC_1407_AL', "setShortName - selected value");

# To experiment later 
my $gref2 = dclone $gref;

################################################################################
# test sortTable
$gref = sortTable('table' => $gref);

isa_ok($gref, 'HASH', "sortTable - returns a hashref");
my @firstcols = qw/assembly excluded selectionGroup/;
my @firstrows = qw/ GCA_001467045.1 GCA_000236165.1 GCA_001467685.1/;
for my $i (0..2){ 
  is($gref->{'columns'}[$i], $firstcols[$i], "sortTable - sorts cols right");
  is($gref->{'rows'}[$i], $firstrows[$i], "sortTable - sorts rows right");
}
# Save for later
# writeTable('table' => $gref,
# 	   'file' => 'selection_test.tab');

# Add two columns and shuffle colnames
my %col = map { $_ => 1 } @{ $gref2->{'rows'} };
$gref2 = removeColumn('table' => $gref2, 'colname' => 'taxostring');
$gref2 = removeColumn('table' => $gref2, 'colname' => 'selectionGroup');
$gref2 = addColumn('table' => $gref2, 'colname' => 'test1', 'col' => \%col);
$gref2 = addColumn('table' => $gref2, 'colname' => 'test2', 'col' => \%col);
@{ $gref2->{'columns'} } = reverse @{ $gref2->{'columns'} };
$gref2 = sortTable('table' => $gref2);
is($gref2->{'columns'}[-1], 'test1', "sortTable - extra columns");
is($gref2->{'rows'}[-1], 'GCA_900187355.1', 
  "sortTable - removed some cols, rows OK");
# To look further into results
# writeTable('table' => $gref2,
# 	   'file' => 'selection_test2.tab');



