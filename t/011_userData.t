# t/011 - test parsing user-input data
use Test::More tests => 6;
use strict;
use warnings;
use FindBin qw/$Bin/;
use lib "$Bin/../lib";

use File::Temp qw/ tempfile tempdir /;
use PhyloSkeleton qw/ checkUserTable /;
use PhyloSkeleton::TabFiles qw/ readTable writeTable /;

################################################################################
# Open file
my $gref = readTable('file' => "$Bin/../share/userGenomes.tab", 
		     'rowname' => 'assembly', 
		     'lenient' => 0);

################################################################################
# Test checkUserTable
my ($taxoNeeded, $gatherNeeded, $annotNeeded);
($gref, $taxoNeeded, $gatherNeeded, $annotNeeded) = 
  checkUserTable('table' => $gref);
isa_ok($gref, 'HASH', "checkUserTable - returns hashref");
is($taxoNeeded, 3, "checkUserTable - returns taxoNeeded");
is($gatherNeeded, 2, "checkUserTable - returns gatherNeeded");
is($annotNeeded, 5, "checkUserTable - returns annotNeeded");
is($gref->{'byrow'}{'X1'}{'excluded'}, '9_notEnoughInfo', 
   "checkUserTable - returns right excluded");
is($gref->{'byrow'}{'X2'}{'superkingdom'}, 'Bacteria', 
   "checkUserTable - returns superkingdom right");



# # For further inspection
#  writeTable('file' => 'testUserTab.tab',
#  	   'table' => $gref);
