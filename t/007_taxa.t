# t/007_taxa.t - test giving better names
use Test::More tests => 8;
use strict;
use warnings;
use FindBin qw/$Bin/;
use lib "$Bin/../lib";

use PhyloSkeleton::Taxa qw/shortenName shortenNameWGene parseGI computerizeName completeNameNCBI/;

################################################################################
# test computerizeName
my $ln = 'Candidatus Testus testa str. Kaboom&%-=}855 jiiz meh&¤# uid00985';
my $cn = computerizeName($ln);
is($cn, 'Candidatus_Testus_testa_str_Kaboom_855_jiiz_meh_uid00985', 
   "computerizeName");

################################################################################
# test shortenName
my $sn = shortenName($cn);
is($sn, 'Ca_Testus_testa_str_Kaboom_85', "shortenName");

################################################################################
# test shortenNameWGene
my $gene = 'ABCDE664778';
my $ng = shortenNameWGene($cn, $gene);
is($ng, 'Ca_Testus_testa_s_ABCDE664778', "shortenNameWGene");

################################################################################
# test parseGI
my $header = 'gi|332797784|ref|YP_004459284.1|';
my $gi = parseGI($header, 4);
is($gi, 'YP_004459284.1', "parseGI - field 4");
$gi = parseGI($header, 2);
is($gi, '332797784', "parseGI - default");

################################################################################
# test completeNameNCBI
my $on = 'Legionella pneumophila 2300/99 Alcoy';
my $sb1 = 'strain=2300/99 Alcoy';
my $sb2 = 'ATCC 766554';
my $in1 = '2300/9';
my $in2 = 'Coyote';

my $n1 = completeNameNCBI($on, $sb1, $in1);
is($n1, 'Legionella pneumophila 2300/99 Alcoy', 
   "completeNameNCBI - overlap all");

my $n2 = completeNameNCBI($on, $sb2, $in1);
is($n2, 'Legionella pneumophila 2300/99 Alcoy ATCC 766554', 
   "completeNameNCBI - overlap one");

my $n3 = completeNameNCBI($on, $sb2, $in2);
is($n3, 'Legionella pneumophila 2300/99 Alcoy ATCC 766554 Coyote', 
   "completeNameNCBI - no overlap");
