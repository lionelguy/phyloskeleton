#!/usr/bin/perl -w

=head1 NAME

phyloSkeleton.pl - automated phylogenomic selection pipeline

=head1 SYNOPSIS

Getting help:

phyloSkeleton.pl -h|--help

Starting an analysis from scratch:

phyloSkeleton.pl 
    [-o|--output-folder <folder>] 
    [-p|--result-prefix <string>] 
    [-s|--selection-tab <file> | -n|--ncbi-selection <file>]
    [-m|--marker-map <file>]
    [-a|--ncbi-all-projects <file>] 
    [-J|--jgi-selection <file> -L|--jgi-login <login> -P|--jgi-password <pw>] 
    [-t|--taxonomy-table <file>] 
    [-r|--ranks <rank1,rank2,...>] 
    [-T|--taxonomy-path <folder>] 
    [-G|--genomic-data-path <folder>] 
    [--ingroup <string> --ingroup-level <string>  
      [--ingroup-selection-level <string> ---outgroup-selection-level <string>]
        | -l|--levels <file>] 
    [-A|--annotation-software <string>] 
    [-N|--annotate_rrnas] 
    [-f|--force-annotate] 
    [-H|--hmms <file>] 
    [-e|--evalue-threshold <real>] 
    [-c|--completeness-threshold <real>] 
    [-b|--best-match-only] 
    [-R|--refresh-if-older] 
    [--donotgather] [--donotannotate] [--donotattribute]  
    [-C|--cpus <integer>] 
    [--threads-per-process <integer>]

Updating:

phyloSkeleton.pl -s|--selection-tab <file>

=head1 INPUT

=over

=item B<-o>|B<--output-folder> I<folder>

Folder to store output files. Created if not existing. See output for the details of the output files.

=item B<-p>|B<--result-prefix> I<string>

A prefix given to result files and folders, at the maker attribution stage. Default: 'result'.

=item B<-s>|B<--selection-tab> I<file>

File containing user-selected assemblies to add to the selection. Tab-separated file with a header and the following columns (at least): 

=over 

=item - 'assembly': a unique id

=item - 'name': a name representative of the organism

=item - 'source': can be either 'local', 'genbank' or 'jgi'

=item - 'genomefile' or, if the source is 'genbank', a 'ftp_path' column or nothing if source is 'jgi'

=back

Optional columns can all be included at user's discretion, but a few columns are worth mentioning:

=over

=item - taxid: if this one is set, the following columns will be added: gcode, some "ranks" columns (corresponding to the '-r|--ranks' option), taxostring

=item - gcode: if no taxid is present and the organism doesn't have a standard genetic code (11).

=item - selectionGroup: useful to group organism and color them later

=item - genomeFile and proteomeFile: very useful for local data: point to a fasta file containing the genome and proteome, respectively.

=item - shortName: will be set automatically if not set.

=back

=item B<-m>|B<--marker-map> I<file>

A marker map, as output by the pipeline and (eventually) modified by the user to remove paralogs. Running this script with a map and a corresponding --selection-tab file will bypass the whole selection, annotation and HMM attribution sections and only create fasta files. The user will be warned if there are markers with more than one copy per organism. 

=item B<-n>|B<--ncbi-selection> I<file>

Tab-separated file obtained from NCBI genome browser. See below for details.

=item B<-a>|B<--ncbi-all-projects> I<file>

Tab-separated file from NCBI Genbank (assembly_summary_genbank.txt). See below for details.

=item B<-J>|B<--jgi-selection> I<file>

Tab-separated file with a selection of genomes from JGI. See below for details.

=item B<-L>|B<--jgi-login> I<string> and B<-P>|B<--jgi-password> I<string>

Credentials to access the JGI website. Transmitted in clear.

=item B<-t>|B<--taxonomy-table> I<file>

This option can be used as input or output. When running phyloSkeleton.pl for the first time with a given set of assembly lists (see above), use it as output, pointing to a non-existing file. The taxids contained in the assembly lists will be parsed and a taxonomy table will be output. In subsequent runs of phyloSkeleton.pl with the same assembly lists, the taxonomy table can be used as input, to skip parsing the taxids, which can be time consuming.

=item B<-r>|B<--ranks> I<rank1,rank2,...>

Comma-separated list of ranks to parse, that can be used for selection. Any of the ranks used in --levels or the selection criteria should be present here. By default: superkingdom,phylum,class,order,family,genus,species.

=item B<-T>|B<--taxonomy-path> I<folder>

A folder to store taxdump files from NCBI, or where existing files already are. By default current folder (not recommended). 

=item B<--ingroup> I<clade>

An clade representing an ingroup.

=item B<--ingroup-level> I<rank>

The rank of the ingroup.

=item B<--ingroup-selection-level> I<rank>

The rank at which to select the ingroup from. Genus by default.

=item B<--outgroup-selection-level> I<rank>

The rank at which to select the outgroup from. Family by default.

=item B<-l>|B<--levels> I<file>

A tab-separated file describing at which rank taxon should be sampled, with a title with the following column names: 'group' (a taxon name), 'groupLevel' (taxonomic rank of the group, e.g. class, phylum) and selectionLevel (a lower taxonomic level, e.g. genus). If the last element is "name", then all organisms will be selected. 

Example (file must be tab-separated, beware of copy-pasting): 

     #group          groupLevel  selectionLevel
     default         -           class
     Legionellales   order       family
     Legionellaceae  family      genus
     Tatlockia       genus       species
     Fluoribacter    genus       name


=item B<-G>|B<--genomic-data-path> I<folder>

Where to store genomic data. If --ncbi-all-projects is not set, then the downloaded file will be stored there too. By default: genomicData folder in the current folder.

=item B<-A>|B<--annotation-software> I<string>

The annotation software to use. Either 'prodigal' (default) or 'prokka'. The corresponding program must be present on the user's path.

=item B<-A>|B<--annotate-rrnas>

Boolean. Should rRNA genes (16 and 23S) also be annotated?

=item B<-f>|B<--force-annotate>

Boolean. Should genomes already annotated be annotated again? (Doesn't apply to annotations downloaded from NCBI or JGI).

=item B<-H>|B<--hmms> I<file>

A file containing HMM profiles of the markers to use in the marker search phase. These profiles can be generated by the user using the tools available in HMMer, particularly hmmbuild. By default, uses a set of 15 "universal" ribosomal proteins. This collection, as well as four bacteria- and archaea-specific ones, are located in the distribution folder, in the 'share' subfolder. They can be accessed using the reserved variable 'RP15', 'Bact109', 'Bact139' and 'Arch162', respectively.

=item B<-e>|B<--evalue-threshold> I<real> 

E-value threshold to include matches for markers. Default: 1e-10.

=item B<-c>|B<--completeness-threshold> I<real>

Completeness threshold to include genomes in the analysis: genomes that include a lower fraction of genes will be excluded. Default: 0.8.

=item B<-b>|B<--best-match-only>

Should only the best match be included in the main map? This allows to directly run the pipeline to the end and run the multiple alignment files, but it's done at one's own risks: a better paralog might have a slightly higher e-value.

=item B<-R>|B<--refresh-if-older> I<integer>

If files automatically downloaded (taxdump and assembly_summary_genbank.txt), refresh them if older than this number of days. Default: 7.

=item B<--no-excluded>

If set, assemblies not selected will not be printed. Otherwise, all assemblies are printed and the step at which they were excluded is indicated in the column 'excluded'.

=item B<--donotgather>

Boolean. If set, doesn't gather data from external sources (and doesn't go further in the pipeline). Useful to get a first idea of the assembly selection.

=item B<--donotannotate>

Boolean. If set, doesn't annotate the genomes (and doesn't go further in the pipeline). Useful to get a first idea of the assembly selection, and how much is available online.

=item B<--donotattribute>

Boolean. If set, doesn't search ofr marker genes (and doesn't go further in the pipeline). Useful to get an idea of the assembly selection, and how much is available after annotation.

=item B<--donotcreatefasta>

Boolean. If set, doesn't create fasta files per marker. 

=item B<-C>|B<--cpus> I<integer>

Number of CPUs to use to annotate and to search for markers. The program uses Parallel::ForkManager to annotate several genomes in parallel. Default: 4.

=item B<--threads-per-process> I<integer>

Number of threads to use per process. If --cpus is set to 15 and --threads-per-process to 3, then 5 processes (of prokka or barrnap) will be launched in parallel. Default: 4.

=item B<-h>|B<--help>

Displays help message

=back


=head1 EXAMPLES

See README.md at L<https://bitbucket.org/lionelguy/phyloskeleton>

=head1 TODO

See L<https://bitbucket.org/lionelguy/phyloskeleton/issues>

=head1 AUTHOR

Lionel Guy (L<lionel.guy@imbim.uu.se>)

=head1 DATE

Wed Jan 13 11:29:35 CET 2016

=head1 COPYRIGHT AND LICENSE

Copyright 2016 Lionel Guy

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

################################################################################
# Libraries, variables, validation
################################################################################
# Libraries
use strict;
use Getopt::Long qw(:config no_ignore_case);
use Pod::Usage;
use List::MoreUtils qw/ uniq /;
use Cwd;
use File::Path qw/make_path/;
use File::Share ':all';
use File::Basename;

use FindBin qw/$Bin/;
use lib "$Bin/lib";

use PhyloSkeleton qw/ parseNCBIlist parseNCBIlproks parseJGIlist parseSelectionLevels selectAssemblies setShortName checkUserTable ref_rank ass_rank ranks/; 
use PhyloSkeleton::TabFiles qw/ readTable writeTable mergeTables getColumn /;
use PhyloSkeleton::Files qw/ downloadIfOlder /;
use PhyloSkeleton::Taxonomy qw/ downloadTaxdump addTaxonomy parseTaxonomy /;
use PhyloSkeleton::GatherData qw/ getData /; 
use PhyloSkeleton::Annotate qw/ annotate /;
use PhyloSkeleton::AttributeHMMs qw/ attributeHMMs /;
use PhyloSkeleton::Sequences qw/ fastasFromMap /;

# Constants
my %ref_rank = ref_rank();
my %ass_rank = ass_rank();
my @ranks = ranks();
my $sumGenbankURI = "ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/assembly_summary_genbank.txt";

my %included_hmms = ('RP15'    => 1,
		     'Bact109' => 1,
                     'Bact139' => 1,
		     'Arch137' => 1,
                     'Arch162' => 1);

# Options
my $outputFolder = getcwd . '/result';
my $resultPrefix = 'result';
my $selectionTab;
my $markerMap;
my ($taxonomyFolder, $genomicDataFolder);
my ($ncbiSelection, $ncbiAllProjects, $jgiSelection, $jgiLogin, $jgiPassword, 
    $idTab, $ranks);
my ($ingroupName, $ingroupLevel); # = qw/ - species /;
my ($ingroupSelectionLevel, $outgroupSelectionLevel) = qw/ genus family /;
my $levels;
my ($dontgather, $dontannotate, $dontattribute, $dontcreatefasta);
my $annotator = 'prodigal';
my ($annotate_rrnas, $force_annotate);
my $hmms = 'RP15';
my $evalueThreshold = 1e-10;
my $completenessThreshold = 0.8;
my $bestMatchOnly;
my $refreshIfOlder = 7;
my $noexcluded;
my $threads = 4;
my $nthreads_per_process = 4;
my ($debug, $help, $version);

my $isTaxoNeeded;

# Available single-letter options:
# abcdef h   lmnop rst v
# A C   GH J L N P R T
GetOptions('o|output-folder=s'            => \$outputFolder,
	   'p|result-prefix=s'            => \$resultPrefix,
	   's|selection-tab=s'            => \$selectionTab,
	   'm|marker-map=s'               => \$markerMap,
	   'n|ncbi-selection=s'           => \$ncbiSelection,
	   'a|ncbi-all-projects=s'        => \$ncbiAllProjects,
	   'J|jgi-selection=s'            => \$jgiSelection,
	   'L|jgi-login=s'                => \$jgiLogin,
	   'P|jgi-password=s'             => \$jgiPassword,
	   't|taxonomy-table=s'           => \$idTab,
	   'r|ranks=s'                    => \$ranks,
	   'T|taxonomy-path=s'            => \$taxonomyFolder,
	   'G|genomic-data-path=s'        => \$genomicDataFolder,
	   'ingroup=s'                    => \$ingroupName,
	   'ingroup-level=s'              => \$ingroupLevel,
	   'ingroup-selection-level=s'    => \$ingroupSelectionLevel,
	   'outgroup-selection-level=s'   => \$outgroupSelectionLevel,
	   'l|levels=s'                   => \$levels,
	   'A|annotation-software=s'      => \$annotator,
	   'N|annotate-rrnas'             => \$annotate_rrnas,
	   'f|force-annotate'             => \$force_annotate,
	   'H|hmms=s'                     => \$hmms,
	   'e|evalue-threshold=s'         => \$evalueThreshold,
	   'c|completeness-threshold=s'   => \$completenessThreshold,
	   'b|best-match-only'            => \$bestMatchOnly,
	   'R|refresh-if-older=s'         => \$refreshIfOlder,
	   'no-excluded'                  => \$noexcluded,
	   'donotgather'                  => \$dontgather,
	   'donotannotate'                => \$dontannotate,
	   'donotattribute'               => \$dontattribute,
	   'donotcreatefasta'             => \$dontcreatefasta,
	   'C|cpus=s'                     => \$threads,
	   'threads-per-process=s'        => \$nthreads_per_process,
	   'h|help'                       => \$help,
	   'd|debug'                      => \$debug,
	   'v|version'                    => \$version,
	  );

# Checking options
pod2usage(-exitval => 1, -verbose => 2) if $help;
pod2usage(-exitval => 1, -verbose => 0, -message => "$PhyloSkeleton::VERSION") 
  if $version;
pod2usage(-exitval => 2, 
	  -message => "One of --ncbi-selection or --selection-tab mandatory.\n")
  unless ($ncbiSelection || $selectionTab);
pod2usage(-exitval => 3,
	  -message => "Either --levels or --ingroup and --ingroup-level must" . 
	  "be set if --ncbi-selection is set\n") 
  unless (!$ncbiSelection || $levels || ($ingroupName && $ingroupLevel));
pod2usage(-exitval => 4,
	  -message => "JGI selection file requires JGI login and password\n") 
  if ($jgiSelection && !($jgiLogin || $jgiPassword));
pod2usage(-exitval => 5,
	  -message => "Both --ncbi-selection and --marker-map given. Choose ". 
	  "only one.")
  if ($ncbiSelection && $markerMap);

# Variables
my $ncbiGref;  # genomes from genbank by ref, keys are assembly ids
my $jgiGref;   # genomes from jgi by ref, keys are IMG Genome ID
my $userGref;  # genomes from user
my $gref;      # final selection
my $taxidsref; # Taxids
my $selLevRef; # Selection groups
my $sampledTaxaref;
my $taxoDownloaded;

my $skipgather;   # in case it turns out we don't need to gather
my $skipannotate; # in case it turns out we don't need to annotate

@ranks = split(/,/, $ranks) if $ranks;

# Default values for taxonomy and genomic data
$taxonomyFolder = $outputFolder . "/taxonomy" unless $taxonomyFolder;
$genomicDataFolder = $outputFolder . "/genomicData" unless $genomicDataFolder;

# Create output folders if not present
foreach my $path ($outputFolder, $genomicDataFolder, $taxonomyFolder){
  make_path($path) unless (-d $path);
}

$ncbiAllProjects = "$outputFolder/assembly_summary_genbank.txt" 
  unless $ncbiAllProjects;

my $ncbiAllProjectsDir = dirname($ncbiAllProjects);
make_path($ncbiAllProjectsDir) unless (-d $ncbiAllProjectsDir);

# Check default values for hmms
$hmms = dist_file('PhyloSkeleton', $hmms . '.hmm') if ($included_hmms{$hmms});

# If user-provided map, set $dontgather to skip the whole collection steps
$skipgather++ if $markerMap;

## LOG FILE!! ##

# Communicate
print STDERR "Using the following files and folders: \n";
print STDERR "  Output folder:           $outputFolder\n";
print STDERR "  Taxonomy path            $taxonomyFolder\n";
print STDERR "  Genomic data folder      $genomicDataFolder\n";
print STDERR "  Map of markers:          $markerMap\n" if $markerMap;
print STDERR "  User selection list:     $selectionTab\n" if $selectionTab;
print STDERR "  Genbank selection list:  $ncbiSelection\n" if $ncbiSelection;
print STDERR "  Genbank assembly list:   $ncbiAllProjects\n"if $ncbiAllProjects;
print STDERR "  JGI list:                $jgiSelection\n"  if $jgiSelection;
print STDERR "  JGI login:               $jgiLogin\n" if $jgiLogin;
print STDERR "  Taxid tab file:          $idTab\n" if $idTab;
print STDERR "  Ranks:                   $ranks\n" if $ranks;
print STDERR "  Ingroup name & level     $ingroupName, $ingroupLevel\n" 
  if $ingroupName;
print STDERR "  In-/outgroup select lvl: $ingroupSelectionLevel, " . 
  "$outgroupSelectionLevel\n" if $ingroupName;
print STDERR "  Selection levels file    $levels\n" if $levels;


################################################################################
# Add own genomes / previous round
################################################################################

if ($selectionTab){
  $userGref = readTable('file'    => $selectionTab,
			'rowname' => 'assembly',
			'lenient' => 1);
  my ($taxoNeeded, $gatherNeeded, $annotNeeded);
  ($userGref, $taxoNeeded, $gatherNeeded, $annotNeeded) = 
    checkUserTable('table' => $userGref,
		   'ranks' => \@ranks,
		   'annotate_rrnas' => $annotate_rrnas);
  # Determine whether taxonomy, data gathering or annotation is necessary
  if ($taxoNeeded){
    downloadTaxdump('localFolder'    => $taxonomyFolder,
		    'refreshIfOlder' => $refreshIfOlder);
    $taxoDownloaded++;
    my $taxids_ref = getColumn('table' => $userGref,
			       'column'=> 'taxid');
    my @taxids = grep { !/-/ } uniq @$taxids_ref;
    my $userTref = parseTaxonomy('nodes' => $taxonomyFolder . "/nodes.dmp",
				 'ranks' => \@ranks,
				 'taxids'=> \@taxids);
    $userGref = addTaxonomy('gref' => $userGref,
			    'tref' => $userTref);
  }
  $skipgather++ unless ($gatherNeeded || $ncbiSelection);
  $skipannotate++ unless ($annotNeeded || $ncbiSelection);
}

################################################################################
# Parse genome and taxonomy files; selection levels
################################################################################

# NCBI genomes list
if ($ncbiSelection){
  # Selection levels
  if ($levels){
    $selLevRef = parseSelectionLevels('file' => $levels, 
				      'title'=> 1,
				      'titleChar' => '#');
  } else {
    $selLevRef = 
      parseSelectionLevels('ingroupName'  => $ingroupName,
			   'ingroupLevel' => $ingroupLevel,
			   'ingroupSelectionLevel' => $ingroupSelectionLevel,
			   'outgroupSelectionLevel'=> $outgroupSelectionLevel);
  }
  # Refresh assembly_summary_genbank.txt
  downloadIfOlder('remoteFile' => $sumGenbankURI,
		  'localFile'  => $ncbiAllProjects,
		  'age'        => $refreshIfOlder);
  # Parse selection from NCBI
  $gref = parseNCBIlproks('file' => $ncbiSelection);
  # Add info from assembly list
  $gref = parseNCBIlist('file' => $ncbiAllProjects,
			'gref' => $gref);
  # Add JGI if required
  if ($jgiSelection){
    $jgiGref = parseJGIlist('file' => $jgiSelection);
    $gref = mergeTables('tables'  => [ $gref, $jgiGref ],
			'columns' => 'union');
  }
  # Add taxonomy info: first check if we want to use already parsed info.
  my $tref;
  if ($idTab && -s $idTab){
    $tref = readTable('file' => $idTab, 
		      'rowname' => 'taxid');
    # Check and report how many we found.
  } else {
    downloadTaxdump('localFolder'    => $taxonomyFolder,
		    'refreshIfOlder' => $refreshIfOlder) 
      unless $taxoDownloaded; 
    my $taxids_ref = getColumn('table' => $gref,
			       'column'=> 'taxid');
    my @taxids = grep { !/-/ } uniq @$taxids_ref;
    $tref = parseTaxonomy('nodes' => $taxonomyFolder . "/nodes.dmp",
			     'ranks' => \@ranks,
			     'taxids'=> \@taxids);
    # Write result, to the file specified or to a standard place
    my $outputtaxo = $idTab ? $idTab : "$outputFolder/$resultPrefix.taxids.tab";
    writeTable('table' => $tref,
	       'file'  => $outputtaxo);
  }
  $gref = addTaxonomy('gref' => $gref,
		      'tref' => $tref);

  ##############################################################################
  # Select representatives
  ##############################################################################
  $gref = selectAssemblies('genomes'         => $gref,
			   'selectionLevels' => $selLevRef,
			   'taxonomy'        => $tref);
}

# Merge to user-added list
if ($userGref && $gref){
  $gref = mergeTables('tables'  => [ $gref, $userGref ],
		      'columns' => 'union',
		      'rowname' => 'assembly');
} elsif ($userGref) {
  $gref = $userGref;
} elsif (!$gref){
  die "No selection of genomes. Stopping."
}

# Set shortName
$gref = setShortName('gref' => $gref, 'maxchar' => 40);
################################################################################
# Gather data
################################################################################
# Set columm genome, potentially proteome
unless ($skipgather || $dontgather){
  # Gather
  my $ndownloaded;
  ($gref, $ndownloaded) = getData('gref'        => $gref,
				  'genomicData' => $genomicDataFolder,
				  'jgilogin'    => $jgiLogin,
				  'jgipassword' => $jgiPassword,
				  'cookies'     => $outputFolder);
}

################################################################################
# Annotate
################################################################################
# Set column proteome
unless ($skipannotate || $dontgather || $dontannotate){
  my $nannotated;
  ($gref, $nannotated) = annotate(
    'gref'                => $gref,
    'annotation_software' => $annotator,
    'annotate_rrnas'      => $annotate_rrnas,
    'cpus'                => $threads,
    'threads_per_process' => $nthreads_per_process,
    'force'               => $force_annotate);
}

################################################################################
# Attribute HMMs
################################################################################
# Set column n markers?
# Assumes that if you have a user-tab you want
unless ($markerMap || $dontgather || $dontannotate || $dontattribute){
  my $nsearched;
  ($gref, $nsearched) = attributeHMMs(
    'gref'                   => $gref,
    'prefix'                 => $resultPrefix,
    'output_folder'          => $outputFolder,
    'hmms'                   => $hmms,
    'annotate_rrnas'         => $annotate_rrnas,
    'all_single_gene_maps'   => 1,
    'evalue_threshold'       => $evalueThreshold,
    'completeness_threshold' => $completenessThreshold,
    'cpus'                   => $threads,
    'best_match_only'        => $bestMatchOnly);
  $markerMap = "$outputFolder/$resultPrefix.map";
}

################################################################################
# Fasta from map
################################################################################
if ($markerMap && !($dontgather || $dontannotate || $dontattribute || 
	$dontcreatefasta)) {
  my $nfastas;
  ($gref, $nfastas) = fastasFromMap('table'        => $gref,
			      'map'          => $markerMap,
			      'fasta_folder' => 
			      "$outputFolder/$resultPrefix.fasta",
			      'include_rrnas'=> $annotate_rrnas);
}
################################################################################
# Sort, remove excluded if required and print
################################################################################
if ($noexcluded){
  foreach my $assid ( @{ $gref->{'rows'} } ){
    if ($gref->{'byrow'}{$assid}{'excluded'} ne '0_included'){
      removeRow('table'   => $gref,
		'rowname' => $assid);
    }
  }
}

writeTable('file'  => "$outputFolder/$resultPrefix.selection.tab",
	   'table' => $gref);


# Finish
print STDERR "Done\n";
exit 1;
