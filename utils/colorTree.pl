#!/usr/bin/perl -w

=head1 NAME

colorTree.pl - Assign groups and colors, using somehow flexible rules.

=head1 SYNOPSIS

colorTree.pl [-c|--show-colors]

colorTree.pl [-p|--show-palettes]

colorTree.pl [-r|--rules rules_tab] [-s|--group-by-subgroup] [-g|--group-colors file] input_tab

=head1 INPUT

=over

=item B<-c>, B<--show-colors>

If activated, will print all available color names.

=item B<-p>, B<--show-palettes>

If activated, will print all available palette names.

=item B<-r>, B<--rules> I<rules_tab>

The tab-separated file which contains the rules to color. If no rules are given, the groups are colored according to the column selGroup, using the set3 palette.

=item B<-s>, B<--group-by-main-group>

For complex rules, group organisms by their main group (i.e. groups by 'column' column) instead of by group (i.e. groups by 'toneBy' column). This will result in several colors being attributed to the same group.

=item B<-g>, B<--group-colors> I<file>

Outputs the colors by group as well to this file. The colors can be visualized in R, producing a PDF, with the following command:

=begin text

Rscript -e 'df<-read.table("groupColors", h=F, comment.char="", sep="\t", stringsAsFactors=F); cairo_pdf("legendColors.pdf", h=0.21*nrow(df), w=4); par(mar=c(0,0,0,0)); plot(c(0, 1), c(-1, 0), xaxt="n", yaxt="n", type="n"); legend(0, 0, legend=df$V2, fill=df$V1, xjust=0, yjust=1); dev.off()'

=end text

=item I<input_tab>

An input tab coming from the Phylogenomics pipeline.

=back

=head1 DESCRIPTION

colorTree.pl creates a tab file with groups and colors, for input in NexusAddColors.pl. As an input, it takes a selectionTab, the result of one of the steps of the Phylogenomics pipeline, and a tab file giving rules.

The rules can be of two types: the first one is simpler, and attributes a definite color to a group of organisms, using - a priori - any of the columns of the selection tab. The second type of rules attributes a color palette to a group of group organisms, and then at what level should organisms be given the same tone of the palette.

The rules tab is tab-separated file, with the following columns: group, column, value, color and toneBy. The group gives an arbitrary name to the group. The column is where to look for values to split the group into. The value can be a regexp or just a value. All organisms that match will be attributed the color or the palette. The color should be a valid palette name if the next column, toneBy is activated, and a valid color name or a hexadecimal value otherwise. The toneBy tells by which column the tones should be distributed.

The rules are applied hierarchically, from top to bottom: the first rule is applied, then the second, and the first is overruled if it applies to the same organisms.

An example is provided here. The title line is optional, but should start with a hash if present.

=begin text

#group    column    value                color    toneBy
default   selGroup  .*                   set1     phylum
alphas    selGroup  Alphaproteobacteria  pastel1  class
gammas    selGroup  Gammaproteobacteria  greys    family
terrigl   genus     Terriglobus          blue
candid    species   Candidatus*          pink

=end text

The color palettes are from ColorBrewer. More information can be found at their website: L<http://colorbrewer2.org>

The colors can be displayed by running the following code in R (provided the RColorBrewer package is installed):

=begin text

Rscript -e 'library(RColorBrewer); display.brewer.all()'

=end text

=head2 Output

A tab-separated file with three columns: color, organism and group

=head1 EXAMPLES

colorTree.pl

=head1 DEPENDENCIES

=over

=item Graph::Easy

=back

=head1 AUTHOR

Lionel Guy (L<lionel.guy@imbim.uu.se>)

=head1 DATE

Wed Apr 27 13:18:11 CEST 2016

=head1 COPYRIGHT AND LICENSE

Copyright 2016 Lionel Guy

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

# Libraries
use strict;
use Pod::Usage;
use Getopt::Long;
use Graph::Easy qw/ color_name /;

# Constants
my %sets = ( 'accent'  => {'type' => 'categorical', 'max' => 6},  # Qualit/Categ
	     'dark2'   => {'type' => 'categorical', 'max' => 8},
	     'paired'  => {'type' => 'categorical', 'max' => 12},
	     'pastel1' => {'type' => 'categorical', 'max' => 9},
	     'pastel2' => {'type' => 'categorical', 'max' => 8},
	     'set1'    => {'type' => 'categorical', 'max' => 9},
	     'set2'    => {'type' => 'categorical', 'max' => 8},
	     'set3'    => {'type' => 'categorical', 'max' => 12},

	     'brbg'    => {'type' => 'diverging', 'max' => 11}, # Diverging
	     'piyg'    => {'type' => 'diverging', 'max' => 11},
	     'prgn'    => {'type' => 'diverging', 'max' => 11},
	     'puor'    => {'type' => 'diverging', 'max' => 11},
	     'rdbu'    => {'type' => 'diverging', 'max' => 11},
	     'rdgy'    => {'type' => 'diverging', 'max' => 11},
	     'rdylbu'  => {'type' => 'diverging', 'max' => 11},
	     'rdylgn'  => {'type' => 'diverging', 'max' => 11},
	     'spectral'=> {'type' => 'diverging', 'max' => 11},

	     'blues'  => {'type' => 'sequential', 'max' => 9},   # Sequential
	     'bugn'   => {'type' => 'sequential', 'max' => 9},
	     'bupu'   => {'type' => 'sequential', 'max' => 9},
	     'gnbu'   => {'type' => 'sequential', 'max' => 9},
	     'greens' => {'type' => 'sequential', 'max' => 9},
	     'greys'  => {'type' => 'sequential', 'max' => 9},
	     'oranges'=> {'type' => 'sequential', 'max' => 9},
	     'orrd'   => {'type' => 'sequential', 'max' => 9},
	     'pubu'   => {'type' => 'sequential', 'max' => 9},
	     'pubugn' => {'type' => 'sequential', 'max' => 9},
	     'purd'   => {'type' => 'sequential', 'max' => 9},
	     'rdpu'   => {'type' => 'sequential', 'max' => 9},
	     'reds'   => {'type' => 'sequential', 'max' => 9},
	     'ylgn'   => {'type' => 'sequential', 'max' => 9},
	     'ylgnbu' => {'type' => 'sequential', 'max' => 9},
	     'ylorbr' => {'type' => 'sequential', 'max' => 9},
	     'ylorrd' => {'type' => 'sequential', 'max' => 9},
	   );


# Options
my ($show_colors, $show_palettes, $rules_file, $bygroups, $group_color_file);
my $help;

GetOptions('c|show-colors'          => \$show_colors,
	   'p|show-palettes'        => \$show_palettes,
	   'r|rules=s'              => \$rules_file,
	   's|group-by-main-group!' => \$bygroups,
	   'g|group-colors=s'       => \$group_color_file,
	   'h|help'                 => \$help,
);
my $input_tab = shift @ARGV;

pod2usage(-exitval => 1, -verbose => 2) if $help;
pod2usage(-exitval => 2, -message => "Select either to --show-colors, " . 
	  "--show-palettes or input one tab file\n") 
  unless ($input_tab or $show_colors or $show_palettes);


# Initiate colors
my $all_colors = Graph::Easy->color_names();

# Show colors and palettes
show_colors($all_colors) if ($show_colors);
show_palettes(\%sets) if ($show_palettes);

# Read rules and input tab
my $rules_r = read_rules($rules_file);
my $tab_r = read_tab($input_tab);

# Apply and print rules
my ($orgs_r, $cols_r) = apply_rules($tab_r, $rules_r);
print_colors($orgs_r);
print_groups($orgs_r, $group_color_file, $cols_r) if ($group_color_file);

exit 1;
# my $color = Graph::Easy->color_value(1, "accent8");
# #print "Accent1: $color\n";
# foreach my $key (keys %$all_colors){
#   print "$key\n";
# }

################################################################################
## SUBROUTINES
################################################################################
# Apply the rules and returns a hash ref with each org, and a group and color 
# for each
sub apply_rules {
  my ($tab_r, $rules_r) = @_;
  my %orgs;
  my $col_r = ();
  # The base is to color all in black and to give grouping according to 
  # selGroup
  foreach my $org (@{ $tab_r->{'rows'} }){
    #my $name = $tab_r->{'byrow'}{$org}{'shortName'};
    $orgs{$org}{'color'} = check_color('black');
    $orgs{$org}{'group'} = $tab_r->{'byrow'}{$org}{'selectionGroup'};
  }
  # Apply the rules
  my $org_r;
  foreach my $rule_r (@$rules_r){
    if ($rule_r->{'toneBy'}){
      ($org_r, $col_r) = apply_complex_rule($tab_r, $rule_r, \%orgs, $col_r);
    } else {
      ($org_r, $col_r) = apply_simple_rule($tab_r, $rule_r, \%orgs, $col_r);
    }
  }
  return ($org_r, $col_r);
}

# Apply complex rule
sub apply_complex_rule {
  my ($tab_r, $rule_r, $org_r, $col_r) = @_;
  my %groups;
  foreach my $org (keys %$org_r){
    # Collect all shortNames that match the value in the column, then collect
    # all values in the 'toneBy' column, and attribute these values tones
    # from the relevant palette
    # Group value according to the value (i.e. can be different
    # for the same color) of either the 'column' column or 'toneBy' if
    # the 'bysubgroups' option is activated
    my $regexp = $rule_r->{'value'};
    my $grouping_value = $tab_r->{'byrow'}{$org}{$rule_r->{'column'}};
    if ($grouping_value =~ /$regexp/){
      my $subgrouping_value = $tab_r->{'byrow'}{$org}{$rule_r->{'toneBy'}};
      push @{ $groups{$subgrouping_value} }, $org;
      $org_r->{$org}{'color'} = $rule_r->{'color'};
      # Attribute group
      if ($bygroups){
	$org_r->{$org}{'group'} = $grouping_value;
      } else {
	$org_r->{$org}{'group'} = $subgrouping_value;
      }
    }
  }
  # Now go through groups and attribute tones
  my @groups = sort keys %groups;
  my $ngroups = scalar @groups;
  # Fetch appropriate palette subset: minimum 3, if exceeds max then recycle,
  # remove pale tones
  my @tones = findTones($ngroups, $rule_r->{'color'});
  foreach my $i (0..$#groups){
    my $color = $tones[$i % scalar(@tones)];
    # Now attribute tones to the organisms in the groups; push a unique
    # set of colors to $col_r
    push @$col_r, $color unless grep(/^$color$/, @$col_r);
    foreach my $org (@{ $groups{$groups[$i] }}){
      $org_r->{$org}{'color'} = $color;
    }
  }
  return $org_r, $col_r;
}

# Given the palette, the number of groups, returns an array of colors,
# removing the very pale colors
sub findTones {
  my ($ngroups, $palette_name) = @_;
  my $n_in_palette;
  my @tones;
  if ($ngroups < 3){
    $n_in_palette = 3;
  } elsif ($ngroups > $sets{$palette_name}{'max'}) {
    $n_in_palette = $sets{$palette_name}{'max'};
  } else {
    $n_in_palette = $ngroups;
  }
  # Selecting the right palette version
  my $palette = $palette_name . $n_in_palette;
  # Then: 1. remove the palest tones for sequential palettes
  # 2. Remove "middle" color for diverging (whitish)
  my %skip;
  my $setstart = 1;
  my $setend = $n_in_palette;
  if ($sets{$palette_name}{'type'} eq 'sequential'){
    $skip{'1'}++ if ($n_in_palette > 3);
    $skip{'2'}++ if ($n_in_palette > 7);
  } elsif ($sets{$palette_name}{'type'} eq 'diverging'){
    my $middle = ($n_in_palette+1)/2;
    $skip{"$middle"}++ unless ($n_in_palette % 2);
  }
  for my $i ($setstart..$setend){
    push @tones, Graph::Easy->color_value($i, $palette) unless $skip{$i};
  }
  return @tones;
}

# Apply simple rule
sub apply_simple_rule {
  my ($tab_r, $rule_r, $org_r, $col_r) = @_;
  my %groups;
  foreach my $org (keys %$org_r){
    # Collect all shortNames that match the value in the column, and attribute
    # the same color. Group value according to the value (i.e. can be different
    # for the same color)
    my $regexp = $rule_r->{'value'};
    my $grouping_value = $tab_r->{'byrow'}{$org}{$rule_r->{'column'}};
    if ($grouping_value =~ /$regexp/){
      my $color = $rule_r->{'color'};
      push @$col_r, $color unless grep(/^$color$/, @$col_r);
      $org_r->{$org}{'color'} = $color;
      $org_r->{$org}{'group'} = $grouping_value;
    }
  }
  return $org_r, $col_r;
}

# Print the resulting tab with color, name and group
sub print_colors {
  my ($orgs_r) = @_;
  foreach my $org (keys %{ $orgs_r }){
    print $orgs_r->{$org}{'color'}, "\t", $org, "\t", $orgs_r->{$org}{'group'},
     "\n"; 
  }
}

# Print groups and colors
sub print_groups {
  my ($orgs_r, $group_color_file, $cols_r) = @_;
  my %groups;
  foreach my $org (keys %$orgs_r){
    $groups{$orgs_r->{$org}{'color'}}{$orgs_r->{$org}{'group'}}++;
  }
  open GROUPS, '>', $group_color_file
    or die "Cannot open $group_color_file for writing: $!\n";
  foreach my $color (@$cols_r){
  #foreach my $color (sort keys %groups){
    foreach my $group (sort keys %{ $groups{$color} }){
      print GROUPS "$color\t$group\n";
    }
  }
}

# Reads the rule tab
sub read_rules {
  my ($file) = @_;
  my @rules; 
  if ($file){
    open RULES, '<', $file or die "Cannot open rules file $file: $!\n";
    while (<RULES>){
      chomp;
      next if /^#/;
      next if /^\s*$/;
      my ($name, $column, $value, $color, $toneBy) = split;
      die "Not all columns filled at $_\n"
	unless ($name && $column && $value && $color);
      # Check that the color is valid or is a set
      $color = check_color($color, $toneBy);
      my %rule = ( 'name'   => $name,
		   'column' => $column,
		   'value'  => $value,
		   'color'  => $color,
		   'toneBy' => $toneBy);
      push @rules, \%rule;
    }
  } else {
    my %rule = ( 'name' => 'default',
    		 'column' => 'selGroup',
    		 'value' => '.*',
    		 'color' => 'set3',
    		 'toneBy' => 'selGroup');
    push @rules, \%rule;
  }
  return \@rules;
}

# Reads the organisms tab
sub read_tab {
  my ($file) = @_;
  # Row names col is the column that gives names to the rows. Should
  # be uniqe ids, otherwise you'll run into trouble.
  my $rownames_col = 'shortName';
  my $rownames_idx = 0;
  my @mandatory_cols = ($rownames_col, qw / selectionGroup /);
  my %tab;
  open TAB, '<', $file or die "Cannot open file $file: $!\n";
  my $title = <TAB>;
  chomp $title;
  $title =~ s/#^//;
  my @columns = split("\t", $title);
  # Check whether there are columns matching the mandatory ones, and find the
  # rownames idx
  my %columns = map { $_ => 1 } @columns;
  foreach (@mandatory_cols){
    die "Column $_ mandatory but not present\n" unless $columns{$_};
  }
  ($rownames_idx) = grep { $columns[$_] eq $rownames_col } 0..$#columns;
  my @rows;
  $tab{'ncol'} = scalar @columns;
  push @{ $tab{'columns'} }, [@columns];
  while (<TAB>){
    chomp;
    next if /^#/;
    my @a = split("\t", $_);
    die "Not $tab{'ncol'} columns in line $_: " . scalar(@a) . "\n" 
      unless ($tab{'ncol'} == scalar(@a));
    push @rows, $a[$rownames_idx];
    foreach my $i (0..$#columns){
      $tab{'byrow'}{$a[$rownames_idx]}{$columns[$i]} = $a[$i];
      $tab{'bycol'}{$columns[$i]}{$a[$rownames_idx]} = $a[$i];
    }
  }
  # Stats for rows
  $tab{'nrow'} = scalar @rows;
  push @{ $tab{'rows'} }, @rows;
  return \%tab;
}

# Show all available colors
sub show_colors {
  my ($colors) = @_;
  my $last_color;
  foreach my $color (sort (keys $colors->{'w3c'}, keys $colors->{'x11'})){
    print "$color\n" unless ($color eq $last_color || $color =~ /^\d/);
    $last_color = $color
  }
  exit 1;
}

# Show available palettes, and the max number of colors per palette.
sub show_palettes {
  my ($sets) = @_;
  print "set\ttype\tmax_tones\n";
  foreach my $set (sort { $sets->{$a}{'type'} cmp $sets->{$b}{'type'} ||
			    $a cmp $b } keys %$sets){
    print "$set\t$sets->{$set}{'type'}\t$sets->{$set}{'max'}\n";
  }
  exit 1;
}

# Validate color
sub check_color {
  my ($color, $toneBy) = @_;
  if ($toneBy){
    if ($sets{$color}){
      return $color;
    } else {
      die "Requested a set $color but not a valid one\n";
    }
  } else {
    my $val = Graph::Easy->color_as_hex($color);
    if ($val){
      return $val;
    } else {
      die "Not a valid color name or value: $color\n" unless $val;
    }
  }
}
