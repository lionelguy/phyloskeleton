#!/usr/bin/perl -w

=head1 NAME

attributeCOGs.pl - Attribute reference COGs to a set of new genomes

=head1 USAGE

attributeCOGs.pl [-h|--help] -m|--mapping <file> -c|--cog-folder <folder>  [-l|--cog-list <list>]  [-e|--cog-extension <string>] [-d|--cog-definition <file>] [-o|--output-folder] [-t|--threads <nthreads>] [--evalue <eval>] fasta1 [fasta2 ...]

=head1 INPUT

=over

=item [-h|--help]

Displays help message.

=item -m|--mapping <file>

A mapping file that attributes a genome to all sequences in fasta. Tab separated:

<protid>    <genomeid>

=item -c|--cog-folder <folder>

Folder that contains COGs, in form of aligned sequences (fasta format), one per COG. 

=item [-l|--cog-list <list>]

File containing a list of COGs. Uses only a subset of the COGs present in the folder.

=item [-e|--cog-extension <string>]

File extension of the COG files. By default .aln.

=item [-d|--cog-definition <file>]

Optional. If a previous COG definition is available, that attributes functional class and possibly super-class to COGs.

Format is (at least) 3 columns, tab-separated: COGid, super-class, functional class. Columns 2 and 3 can be empty.

Example:

arCOG00016  COG00524  G  Sugar kinase, ribokinase family
arCOG00017  COG02522  R  Predicted transcriptional regulator
arCOG00018  COG00063  G  Predicted sugar kinase

=item [-o|--output-folder]

Folder to output (intermediate) results, among others the concatenated database. By default, creates a 'db' folder in the current folder.

=item [-t|--threads <nthreads>]

Number of threads to use in the psiblast run. By default, 4.

=item fasta1 [fasta2 ...]

Fasta file(s) containing all proteins described in the mapping file.

=back

=head1 OUTPUT

Output is an COG definition, comma-separated file, which consists of the following fields: <domain-id>,<genome-name>,<protein-id>,<protein-length>,
<domain-start>,<domain-end>,<arCOG-id>,<membership-class>,
<functional-class>,<super-cluster>

Example:

305663673,Ignisphaera_aggregans_DSM_17230_uid51875,305663673,108,
1,108,arCOG00001,0,
K,COG1695

See COG documentation for more details.

=head1 DETAILS

The following steps are taken:

=over

=item 1

Prepare data

=over 

=item 1

Concatenate fasta files

=item 2

Make sure all fasta entries are in mapping and vice-versa

=item 3

Calculate lengths for all genes

=back

=item 2

PSI-blast

=over

=item 1

Make database with concatenated fasta file

=item 2

Run PSI-blast

=back

=item 3

Parse

=over

=item 1

Read

=item 2

Sort for each query

=back

=item 4

Output results

=back

=head1 AUTHOR

Lionel Guy (lionel.guy@icm.uu.se)

=head1 DATE

Thu Feb  6 16:11:05 CET 2014

=head1 COPYRIGHT

Copyright (c) 2014 Lionel Guy

=head1 LICENSE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

## Libraries
use strict;
use File::Basename;
use Pod::Usage;
use Getopt::Long;
use Bio::SeqIO;
#use Bio::Tools::Run::BlastPlus;
use Bio::Tools::Run::StandAloneBlastPlus;

my @blast_fields = qw/q s perid allen mism gaps qs qe ss se eval bits/;

## Options
my $help;
my ($mapping_file, $cog_folder, $cog_list, $cogdef_file);
my $cog_ext = 'aln';
my $output_folder = 'db';
my $nthreads = 4;
my $evalue = 1e-4;
GetOptions(
    'h|help'             => \$help,
    'm|mapping=s'        => \$mapping_file,
    'c|cog-folder=s'     => \$cog_folder,
    'l|cog-list=s'       => \$cog_list,
    'e|cog-extension=s'  => \$cog_ext,
    'd|cog-definition=s' => \$cogdef_file,
    'o|output-folder=s'  => \$output_folder,
    't|threads=s'        => \$nthreads,
    'evalue=s'           => \$evalue,
);

pod2usage(-exitval => 1, -verbose => 1) if $help;
pod2usage(-exitval => 2, -message => "No fasta files\n") unless (@ARGV);
pod2usage(-exitval => 2, -message => "No mapping file or file not found\n") 
    unless ($mapping_file && -e $mapping_file);
pod2usage(-exitval => 2, -message => "No COG folder or not a folder\n") 
    unless ($cog_folder && -d $cog_folder);

## Variables
my %seqs;
my %map;
my $fasta_db = "$output_folder/db.fa";

#-------------------------------------------------------------------------------
# 1. Prepare data
#-------------------------------------------------------------------------------
## Read fasta, store in %seqs, concatenate
print STDERR "Preparing data: read and concatenate fasta file... ";
mkdir($output_folder) unless(-d $output_folder);
my $n_seqs = 0;
my $fasta_out = Bio::SeqIO->new(-file => ">$fasta_db",
				-format => 'fasta');
foreach (@ARGV){
    my $fasta_in = Bio::SeqIO->new(-file => $_, 
				   -format => 'fasta');
    while (my $seq = $fasta_in->next_seq){
	## Debug
	#last if ($n_seqs > 1000);
	## End debug
	my $id = deNCBIze($seq->id);
	$seqs{$id}{'seq'} = $seq;
	$seqs{$id}{'length'} = $seq->length;
	$fasta_out->write_seq($seq);
	$n_seqs++;
    }
}
print STDERR "  written $n_seqs sequences to $fasta_db\n";

## Read mapping, store in %seqs, check that present in fasta
print STDERR "Reading mapping... ";
open MAP, '<', $mapping_file or die "$!";
my $n_ids; 
while (<MAP>){
    my ($protid, $genomeid) = split;
    $protid = deNCBIze($protid);
    die "ID $protid in mapping not present in fasta files.\n" 
	unless $seqs{$protid}{'seq'};
    $seqs{$protid}{'genome'} = $genomeid;
    $n_ids++;
}
print STDERR "read ", scalar(keys %seqs), " ids\n";
warn "Number of seqs and ids not matching: sequences in the fasta file " .
    "are not present in the mapping. These will be ignored.\n" 
    unless ($n_ids == $n_seqs);

## Read arCOGdef
my %cogmap;
if ($cogdef_file){
    print STDERR "Reading COG definitions... ";
    open COGMAP, '<', $cogdef_file or die "$!"; 
    while (<COGMAP>){
	my ($arcog, $cog, $fun_class, @rest) = split(/\t/, $_);
	$cogmap{$arcog}{'cog'} = $cog;
	$cogmap{$arcog}{'fun_class'} = $fun_class;
    }
    print STDERR "read ", scalar(keys %cogmap), " COG definitions\n";
}

## Read COG list
my %cogsubset;
if ($cog_list){
    open COGSUB, '<', $cog_list or die "$!";
    while (<COGSUB>){
	chomp;
	s/\.$cog_ext$//;
	$cogsubset{$_}++;
    }
}

#-------------------------------------------------------------------------------
# 2. Do blasts
#-------------------------------------------------------------------------------
## Do blasts
print STDERR "Prepare BLAST database... ";
my $mkdbcmd = "makeblastdb -in $fasta_db -dbtype prot -out $fasta_db &> mkblastdb.log";
catch_system($mkdbcmd);
print STDERR "done.\n";
# open folder where COGs are
opendir(my $dh, $cog_folder) or die;
my $n_cogs = 0;
print STDERR "Doing BLASTS...\n";
while (readdir $dh) {
    next if /^\./;
    next unless /\.$cog_ext$/;
    my $cog_file = "$cog_folder/$_";
    my $cog_name = basename($cog_file, ".$cog_ext");
    next if (%cogsubset && !$cogsubset{$cog_name});
    ## Debug
    #last if $n_cogs > 10;
    ## End debug
    # Here, do blast: no method available on Bio::Seq to do PSSM-based so
    # doing it on the command line.
    my $cmd = "psiblast -in_msa $cog_file -db $fasta_db -ignore_msa_master " 
	. "-num_threads $nthreads -evalue $evalue -show_gis -outfmt 7 " 
	    . "-max_target_seqs 1000 -dbsize 100000000 -comp_based_stats F " 
		. "-seg no > $output_folder/$cog_name.blast";
    catch_system($cmd);
    $n_cogs++;
    print STDERR "\r  done $n_cogs blasts. Reading blast for $cog_name";
    open BLAST, '<', "$output_folder/$cog_name.blast" 
	or die "Could not open file: $!";
    while (<BLAST>){
	chomp;
	next if /^#/;
	# parse BLAST fields
	my @values = split;
	die "Not the correct number of fields in $_\n" 
	    unless (@blast_fields == @values);
	my %h;
	for my $i (0 .. $#blast_fields){
	    $h{$blast_fields[$i]} = $values[$i];
	}
	# Blast incorreclty attributes "unnamed" to s
	$h{'q'} = $cog_name;
	# Parse ids, to take the second field of <db1>|<dbid1>|<db2>|<dbid2>
	if ($h{'s'} =~ /\|/){
	    my @fields = split(/\|/, $h{'s'});
	    $h{'s'} = $fields[1];
	}
	die "ID $h{'s'} not found in map\n" unless ($seqs{$h{'s'}});
	push @{ $seqs{$h{'s'}}{'hits'} }, \%h;
    }
}
closedir $dh;
print STDERR "\r  performed $n_cogs blasts\n";

#-------------------------------------------------------------------------------
# 3. Sort results
#-------------------------------------------------------------------------------

# Treat each query independently
my @results;
print STDERR "Sorting profile hits per protein query\n";
my ($hits, $no_hits, $sec_hits) = (0, 0, 0);
foreach my $id (sort keys %seqs){
    print STDERR "\r  reading hits for pid $id";
    # Get the array from the big hash
    my $genome = $seqs{$id}{'genome'};
    my @hits;
    if ($seqs{$id}{'hits'}){
	my $primary_cog;
	@hits = @{ $seqs{$id}{'hits'} };
	# Sort hits by bit score
	foreach my $key (sort { $b->{'bits'} <=> $a->{'bits'} } @hits){
	    # print $key->{'s'}, ": ", $key->{'q'}, ". Score: ", $key->{'bits'},
	    # 	"\n";
	    if (!$primary_cog){
		$hits++;
		#my $res = formatCOG($key);
		push @results, formatCOG($key);
		$primary_cog = $key;
	    } elsif (nonOverlapping($primary_cog, $key)){
		$sec_hits++;
		#my $res = formatCOG($key);
		push @results, formatCOG($key);
		# Only one secondary hit
		last;
	    }
	}
    } else {
	#formatNoCOG($id);
	push @results, formatNoCOG($id);
	$no_hits++;
    }
}
## Sort and write
foreach my $res ( sort { $a->{'arcog'} cmp $b->{'arcog'} ||
			     $a->{'genome'} cmp $b->{'genome'} ||
				 $a->{'id'} cmp $b->{'id'} } @results ) {
    print $res->{'string'};
}

print STDERR "\nDone.\nAnalyzed ", scalar(keys %seqs), " proteins:\n  ",
    "without hits: $no_hits\n  with hits: $hits\n    ", 
    "with secondary hits: $sec_hits\n";
## print STDERR "Error checking: $hits + $no_hits = ", ($hits+$no_hits), " =? " scalar(keys %seqs), "\n";

sub nonOverlapping {
    my ($one, $two) = @_;
    if ($one->{'ss'} > $two->{'se'} || $one->{'se'} < $two->{'ss'}){
	return 1;
    } else {
	return 0;
    }
}

#<domain-id>, <genome-name>, <protein-id>,<protein-length>,
#<domain-start>, <domain-end>, <arCOG-id>, <membership-class>, <functional-class>, <super-cluster>

#* Example:
#305663673,Ignisphaera_aggregans_DSM_17230_uid51875,305663673,108,1,108,arCOG00001,0,K,COG1695
sub formatCOG {
    my ($hit) = @_;
    my $id = $hit->{'s'};
    my $genome = $seqs{$id}{'genome'};
    my $length = $seqs{$id}{'length'};
    my $arcog = $hit->{'q'};
    my $mem_class = 0;
    my $s = $hit->{'s'} . ",$genome,$id,$length," . $hit->{'ss'} . "," . 
	$hit->{'se'} . ",$arcog,$mem_class,";
    $s .= $cogmap{$arcog}{'fun_class'} if ($cogmap{$arcog}{'fun_class'});
    $s .= ",";
    $s .= $cogmap{$arcog}{'cog'} if $cogmap{$arcog}{'cog'};
    $s .= "\n";
    my %h = ( 'genome' => $genome,
	      'id'     => $id,
	      'arcog'  => $arcog,
	      'string' => $s );
    return \%h;
}
sub formatNoCOG {
    my ($id) = @_;
    my $genome = $seqs{$id}{'genome'};
    my $length = $seqs{$id}{'length'};
    my $s = "$id,$genome,$id,$length,1,$length,,,,\n";
    my %h = ( 'genome' => $genome,
	      'id'     => $id,
	      'arcog'  => 'zzz',
	      'string' => $s );
    return \%h;
}

sub deNCBIze {
    my $id = shift @_;
    if ($id =~ /\|/){
	my @f = split(/\|/, $id);
	$id = $f[1];
    }
    return $id;
}

sub catch_system {
    my $cmd = shift @_;
    my $return_code = system($cmd);
    if ($return_code) {
        die "Failed with code $return_code\nCommand $cmd\nMessage: $?\n";
    }
    return 0;
}
