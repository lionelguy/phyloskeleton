#!/usr/bin/perl -w
use strict;
use Bio::NEXUS;
use Getopt::Long;
use File::Temp qw/ tempfile tempdir /;

## Read from file
my $infile;
my $remove;
my $unique;
my ($fh, $filename) = tempfile();
GetOptions(
    'i|infile=s' => \$infile,
    'r|remove-non-alphanumeric-characters' => \$remove,
    'u|give-unique-names' => \$unique,
);
my $INCON = \*STDIN;
if ($infile){
    open $INCON, '<', $infile or die;
}

my ($tree_str) = <$INCON>;
chomp $tree_str;

## Remove dashes. Hopefully prevents the nexus module to screw up
$tree_str =~ s/[\/\'\=\-]/_/g if $remove;

## Create an empty Trees Block, and then add a tree to it
my $trees_block   = new Bio::NEXUS::TreesBlock('trees');

$trees_block->add_tree_from_newick($tree_str, "my_tree");
## Create new Bio::NEXUS object
my $nexus_obj = new Bio::NEXUS;
$nexus_obj->add_block($trees_block);
$nexus_obj->write($filename);

## Fine tune and print
open NEX, '<', $filename;
my %labels;
my %nonuniq;
while (my $line = <NEX>){
    chomp $line;
    if ($line =~ /(\s+)(TAXLABELS\s+.*)$/){
    	my $spaces = $1;
    	my @taxlabels = split(/\s+/, $2);
	$taxlabels[-1] =~ s/\;$//;
	my $tag = shift @taxlabels;
	print "$spaces$tag\n";
	# find non-uniques
	if ($unique){
	    foreach (@taxlabels){
		$nonuniq{$_} = 1 if $labels{$_};
		$labels{$_}++;
	    }
	    foreach (@taxlabels){
		$_ = $_ . $nonuniq{$_}++ if $nonuniq{$_};
	    }
	}
    	print $spaces . join("\n$spaces", @taxlabels) . "\n;\n";
    }
    elsif ($line =~ /\s+TREE my_tree =/){
	if ($unique){
	    foreach my $tag (keys %nonuniq){
		my $c = 1;
		$line =~ s/$tag/$tag . $c++/ge;
	    }
	}
    	$line =~ s/inode//g;
    	$line =~ s/\';\'//g;
    	print "$line\n";
    }
    else {
    	print "$line\n";
    }
}
