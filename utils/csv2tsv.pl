#!/usr/bin/perl -w

=head1 NAME

csv2tsv.pl - Convert CSV to TSV, taking care of embbedded commas and newlines

=head1 SYNOPSIS

csv2tsv.pl [-q|--quote-char] < input.csv > output.tsv
csv2tsv.pl [-q|--quote-char] -i|--input input.csv -o|--output output.tsv

=head1 INPUT

=over

=item I<input.csv> 

A comma-separated value file.

=item B<-q>|B<--quote-char> I<character>

A character to protect text fields. None by default. You may need to escape the character ("\"").

=head1 DESCRIPTION

In the very mean case where there are mixed \r\n in the input, one can use

tr -d '\015' 

to remove them. To get rid of annoying badly escaped quotation marks, try

sed 's/\([^,]\)"/\1/g'

=head2 Output

A tab-separated value file. Embedded newlines and tabs are removed.

=head1 DEPENDENCIES

Text::CSV

=head1 AUTHOR

Lionel Guy (L<lionel.guy@imbim.uu.se>)

=head1 DATE

Mon Jan 18 14:22:53 CET 2016

=head1 COPYRIGHT AND LICENSE

Copyright 2016 Lionel Guy

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

# libraries
use strict;
use Pod::Usage;
use Getopt::Long;

use FindBin qw/$Bin/;
use lib "$Bin/../lib";

use PhyloSkeleton::TabFiles qw/ csv2tsv /;

my $quote_char = undef;
my $help;
my ($input, $output);

GetOptions('h|help'         => \$help,
	   'q|quote_char=s' => \$quote_char,
	   'i|input=s'      => \$input,
	   'o|output=s'     => \$output,
);

pod2usage(-exitval => 1, -verbose => 2) if $help;

my $res = csv2tsv('input' => $input,
		  'output'=> $output,
		  'quote_char' => $quote_char);


