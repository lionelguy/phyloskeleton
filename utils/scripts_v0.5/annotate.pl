#!/usr/bin/perl -w

=head1 NAME

annotate.pl - Annotate unannotated genomes with prokka

=head1 SYNOPSIS

annotate.pl -t|--tab <file> -e|--exclude <file> [-a|--annotation-software <string>] [-r|--annotate-rrnas] [-c|--cpus <integer>] [--threads-per-process <integer>] [-f|--force]

=head1 INPUT

=over

=item B<-t>, B<--tab> I<file>

A tab-separated file, from gatherData.pl.

=item B<-e>, B<--exclude> I<file>

A list of shortNames to exclude.

=item B<-a>, B<--annotation-software>

Annotation software to be used to find CDSs. So far only prodigal (default) or prokka.

=item B<-r>, B<--annotate-rrnas>

Boolean. Should rRNAs be also annotated?

=item B<-c>, B<--cpus> I<integer>

Number of cpus to use. The program uses Parallel::ForkManager to annotate several genomes in parallel. Default: 4.

=item B<--threads-per-process> I<integer>

Number of threads to use per process. If --cpus is set to 15 and --threads-per-process to 3, then 5 processes (of prokka or barrnap) will be launched in parallel. Default: 4.

=item B<-f>, B<--force>

Force running annotation again for organisms for which contigs only were downloaded.

=back

=head1 DESCRIPTION

Annotate unannotated genomes with prodigal or prokka (for CDS) and barrnap (rRNAs). Prokka annotation are better and more comprehensive but require more installation.
Use tab from previous step and replace (if necessary) the last field.

=head2 Output

=head1 DEPENDENCIES

=over

=item gatherData.pl to provide the correct input file

=item Parallel::Forkmanager

=item Bio::Tools::GFF

=item Bio::SeqIO

=item prokka or prodigal (the first one depends on the second)

=item barrnap

=back

=head1 AUTHOR

Lionel Guy (L<lionel.guy@imbim.uu.se>)

=head1 DATE

Fri Jan 22 10:09:21 CET 2016

=head1 COPYRIGHT AND LICENSE

Copyright 2016 Lionel Guy

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

# libraries
use strict;
use Pod::Usage;
use Getopt::Long;
use File::Basename;
use List::Util 'shuffle';
use Parallel::ForkManager;
use POSIX qw (floor);
use Bio::Tools::GFF;
use Bio::SeqIO;

# variables
my $help;
my $debug;
my $threads = 4;
my $nthreads_per_process = 4;
my $force;
my ($intab, $exclude, $dorrnas);
my $annotator = 'prodigal';
GetOptions('t|tab=s'                 => \$intab,
	   'e|exclude=s'             => \$exclude,
	   'a|annotation-software=s' => \$annotator,
	   'r|annotate-rrnas'        => \$dorrnas,
	   'c|cpus=s'                => \$threads,
	   'threads-per-process=s'   => \$nthreads_per_process,
	   'f|force'                 => \$force,
	   'h|help'                  => \$help,
	   'd|debug'                 => \$debug,
);


pod2usage(-exitval => 1, -verbose => 2) if $help;
pod2usage(-exitval => 2, 
	  -message => "No selection tab input\n") unless ($intab);

# Check annotator
my %annotators = ( 'prokka'   => 1,
		   'prodigal' => 1);
pod2usage(-exitval => 3, 
	  -message => "Annotation software can only be one of " . 
	  join(", ", sort(keys %annotators))) 
  unless ($annotators{$annotator});


# Constants
my @suff = qw/fna faa/;

# Variables
my %prefixes;
my $exclude_r;
my @rows;

# Read exclude
if ($exclude){
  $exclude_r = readExclude($exclude);
}

# Start ForkManager; one wants to give 4 threads to each process, ergo divdide
# threads
my $nprocesses = int($threads/$nthreads_per_process);
if ($nprocesses < 1){
  $nprocesses = 1;
  $nthreads_per_process = $threads;
}
my $pm = new Parallel::ForkManager($nprocesses);

# Prepare run on finish
$pm->run_on_finish(
  sub {
    my ($pid, $exit_code, $ident, $exit_signal, $core_dump, $retarrref) = @_;
    if ($retarrref){
      push @rows, [ @$retarrref ];
    } else {
      die "No value returned from pm sub\n";
    }
  }
);


# Read tab
my ($nrows, $nannotated) = (0, 0);
print STDERR "Reading selected projects $intab...\n";
open TAB, '<', $intab or die "Could not open $intab: $!\n";
my $cnt = 0;
my $nannot = 0;
# Print a title for the output file
my $title = <TAB>;
chomp $title;
print "$title\t16S_fasta\t23S_fasta\tprefix\n";
while (<TAB>){
  chomp;
  if (/^#/) {
    print "$_\n";
    next;
  }
  $cnt++;
  last if ($debug && $cnt > $debug);
  my @a = split(/\t/, $_);
  my ($name, $kingdom, $genus, $species, $gcode, $shortName, $genomefile) 
    = ($a[1], $a[5],   $a[10], $a[11],   $a[12], $a[21],     $a[22]);
  my $proteomefile = pop @a;
  die "Not all info found at $_\n" unless ($genus && $shortName && $genomefile);
  # Next if excluded
  if ($exclude_r->{$shortName}){
    print STDERR "  $shortName: excluding\n";
    $exclude_r->{$shortName} = undef;
    next;
  }
  my $rrnafile;
  my $prefix = generatePrefix($species, $genus, $name);
  ## PM starts here
  $pm->start and next;
  # Annotate proteins if necessary
  if ($proteomefile =~ /\.faa$/ && -e $proteomefile){
    print STDERR "  $shortName: found protein file\n";
  } else {
    print STDERR "  $shortName: annotate with $annotator\n";
    $proteomefile = annotateCDS($genomefile, $prefix, $shortName, $kingdom, 
			       $gcode, $annotator);
    $nannot++;
  }
  # Annotate rRNAs if necessary
  my ($rRNA16S, $rRNA23S) = ('-', '-'); 
  if ($dorrnas){
    if ($rrnafile && -e $rrnafile) {
      print STDERR "  $shortName: found rRNA file\n";
    } else {
      print STDERR "  $shortName: annotate rRNAs\n";
      ($rRNA16S, $rRNA23S) = 
	annotaterRNA($genomefile, $prefix, $shortName, $kingdom);
    }
  }
  push @a, ($proteomefile, $rRNA16S, $rRNA23S, $prefix);
  print STDERR "  $shortName: done\n";
  $pm->finish(0, \@a);
  ## PM ends here
}
$pm->wait_all_children;

# Print to file
print STDERR "Printing results\n";
foreach my $row_r (sort { $a->[3] cmp $b->[3] or 
			    join('', @{$a}[5..11]) cmp join('', @{$a}[5..11])} 
		   @rows) {
  print join("\t", @$row_r), "\n";
}
# foreach my $aref (@rows){
#   print join("\t", @$aref), "\n";
# }

# Check that we found all to be excluded
print STDERR "Genomes on the exclude list not in the tab:\n";
foreach my $key (keys %{ $exclude_r }){
  print STDERR "  $key\n" 
    if $exclude_r->{$key};
}
print STDERR "Annotated $nannot genomes. Done.\n";

## Use prokka or prodigal directly
# prokka --outdir $localData/$shortName/prokka --force --prefix $shortName --locustag $prefix --fast --increment 10 --kingdom Bacteria --gram neg --cpus $nthreads_per_process --mincontiglen 200 --rnammer --gcode $fna 
# prodigal -i $fna -c -m -g $gcode -p $prodigal_mode -f sco -q";
sub annotateCDS {
  my ($fna, $prefix, $shortName, $kingdom, $gcode, $annotator) = @_;
  my ($filnamefna, $folder, $suffix) = fileparse($fna);
  if (-e "$folder/$shortName.faa" && !$force){
    print STDERR "  $shortName: found previous $annotator annotation, " . 
      "skipping\n";
  } else {
    my $cmd;
    if ($annotator eq 'prokka'){
      $cmd = "prokka --outdir $folder/prokka --force --prefix $shortName --locustag $prefix --fast --increment 10 --kingdom $kingdom --gcode $gcode --cpus $nthreads_per_process --mincontiglen 200 $fna > /dev/null 2> /dev/null";
    } elsif ($annotator eq 'prodigal'){
      mkdir "$folder/prodigal" or die "Cannot mkdir $folder/prodigal" unless
	(-d "$folder/prodigal");
      $cmd = "prodigal -i $fna -c -m -g $gcode -f gbk -a $folder/prodigal/$shortName.faa -o $folder/prodigal/$shortName.gbk -q"
    }
    print STDERR "$cmd\n" if $debug;
    system $cmd;
    # Copy file to the parent directory
    system "cp $folder/$annotator/$shortName.faa $folder/$shortName.faa";
  }
  return "$folder/$shortName.faa";
}

## Use rnammer to annotate
sub annotaterRNA {
  my ($genomefile, $prefix, $shortName, $kingdom) = @_;
  my ($filnamefna, $folder, $suffix) = fileparse($genomefile);
  my %kingdoms = ( 'Bacteria'  => 'bac',
		   'Archaea'   => 'arc',
		   'Eukaryota' => 'euk',
		 );
  my %genes_to_record =  ('16S_rRNA' => 1, 
			  '23S_rRNA' => 1);
  my %molecules;
  my $fileroot = "$folder/$shortName";
  die "Kingdom $kingdom could not be recognized: should be one of " . 
    join(", ", keys(%kingdoms)) . "\n" unless $kingdoms{$kingdom};
  my @fastas;
  # Check whether both are present, in that case skip running barnnap
  if (-e "$fileroot.16S_rRNA.fasta" && -e "$fileroot.23S_rRNA.fasta" 
      && !$force){
    @fastas = ("$fileroot.16S_rRNA.fasta", "$fileroot.23S_rRNA.fasta");
    print STDERR "  $shortName: both rRNA files present or force\n";
  } else {
    # Run barnnap, open the GFF, parse it, determine the best rRNAs and 
    # open the fasta file to retrieve the actual sequences
    my %rrnas;
    my $cmd = "barrnap --kingdom $kingdoms{$kingdom} --quiet --threads $nthreads_per_process --lencutoff 0.8 --reject 0.5 --evalue 1e-06 $genomefile ";
    print STDERR "  $shortName: running barrnap\n";
    print STDERR "$cmd\n" if $debug;
    open CMD, "-|", $cmd or die "Cannot run barrnap: $!\n";
    my $gffin = Bio::Tools::GFF->new(-fh => \*CMD, -gff_version => 3);
    while (my $feature = $gffin->next_feature){
      # Check that the feature has a Name and score tag
      unless ($feature->has_tag('Name') && $feature->has_tag('score')){
	print STDERR "  $shortName: WARN: No name/score in barrnap result\n";
	next;
      }
      my ($name) = $feature->get_tag_values('Name');
      my ($score) = $feature->get_tag_values('score');
      next unless $genes_to_record{$name};
      # Now look whether a previous molecule with a better or equal score has
      # been found
      unless ($molecules{$name} && $molecules{$name}{'score'} <= $score){
	$molecules{$name}{'score'} = $score;
	$molecules{$name}{'feature'} = $feature;
      }
    }
    print STDERR "  $shortName: found " . join(", ", keys(%molecules)) . "\n";
    # Now get sequences
    my $rRNA_fastas_r = retrieverRNA(\%molecules, $genomefile, $shortName, 
				     $fileroot, \%genes_to_record);
    # Return a sensitive result, with "-" instead of undefs
    foreach my $mol (sort keys %genes_to_record){
      my $fasta = '-';
      $fasta = $rRNA_fastas_r->{$mol} if $rRNA_fastas_r->{$mol};
      push @fastas, $fasta;
    }
  }
  return @fastas;
}

## Retrieve actual sequences from contigs
sub retrieverRNA {
  my ($mol_r, $genomefile, $shortName, $fileroot, $targets_r) = @_;
  my %ids_to_parse;
  my %seqs;
  foreach my $mol (keys %{ $mol_r }){
    #$seqs{$mol} = '-';
    my $feat = $mol_r->{$mol}{'feature'};
    push @{ $ids_to_parse{$feat->seq_id} }, $mol;
  }
  my $fasta_in = Bio::SeqIO->new(-file => $genomefile, 
				 -format => 'fasta');
  while (my $contig = $fasta_in->next_seq){
    if ($ids_to_parse{$contig->id}){
      foreach my $mol (@{ $ids_to_parse{$contig->id} }){
	my $subseq = $contig->trunc($mol_r->{$mol}{'feature'}->start,
				    $mol_r->{$mol}{'feature'}->end);
	$subseq = $subseq->revcom if ($mol_r->{$mol}{'feature'}->strand == -1);
	$subseq->id("$shortName.$mol");
	my $fasta_out = Bio::SeqIO->new(-file => ">$fileroot.$mol.fasta",
					-format => 'fasta');
	$fasta_out->write_seq($subseq);
	$seqs{$mol} = "$fileroot.$mol.fasta";
      }
    }
    # If we've seen all ids, close the fasta file
    last if keys(%seqs) == keys(%{ $targets_r });
  }
  return \%seqs;
}

## generate prefixes
sub generatePrefix {
  my ($species, $genus, $name) = @_;
  # Trim candidatus
  $genus =~ s/Candidatus //;
  $species =~ s/Candidatus //;
  $name =~ s/Candidatus //;
  # remove unwanted special characters
  $genus =~ s/[^a-zA-Z0-9 ]//g;
  $species =~ s/[^a-zA-Z0-9 ]//g;
  $name =~ s/[^a-zA-Z0-9 ]//g;
  my $pref;
  # Tries the species first, if it is set
  if ($species && $species ne "NA"){
    die "No underscore in $species\n" unless $species =~ / /;
    my ($gen, $spe) = split(/ /, $species);
    $gen = ucfirst $gen;
    $spe = ucfirst $spe;
    $pref = substr($gen, 0, 3) . substr($spe, 0, 3);
    my $cnt = 3;
    while ($cnt <= length($spe) && $prefixes{$pref}){
      $pref = substr($gen, 0, 3) . substr($spe, 0, 2) . substr($spe, $cnt++, 1);
    }
  }
  # Then tries genus
  elsif ($genus && $genus ne "NA"){
    $pref = substr $genus, 0, 6;
    my $cnt = 6;
    while ($cnt <= length($genus) && $prefixes{$pref}){
      $pref = substr($genus, 0, 5) . substr($genus, $cnt++, 1);
    }
    die "No suitable prefix found for $species $genus $name\n" unless $pref;
  }
  # Finally tries name.
  else {
    my @a = split(" ", $name);
    my $str;
    if ($#a > 2){
      $str = join('', @a[2..$#a]);
    } else {
      $str = $a[-1];
    }
    $pref = substr $str, 0, 6;
  }
  # Constrain the length to 6 characters
  $pref .= 0 x (6 - length($pref));
  # Finally draw random combination of positions along the string until 
  # it works
  my $cnt = 0;
  while ($prefixes{$pref}){
    $cnt++;
    die "No suitable prefix found for $species $genus $name\n" if $cnt > 1000;
    $name =~ s/\W//g;
    my @name = split('', $name);
    my @shuffled_idx = shuffle(0..$#name);
    my @idx = sort @shuffled_idx[0..5];
    $pref = join('', @name[(@idx)]);
  }
  # And checks that the prefix is the right length
  die "Prefix $pref too long\n" if length($pref) > 6;
  die "Prefix $pref still exists in previous assemblies\n" if $prefixes{$pref};
  $prefixes{$pref}++,
  #print STDERR "$pref\n";
  return $pref;
}

sub readExclude {
  my ($exclude) = @_;
  my %exclude;
  print STDERR "Reading exclusion tab $exclude\n";
  open TAB, '<', $exclude or die "Could not open $exclude: $!\n";
  while (<TAB>){
    chomp;
    # Take only first column, second is for comments
    my ($id, $comment) = split;
    #print STDERR "  excluding $_\n";
    $exclude{$id}++ unless $exclude{$id};
  }
  print STDERR "  Found ", scalar(keys(%exclude)), " genomes to exclude\n";
  return \%exclude;
}
