#!/usr/bin/perl -w

=head1 NAME

selectAssemblies.pl - Select genomes from Genbank and JGI, picking up the best representatives at various taxonomic levels depending on clades.

=head1 SYNOPSIS

selectAssemblies.pl -n|--ncbi-selection <file> -a|--ncbi-all-projects <file> -j|--jgi-selection <file> -t|--taxids <file> [-i|--ingroup] [-g|--ingroup-level] [-k|--ingroup-selection-level] [-o|--outgroup-selection-level] [-l|--levels]

=head1 INPUT

=over

=item B<-n>|B<--ncbi-selection> I<file>

Tab-separated file obtained from NCBI genome browser. See below for details.

=item B<-a>|B<--ncbi-all-projects> I<file>

Tab-separated file from NCBI Genbank (assembly_summary_genbank.txt). See below for details.

=item B<-j>|B<--jgi-selection> I<file>

Tab-separated file with a selection of genomes from JGI. See below for details.

=item B<-t>|B<--taxids> I<file>

Tab-separated file obtained with parseTaxo.pl. See below for details.

=item B<-i>|B<--ingroup> I<clade>

An clade representing an ingroup.

=item B<-g>|B<--ingroup-level> I<rank>

The rank of the ingroup.

=item B<-k>|B<--ingroup-selection-level> I<rank>

The rank at which to select the ingroup from. Genus by default.

=item B<-o>|B<--outgroup-selection-level> I<rank>

The rank at which to select the outgroup from. Family by default.

=item B<-l>|B<--levels> I<file>

A tab-separated file describing at which rank taxon should be sampled. Three columns: taxon (group) name, taxonomic rank of the group (e.g. class, phylum) and sampling level (a lower taxonomic level, e.g. genus). If the last element is "name", then all organisms will be selected. 

Example: 

=begin text

default	-	class
Legionellales	order	family
Legionellaceae	family	genus
Tatlockia	genus	species
Fluoribacter    genus   name

=end text

=back

=head1 DESCRIPTION

=head2 Selection priorities

First, the RefSeq category (as decided by NCBI) is taken into account. A genome is preferred if it is:

=over

=item 1. Reference

=item 2. Representative

=item 3. None of the above

=back

Then, the assembly level and the source are decisive:

=over

=item 1. Complete (NCBI)

=item 2. Chromosome (NCBI)

=item 3. Finished (JGI)

=item 4. Chromosome with gaps (NCBI)

=item 5. Scaffold (NCBI)

=item 6. Contig (NCBI)

=item 7. Permanent Draft (JGI)

=item 8. Draft (JGI)

=back

Finally, if there are more than one organism with the same assembly and representativity level, the one with the most proteins is included.

A drawback of the two-step selection system is that an incomplete representative genome could be favored over a complete, non-representative. 

=head2 Obtaining the input files

=head3 ncbi-selection

This list (filtered following the user's choice) gives the range of the genomes selected. Go to the genome browser at NCBI:
L<http://www.ncbi.nlm.nih.gov/genome/browse/#tabs-proks>
Filter at will and download the selected records to a .csv file

=head3 ncbi-all-projects

The file assembly_summary_genbank.txt lists all genomes available on Genbank and can be downloaded from L<ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/assembly_summary_genbank.txt>.

=head3 jgi-selection

The file shows all sequencing projects
https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=FindGenomes&page=genomeSearch

=over

=item 1. Select Bacteria in the domain

=item 2. Add the following fields: 

=over

=item Phylum to JGI Project ID

=item Is Public

=item HighQuality

=item Bioproject Accession 

=item Biosample Accesssions

=item Scaffold count 

=item GC% 

=item rRNA count

=back

=item 3. Select all rows

=item 4. Export to xls

=back

=head3 taxids.tab

The file is parsed from NCBI's taxdump with another perl script, using all taxids from the above files:

=begin text

 wget ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump.tar.gz
 tar -C /somewhere/ -xvzf taxdump.tar.gz
 cat <(cut -f10 <jgi-all-projects>) <(cut -f6 <ncbi-all-projects>) | grep -e "[0-9]" | sort -n | uniq > taxids.list
 parseTaxo.pl -n /somewhere/nodes.dmp taxids.list -r superkingdom,phylum,class,order,family,genus,species > taxids.tab

=end text

=head2 Output

A tab file file with the name, class|order|family|genus|species, nprots, gc,
taxid, prots, bioproject, assembly status, selection status, origin, path,
wgs master

=head1 EXAMPLES

selectAssemblies.pl

=head1 AUTHOR

Lionel Guy (L<lionel.guy@imbim.uu.se>)

=head1 DATE

Wed Jan 13 11:29:35 CET 2016

=head1 COPYRIGHT AND LICENSE

Copyright 2016 Lionel Guy

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

use strict;
use Getopt::Long;
use Pod::Usage;
use List::MoreUtils;

use TabFiles qw/ readTable /; 

# Options
my ($ncbiSelection, $ncbiAllProjects, $jgiSelection, $idTab);
my ($ingroupName, $ingroupLevel) = ('-', 'species');
my ($ingroupSelectionLevel, $outgroupSelectionLevel) = qw/ genus family /;
my $levels;
my ($debug, $help);

GetOptions('n|ncbi-selection=s'           => \$ncbiSelection,
	   'a|ncbi-all-projects=s'        => \$ncbiAllProjects,
	   'j|jgi-selection=s'            => \$jgiSelection,
	   't|taxids=s'                   => \$idTab,
	   'i|ingroup=s'                  => \$ingroupName,
	   'g|ingroup-level=s'            => \$ingroupLevel,
	   'k|ingroup-selection-level=s'  => \$ingroupSelectionLevel,
	   'o|outgroup-selection-level=s' => \$outgroupSelectionLevel,
	   'l|levels=s'                   => \$levels,
	   'h|help'                       => \$help,
	   'd|debug'                      => \$debug,
	  );

# Checking options
pod2usage(-exitval => 1, -verbose => 2) if $help;
pod2usage(-exitval => 2, 
	  -message => "Mandatory argument 'ncbi-selection' not set.\n") 
  unless ($ncbiSelection);
pod2usage(-exitval => 2, 
	  -message => "Mandatory argument 'ncbi-all-projects' not set.\n") 
  unless ($ncbiAllProjects);
pod2usage(-exitval => 2, 
	  -message => "Mandatory argument 'taxids' not set.\n") 
  unless ($idTab);

# Variables
my $gref = {}; # genomes from genbank by ref, keys are assembly ids
my $jgigref = {}; # genomes from jgi by ref, keys are IMG Genome ID
my $taxidsref = {}; # Taxids
my $selLevRef = {}; # Selection groups
my $sampledTaxaref = {};

# Constants
my %ref_rank = ( 
     'reference genome'      => 1, # NCBI
		 'representative genome' => 2,
		 'na'                    => 9);
my %ass_rank = ( 
     'Complete Genome'       => 1, # NCBI
		 'Chromosome'            => 2,
		 'Chromosome with gaps'  => 4,
		 'Scaffold'              => 5,
		 'Contig'                => 6,
		 'Finished'              => 3, # JGI
		 'Permanent Draft'       => 7,
		 'Draft'                 => 8);
my @ranks = qw/superkingdom phylum class order family genus species/;

# Communicate
print STDERR "Using the following files: \n";
print STDERR   "  Genbank selection list (lproks): $ncbiSelection\n";
print STDERR   "  All assembly list (genbank):     $ncbiAllProjects\n";
print STDERR   "  JGI list:                        $jgiSelection\n" 
  if $jgiSelection;
print STDERR   "  Lineage for all involved taxids: $idTab\n";

## Run
# Parse sampling levels
$selLevRef = &parseLevels($levels, $ingroupName, $ingroupLevel, 
			  $ingroupSelectionLevel, $outgroupSelectionLevel);
# Taxids
$taxidsref = &parseTaxids($idTab, @ranks);
# Parse NCBI selection, all projects, JGI list and add taxonomy info to that
$gref = &parseNCBIlproks($ncbiSelection);
$gref = &parseNCBIlist($gref, $ncbiAllProjects);
$gref = &parseJGIlist($gref, $jgiSelection) if $jgiSelection;
$gref = &addTaxonomy($gref, $taxidsref);
# Select assemblies and print report
($gref, $sampledTaxaref) = &sortAssemblies($gref, $selLevRef, $taxidsref);
&printReport($gref, $sampledTaxaref);

# Finish
print STDERR "Done\n";
exit 1;

###############################################################################
# Subs
###############################################################################
sub printReport {
  my ($gref, $sampledTaxaref) = @_;
  print STDERR "Printing report...";
  my @fields1 = qw/ name source selGroup taxid /; 
  my @fields2 = qw/ gcode bioproject biosample size nprots ranking replicons 
		    wgsMaster ftpPath /;
  # print title
  my $nlines = 0;
  print "id\t", join("\t", (@fields1, @ranks, @fields2)), "\n";
  foreach my $id (sort 
		  { $gref->{$a}{'selGroup'} cmp $gref->{$b}{'selGroup'} or 
		      $gref->{$a}{'taxostring'} cmp $gref->{$b}{'taxostring'}} 
		  values %{ $sampledTaxaref }){
    $nlines++;
    print $id;
    foreach my $field (@fields1, @ranks, @fields2){
      my $str; 
      if ($field eq "replicons"){
	$str = join(';', keys(%{ $gref->{$id}{$field}}));
      } else {
	$str = $gref->{$id}{$field};
      }
      $str = '-' unless $str;
      print "\t$str";
    }
    print "\n";
  }
  print STDERR " printed $nlines lines\n";
}

