#!/usr/bin/perl -w

=head1 NAME

gatherData.pl - Locate data for a tab file from selectAssemblies.pl

=head1 SYNOPSIS

gatherData.pl -t|--selection-tab <file> [-l|--local-genbank <folder>] [-m|--local-genbank-map <file>] [-d|--data-folder <folder>] [-c|--cookies <file>] -e|--email-adress <address> -p|--password <pw> [-j|--jgi-only] [-i|--exclude <file>]

=head1 INPUT

=over

=item B<-t>, B<--selection-tab> I<file>

A tab-separated file, from selectAssemblies.pl.

=item B<-l>, B<--local-genbank> I<folder>

The main folder of a local genbank repository.

=item B<-m>, B<--local-genbank-map> I<file>

The map the the above repository (map.txt, often).

=item B<-d>, B<--data-folder> I<folder>

Where the data is stored locally.

=item B<-c>, B<--cookies> I<file>

File to store cookies from JGI

=item B<-e>, B<--email-address> I<addres>

JGI login.

=item B<-p>, B<--password> I<pw>

JGI password.

=item B<-j>, B<--jgi-only>

Attempt to gather data from JGI only. Useful when rerunning when JGI didn't respond correctly.

=item B<-i>, B<--exclude> I<file>

A file listing assemblies to exclude, one per line (must correspond to the first field of the selection-tab).

=back

=head1 DESCRIPTION

From a selection tab obtained from selectAssemblies.pl, locates the data. Data present on a local genbank mirror is taken from there. Data from genbank not on the mirror is taken from the ftp site (i) from the _protein.faa.gz if present or (ii) from _genomic.fna.gz, and annotate later. Data from jgi: try to fetch it automatically.

=head2 Output

An updated tab file with data location column.

=head1 DEPENDENCIES

=over

=item selectAssemblies.pl to provide the correct input file

=back

=head1 AUTHOR

Lionel Guy (L<lionel.guy@imbim.uu.se>)

=head1 DATE

Tue Jan 19 15:14:37 CET 2016

=head1 COPYRIGHT AND LICENSE

Copyright 2016 Lionel Guy

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

# libraries
use strict;
use Pod::Usage;
use Getopt::Long;
use File::Basename;

use FindBin qw/$Bin/;
use lib "$Bin/lib";
use Taxa qw/shortenName computerizeName/;

#use Phylogenomics::Taxa qw/ shortenName computerizeName /;

my ($help, $debug);
my $cookies = 'cookies';
my $localData = 'data';
my ($intab, $localSrc, $dataMap, $jgiprojects, 
    $jgilogin, $jgipw, $exclude, $jgionly);

GetOptions('t|selection-tab=s' => \$intab,
	   'l|local-genbank=s' => \$localSrc,
	   'm|local-genbank-map=s' => \$dataMap,
	   'd|data-folder=s' => \$localData,
	   'c|cookies=s' => \$cookies,
	   'e|email-address=s' => \$jgilogin,
	   'p|password=s' => \$jgipw,
	   'i|exclude=s' => \$exclude,
	   'j|jgi-only' => \$jgionly,
	   'debug=s' => \$debug,
	   'h|help' => \$help,
);

pod2usage(-exitval => 1, -verbose => 2) if $help;
pod2usage(-exitval => 2, 
	  -message => "Selection tab (-t|--selection-tab) argument required\n")
  unless ($intab);
# pod2usage(-exitval => 3, 
# 	  -message => "JGI password and email address required\n")
#   unless ($jgilogin && $jgipw);


# Constants
my %sources = ('jgi' => 1,
	       'genbank' => 1);
my %exclude;

# Read exclude
if ($exclude){
  print STDERR "Reading exclusion tab $exclude\n";
  open TAB, '<', $exclude or die "Could not open $exclude: $!\n";
  while (<TAB>){
    chomp;
    #print STDERR "  excluding $_\n";
    $exclude{$_}++ unless $exclude{$_};
  }
  print STDERR "  Found ", scalar(keys(%exclude)), " genomes to exclude\n";
}

# Read localDataMap
my %localData;
if ($dataMap){
  print STDERR "Reading local data map $dataMap\n";
  open MAP, '<', $dataMap or die "Cannot open map $dataMap\n";
  while (<MAP>){
    chomp;
    my ($name, $assid) = split;
    $localData{$assid} = $name;
  }
}

# Read tab
my ($nrows, $nfound, $njgi) = (0, 0, 0);
our $initJGI;
print STDERR "Reading selected projects $intab...\n";
open TAB, '<', $intab or die "Could not open $intab: $!\n";
my $cnt = 0;
# Print a title for the output file
my $title = <TAB>;
chomp($title);
print "$title\tshortName\tgenomefile\tproteomefile\n";
while (<TAB>){
  chomp;
  next if /^#/;
  next if /^id\tname/;
  $cnt++;
  last if ($debug && $cnt > $debug);
  my @a = split(/\t/, $_);
  my ($id, $name, $source, $ftpPath) = ($a[0], $a[1], $a[2], $a[20]);
  ## To run JGI genomes only
  next if ($jgionly && $source ne 'jgi');
  # Check that we have all info.
  die "Not all info found at $_\n" unless ($id && $name && $source);
  die "Source $source unknown in $_\n" unless $sources{$source};
  # Obtain meaningful names
  $name = computerizeName($name);
  my $shortName = shortenName($name);
  # Get data, and print the result
  print STDERR "  Working on $shortName... ";
  my ($genomefile, $proteomefile) = getData($source, $id, $ftpPath, $shortName);
  print join("\t", @a), "\t$shortName\t$genomefile\t$proteomefile\n" 
    unless ($genomefile eq '-');
}

### Subs
sub getData {
  my ($source, $id, $ftpPath, $shortName) = @_;
  my ($genomefile, $proteomefile);
  if ($source eq "jgi"){
    ($genomefile, $proteomefile) = getJGIdata($id, $shortName);
  } elsif ($source eq "genbank"){
    ($genomefile, $proteomefile) = getGenbankData($ftpPath, $shortName);
  }
  return ($genomefile, $proteomefile);
}

# Get data from genbank
sub getGenbankData {
  my ($ftpPath, $shortName) = @_;
  my ($assid, $dirs, $suffix) = fileparse($ftpPath);
  my ($genomefile, $proteomefile) = ('-', '-');
  unless (-d "$localData/$shortName"){
    mkdir "$localData/$shortName" or die "Couldn't mkdir $localData/$shortName";
    # Present in local data?
    if ($localData{$assid}){
      print STDERR " get from local repo...";
      my $cmd = "cp $localSrc/$assid/*.f?a.gz $localData/$shortName/";
      print STDERR "$cmd\n" if $debug;
      system $cmd;
    } else {
      # run wget in quiet mode
      print STDERR " wget from NCBI...";
      my $cmd = "wget -q -P $localData/$shortName/ $ftpPath/*f?a.gz";
      print STDERR "$cmd\n" if $debug;
      system $cmd;
    }
    # Gunzip both if present
    foreach my $ext ('genomic.fna', 'protein.faa'){
      if (-e "$localData/$shortName/${assid}_$ext.gz"){
	print STDERR " unzip... ";
	my $cmd = "gunzip $localData/$shortName/${assid}_$ext.gz";
	print STDERR "$cmd\n" if $debug;
	system($cmd);
      }
    }
  }
  # Check whether there is at least one or the other file.
  if (-e "$localData/$shortName/${assid}_genomic.fna"){
    print STDERR " found an fna file...";
    $genomefile = "$localData/$shortName/${assid}_genomic.fna";
  } else {
    print STDERR " found no suitable genome file...";
  }
  if (-e "$localData/$shortName/${assid}_protein.faa"){
    print STDERR " found an faa file...";
    $proteomefile = "$localData/$shortName/${assid}_protein.faa";
  }
  print STDERR " done\n";
  return ($genomefile, $proteomefile);
}

# Get data from JGI. This is not 100% bulletproof, but should be relatively
# conservative and return '-' if unsure
sub getJGIdata {
  my ($jgiid, $shortName) = @_;
  my $filename;
  my ($genomefile, $proteomefile) = ('-', '-');
  # Check that correct credentials have been provided
  unless ($jgilogin && $jgipw){
    print STDERR "  Skipping: source at JGI, but no credentials provided\n";
    return ($genomefile, $proteomefile);
  }
  # Init by logging in. Not sure if it works on its own or if a signon in a 
  # browser is also needed
  unless ($initJGI){
    print STDERR "  Signing in to JGI\n";
    my $cmd = "curl -s https://signon.jgi.doe.gov/signon/create --data-ascii login=$jgilogin\'&'password=$jgipw -b $cookies -c $cookies > /dev/null";
    print STDERR "$cmd\n" if $debug;
    system $cmd;
    $initJGI++;
  }
  # Get portalid
  my $query =  "http://genome.jgi.doe.gov/ext-api/search-service/export?core=genome&query=" . $jgiid . "&searchIn=JGI%20Projects&searchType=Keyword&showAll=false&externallySequenced=true&sortBy=displayNameStr&showRestricted=false&showOnlyPublished=false&showSuperseded=true&sortOrder=asc&rawQuery=false&showFungalOnly=false&programName=all&programYear=all&superkingdom=--any--&scientificProgram=--any--&productCategory=--any--&start=0&rows=1000000";
  my $cmd = "curl -s \"$query\" -b $cookies -c $cookies";
  print STDERR "  Working on $jgiid... retrieving portal ID... ";
  print STDERR "$cmd\n" if $debug;
  open XLS, '-|', $cmd or die "Cannot open io to $jgiid: $!\n";
  my $portalid;
  while (<XLS>){
    chomp;
    if (/=HYPERLINK\(""[^,]+"",""([^,]+)""\)/){
      $portalid = $1;
      close(XLS);
      last;
    }
  }
  unless ($portalid) {
    print STDERR "  Portal ID for $jgiid not found\n";
    return '-';
  }
  # Get xml
  # Skip if there is already a project
  if (-e "$localData/$shortName"){
    print STDERR "  $localData/$shortName exists, skipping\n";
  } else {
    my $cmd = "curl -s \"http://genome.jgi.doe.gov/ext-api/downloads/get-directory?organism=$portalid\" -b $cookies -c $cookies";
    print STDERR "getting XML...";
    print STDERR "$cmd\n" if $debug;
    open(XML, '-|', $cmd) or  
      print STDERR "  Cannot open io to $portalid: $!\n" && return '-';
    # if ($retval){
    #   print STDERR "  Cannot open io to $portalid: $retval\n";
    #   return '-';
    # }
    my $isReading = 0;
    my $url;
    # Parse XML
    while (<XML>){
      chomp;
      if (/User $jgilogin does not have access to/){
	print STDERR "  ERR: could not get $shortName (portalid $portalid)\n";
	return '-';
      }
      $isReading = 0 if /<\/folder>/;
      $isReading++ if /name=\"IMG Data\"/;
      if (/filename="(\S+)\.tar\.gz".*url="(\S+)"/ && $isReading){
	$filename = $1;
	$url = $2;
	$url =~ s/&amp;/&/g;
	last;
      }
    }
    close XML;
    my $exit = $? >> 8;
    unless ($filename && $url && $exit < 8) {
      print STDERR "  Could not find a relevant file for $shortName (portalid ".
	"$portalid)\n";
      return '-';
    }
    # Grab the file
    $cmd = "curl -s \"http://genome.jgi.doe.gov/$url\" -b $cookies -c $cookies > $localData/$filename.tar.gz";
    print STDERR " getting $filename.tar.gz...";
    print STDERR "$cmd\n" if $debug;
    my $retval = system($cmd);
    if ($retval){
      print STDERR "  ERR: could not retrieve tar file from JGI: $retval\n";
      return '-';
    }
    # untar
    $cmd = "tar -C $localData -xzf $localData/$filename.tar.gz > /dev/null";
    print STDERR "$cmd\n" if $debug;
    print STDERR " untarring...";
    $retval = system($cmd);
    if ($retval){
      print STDERR "  ERR: could not untar tar file: $retval\n";
      return '-';
    }
    # rename the folder and ID the right file; 
    print STDERR " moving...";
    $retval = system("mv $localData/$filename $localData/$shortName");
    if ($retval){
      print STDERR "  ERR: could not move file: $retval\n";
      return '-';
    }
  }
  opendir my $dir, "$localData/$shortName"
    or die "Cannot open dir $localData/$shortName: $!\n";
  my ($faa, $fna);
  while (my $file = readdir $dir){
    if ($filename && $file =~ /$filename(\..+)/){
      my $suffix = $1;
      system("mv $localData/$shortName/$file $localData/$shortName/$shortName$suffix") == 0 || die "Moving failed: $?\n";
      $file = "$shortName$suffix";
    }
    if ($file =~ /^$shortName(\..+)/){
      my $suffix = $1;
      if ($suffix eq ".genes.faa.gz"){
	system "gunzip $localData/$shortName/$shortName$suffix";
	$faa = "$localData/$shortName/$shortName.genes.faa";
      } elsif ($suffix eq ".genes.faa"){
	$faa = "$localData/$shortName/$shortName.genes.faa";
      } elsif ($suffix eq ".fna.gz"){
	system "gunzip $localData/$shortName/$shortName$suffix";
	$fna = "$localData/$shortName/$shortName.fna";	
      } elsif ($suffix eq ".fna"){
	$fna = "$localData/$shortName/$shortName.fna";
      }
    }
  }
  if ($faa){
    print STDERR "  found an faa file...";
    $proteomefile = $faa;
  }
  if ($fna){
    print STDERR "  found an fna file...";
    $genomefile = $fna;
  } else {
    print STDERR "  found no suitable genome file...";
  }
  print STDERR " done\n";
  return ($genomefile, $proteomefile);
}
