#!/usr/bin/perl -w

=head1 NAME

attributeHMMs.pl - Attribute HMMs from a set of profiles to a set of genomes.

=head1 SYNOPSIS

attributeHMMs.pl -s|--selection-tab <file> -r|--rrnas -m|--hmms <file> -f|--hmm-result-folder <folder> -o|--output-map <file> [-p|--paralogs-maps-output-folder <folder>] [--force-single-gene-maps] [-n|--matrix-evalues <file>] [-c|--completeness-threshold <double>] [-l|--exclude-list <file>] [-e|--evalue-threshold <double>] [-t|--threads <integer>]

=head1 INPUT

=over

=item B<-s>, B<--selection-tab> I<file>

A selection tab, from the annotate.pl script.

=item B<-r>, B<--rrnas>

Boolean. Should 16S and 23S rRNAs be included in the map?

=item B<-m>, B<--hmms> I<file>

A set of HMM profiles, in a single file.

=item B<-f>, B<--hmm-result-folder> I<folder>

Where to store the hmm alignments.

=item B<-o>, B<--output-map> I<file>

Where to store the main output map (a tab file with four columns: organism, marker, protein id and e-value).

=item B<-p>, B<--paralogs-maps-output-folder> I<folder>

A folder to store maps of individual markers containing paralogs (format as above).

=item B<--force-single-gene-maps>

If set, separate maps will be done for each marker (only for paralogs if --paralogs-maps-output-folder is set). --paralogs-maps-output-folder should be then set.

=item B<-n>, B<--matrix-evalues> I<file>

A file to output a matrix with the evalues for each marker (see output section below).

=item B<-c>, B<--completeness-threshold> I<double>

Threshold to warn and output genomes with low number of genes. 0.8 by default.

=item B<-l>, B<--exclude-list> I<file>

A file to output the genomes to exclude. Two columns are printed: the shortName and a comment in the format lowNumberOfGenes:<fraction>.

=item B<-e>, B<--evalue-threshold> I<double>

E-value threshold for hmmsearch. Default: 1e-6.

=item B<-t>, B<--threads> I<integer>

Number of threads to use. Default: 4.

=back

=head1 DESCRIPTION

=head2 Principle

attributeHMMs.pl retrieves the complete proteomes in the selection tab file, and for each of them, runs hmmersearch with the hmm profiles. It then goes through all the hits and selects the best hit. If more than one hit is found that exceeds the threshold, it can create single-gene maps, that can be used to draw single-gene trees and eliminate paralogs.

=head2 Output

=head3 STDOUT

A tab file representing a marker x organisms matrix with the number of copies of each marker passing the e-value threshold.

=head3 matrix-evalues

The same matrix as above, but containing the e-values (comma-separated) for each marker.

=head3 output-map, paralogs-maps-output-folder

Tab files with four columns, separated with tabs: organism, marker, protein id and e-value.

=head1 DEPENDENCIES

=over

=item Parallel::Forkmanager

=item BioPerl

=item hmmer

=head1 AUTHOR

Lionel Guy (L<lionel.guy@imbim.uu.se>)

=head1 DATE

Fri Jan 22 14:40:16 CET 2016

=head1 COPYRIGHT AND LICENSE

Copyright 2016 Lionel Guy

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

# Libraries
use strict;
use Pod::Usage;
use Getopt::Long;
use Parallel::ForkManager;
use Bio::SeqIO;

# Options
my ($selectionTab, $hmms, $hmmResFolder, $outMap, $excludeList, $dorrnas);
my ($outMatrixEvals, $outParalogMapsFolder, $forceSingleGeneMaps);
my $evalueThreshold = 1e-6;
my $completenessThreshold = 0.8;
my $threads = 4;
my $debug;
my $help;

GetOptions('s|selection-tab=s'               => \$selectionTab,
	   'm|hmms=s'                        => \$hmms,
	   'r|rrnas'                         => \$dorrnas,
	   'f|hmm-result-folder=s'           => \$hmmResFolder,
	   'o|output-map=s'                  => \$outMap,
	   'p|paralogs-maps-output-folder=s' => \$outParalogMapsFolder,
	   'force-single-gene-maps'          => \$forceSingleGeneMaps,
	   'n|matrix-evalues=s'              => \$outMatrixEvals,
	   'e|evalue-threshold=s'            => \$evalueThreshold,
	   'c|completeness-threshold=s'      => \$completenessThreshold,
	   'l|exclude-list=s'                => \$excludeList,
	   't|threads=s'                     => \$threads,
	   'd|debug=s'                       => \$debug,
	   'h|help'                          => \$help,
);

pod2usage(-exitval => 1, -verbose => 2) if $help;
pod2usage(-exitval => 2, 
	  -message => "Argument selection-tab required\n") 
  unless ($selectionTab);
pod2usage(-exitval => 3, 
	  -message => "Argument hmms required\n") unless ($hmms);
pod2usage(-exitval => 4, 
	  -message => "Argument hmm-result-folder required\n") 
  unless ($hmmResFolder);
pod2usage(-exitval => 5, 
	  -message => "Argument output-map required\n") unless ($outMap);


# Variables
my @orgs;
my %orthologs;
my $res_ref;
my %seenids;
# Store result file, parse sequentially after all hmms ran
my %resultfiles;
my %rRNAs;

# Read HMM profiles
my @hmms = parseHMMs($hmms);

# Read selection tab
print STDERR "Reading selected projects $selectionTab...\n";
open TAB, '<', $selectionTab or die "Could not open $selectionTab: $!\n";

# Start ForkManager
my $pm = new Parallel::ForkManager($threads);

# Prepare run on finish
$pm->run_on_finish(
  sub {
    my ($pid, $exit_code, $ident, $exit_signal, $core_dump, $retvalref) = @_;
    if ($retvalref){
      $resultfiles{$retvalref->[0]} = $retvalref->[1];
    } else {
      die "No value returned from pm sub\n";
    }
  }
);

# Print a title for the output file
my $title = <TAB>;
chomp $title;
#print "$title\tprefix\n";
my $cnt = 0;
while (<TAB>){
  $cnt++;
  last if ($debug && $cnt > $debug);
  chomp;
  my @a = split(/\t/, $_);
  my ($shortName, $proteomefile, $r16S_fasta, $r23S_fasta, $prefix) 
    = ($a[21],    $a[23],        $a[24],      $a[25],      $a[26]);
  print STDERR "  working on $shortName...\n";
  die "Not all info found at $_\n" 
    unless ($shortName && $proteomefile && $prefix);
  push @orgs, $shortName;
  getrRNAs($shortName, $r16S_fasta, $r23S_fasta, \%rRNAs) if $dorrnas;
  my $pid = $pm->start and next;
  my $resultfile = runHmmsearch($proteomefile, $hmms, $hmmResFolder, 
				$shortName, $prefix);
  my @return = ($shortName, $resultfile);
  print STDERR "  done with $shortName\n";
  $pm->finish(0, \@return);
}
$pm->wait_all_children;
print STDERR "Done running hmmsearch. Now parsing the files\n";

## Now parse the files
foreach my $shortName (keys %resultfiles){
  print STDERR "  working on $shortName...";
  $res_ref = parseHmmfile($resultfiles{$shortName}, \%orthologs, $shortName);
  print STDERR "done\n";
}

## Print results, first matrices
print STDERR "Searched $cnt proteomes. Now printing matrices\n";
my $paralogs_ref = 
  printMatrices($outMatrixEvals, $res_ref, \@orgs, \@hmms, \%rRNAs);
# Maps
print STDERR "Now printing maps\n";
printMaps($outMap, $outParalogMapsFolder, $res_ref, \@orgs, \@hmms, 
	  $paralogs_ref, \%rRNAs);
# Check completeness
print STDERR "Check completeness\n";
checkCompleteness($res_ref, \@orgs, \@hmms);
print STDERR "Finished\n";

# To warn about paralogs: 
# - prints a complete map with just the best hit
# - prints a map per marker, that can be reused then to run 
#   single gene trees (compatible with fastasFromMap.pl)
# - prints a matrix with numbers
# - prints a matrix with evalues, comma-separated when multiple
# - consider how to modify easily the main map 


#### SUBS ####
# Run hmmsearch
# command: hmmsearch -E 1e-6 --tblout test.tab --cpu 4 ~/bin/ettemalab/galaxy_tools/micomplete/Bact139.hmm $proteome > test.ali
sub runHmmsearch {
  my ($proteomefile, $hmms, $hmmResFolder, $shortName, $prefix) = @_;
  my $cmd = "hmmsearch -E $evalueThreshold --tblout $hmmResFolder/$shortName.tab --cpu 1 $hmms $proteomefile > $hmmResFolder/$shortName.ali";
  print STDERR "$cmd\n" if $debug;
  system($cmd);
  return "$hmmResFolder/$shortName.tab";
}
## Parse rRNA files
sub getrRNAs {
  my ($shortName, $r16S_fasta, $r23S_fasta, $rRNAs_r) = @_;
  if ($r16S_fasta && $r16S_fasta ne "-"){
    my $r16Sin = Bio::SeqIO->new(-file => $r16S_fasta,
				 -format => 'fasta');
    my $seq = $r16Sin->next_seq;
    $rRNAs_r->{$shortName}{'16S_rRNA'} = $seq->id;
    die "More than one sequence in $r16S_fasta\n" if ($r16Sin->next_seq);
  }
  if ($r23S_fasta && $r23S_fasta ne "-"){
    my $r23Sin = Bio::SeqIO->new(-file => $r23S_fasta,
				 -format => 'fasta');
    my $seq = $r23Sin->next_seq;
    $rRNAs_r->{$shortName}{'23S_rRNA'} = $seq->id;
    die "More than one sequence in $r23S_fasta\n" if ($r23Sin->next_seq);
  }
  return 0;
}

## Parse HMM profile file
sub parseHMMs {
  my ($hmm_file) = @_;
  my @names;
  my $nmarker = 0;
  print STDERR "Reading HMM profiles...";
  open HMMS, '<', $hmm_file or die "Cannot open $hmm_file\n";
  while (<HMMS>){
    next unless /^NAME/;
    my ($bla, $name) = split;
    push @names, $name;
    $nmarker++;
  }
  print STDERR " found $nmarker profiles in $hmms\n";
  return @names;
}

sub parseHmmfile {
  my ($file, $res_ref, $shortName) = @_;
  open HMMER, '<', $file or die "Cannot open file $file: $!\n";
  my $nfound = 0;
  while (<HMMER>){
    next if /^#/;
    my ($pid, $dash, $hname, $hid, $evalue) = split;
    die ("No pid at $_\n") unless $pid;
    # Skip if evalue under threshold
    if ($evalue < $evalueThreshold){
      # Skip if evalue is lower
      # push to an hash of hashes, pid as key
      $res_ref->{$hname}{$shortName}{$pid} = $evalue;
      # Make sure that we haven't recorded the same pid for two genomes
      die "non-unique IDs: $pid both in $shortName and $seenids{$pid}\n" 
	if ($seenids{$pid} && $seenids{$pid} ne $shortName);
      $seenids{$pid} = $shortName;
    }
  }
  return $res_ref;
}

# Prints two matrices, one on STDOUT which has the number of copies of each
# marker and another that gives the evalues of each of them
sub printMatrices {
  my ($outMatrixEvals, $res_ref, $orgs_ref, $hmms_ref, $rRNAs_r) = @_;
  # Store paralogs in this hash;
  my %paralogs;
  # Print title of the matrix
  if ($outMatrixEvals) {
    open EVALS, '>', $outMatrixEvals or die "Cannot open $outMatrixEvals: $!\n";
  }
  my $title = "\t" . join("\t", @{ $hmms_ref });
  $title .= "\t16S_rRNA\t23S_rRNA" if $dorrnas;
  $title .= "\n";
  print $title;
  print EVALS $title if $outMatrixEvals;
  # print "\t", join("\t", @{ $hmms_ref });
  # print "\t16S_rRNA\t23S_rRNA" if $dorrnas;
  # print "\n";
  foreach my $org (@{ $orgs_ref }){
    print "$org";
    print EVALS "$org" if $outMatrixEvals;
    foreach my $hmm (@{ $hmms_ref }){
      # $res_ref->{$hmm}{$org} is  an array of hashes
      # dereference first, then sort by increasing evalues, check
      # how many and warn if more than one; add to a hash with paralog-
      # containing hash, and come back to these later
      my %pids;
      if ( $res_ref->{$hmm}{$org} ){
	%pids = %{ $res_ref->{$hmm}{$org} };
	my $n = scalar(keys %pids);
	# Tag if more than one
	$paralogs{$hmm}++ if $n > 1;
	print "\t$n";
	if ($outMatrixEvals){
	  my @evalues = sort { $a <=> $b } values %pids;
	  print EVALS "\t", join(',', @evalues);
	} 
      }else {
	print "\t0";
	print EVALS "\t-" if $outMatrixEvals;
      }
    }
    if ($dorrnas){
      foreach my $mol ('16S_rRNA', '23S_rRNA'){
	if ($rRNAs_r->{$org}{$mol}){
	  print "\t1";
	} else {
	  print "\t0";
	}
	print EVALS "\tNA" if $outMatrixEvals;
      }
    }
    print "\n";
    print EVALS "\n" if $outMatrixEvals;

  }
  # Return paralog-containing hash
  return \%paralogs;
}

# Prints global and single-gene maps for paralogs 
# in three columns (organism, hmm and pid)
sub printMaps {
  my ($map, $outFolder, $res_ref, $orgs_ref, $hmms_ref, $paralogs_ref, 
      $rRNAs_r) = @_;
  open MAP, '>', $map or die "Cannot open map $map: $!\n";
  # Checks that the single-map folder exists, creates it otherwise
  mkdir $outFolder or die "Cannot mkdir $outFolder: $!\n" 
    if ($outFolder && !(-d $outFolder) && 
	($paralogs_ref || $forceSingleGeneMaps));
  foreach my $hmm (@{ $hmms_ref }){
    my $doSingleMap = 
      $outFolder && ($paralogs_ref->{$hmm} || $forceSingleGeneMaps);
    print STDERR "  Marker $hmm has paralogs for $paralogs_ref->{$hmm} " . 
      "organisms\n" if ($paralogs_ref->{$hmm});
    open SINGLE, '>', "$outFolder/$hmm.map" if ($doSingleMap);
    foreach my $org (@{ $orgs_ref }){
      # $res_ref->{$hmm}{$org} is now a hash of hashes
      # dereference first, then sort by increasing evalues, check
      # how many and warn if more than one; add to a hash with paralog-
      # containing hash, and come back to these later
      my $pid = $res_ref->{$hmm}{$org}{'pid'};
      my %pids = %{ $res_ref->{$hmm}{$org} };
      if (%pids){
	my @pids = sort { $pids{$a} <=> $pids{$b} } keys %pids;
	print MAP "$org\t$hmm\t$pids[0]\t$pids{$pids[0]}\n";
	if ($doSingleMap){
	  foreach my $pid (@pids){
	    print SINGLE "$org\t$hmm\t$pid\t$pids{$pid}\n";
	  }
	}
      }
    }
  }
  if ($dorrnas){
    foreach my $mol ('16S_rRNA', '23S_rRNA'){
      foreach my $org (@{ $orgs_ref }){
	if ($rRNAs_r->{$org}{$mol}){
	  print MAP "$org\t$mol\t" . $rRNAs_r->{$org}{$mol} . "\n";
	}
      }
    }
  }
}

sub checkCompleteness {
  my ($res_ref, $orgs_ref, $hmms_ref) = @_;
  my $total_hmms = scalar @$hmms_ref;
  my $n_low_completeness = 0;
  # open the ouput if necessary
  my $fh;
  if ($excludeList){
    open $fh, '>', $excludeList or die "Cannot open $excludeList: $!\n";
  }
  foreach my $org (@$orgs_ref){
    my $n_hmms = 0;
    foreach my $hmm (@$hmms_ref){
      # issue7: take care of that
      $n_hmms++ if $res_ref->{$hmm}{$org};
    }
    my $completeness = $n_hmms / $total_hmms;
    if ($completeness < $completenessThreshold){
      print STDERR "  $org has only $n_hmms / $total_hmms (", 
	sprintf('%.1f', $completeness * 100), "%)\n";
      $n_low_completeness++;
      print $fh "$org\tlowNumberOfGenes:", sprintf('%.3f', $completeness), 
	"\n" if ($excludeList);
    }
  }
  print STDERR "WARNING: $n_low_completeness genomes were less than ",
    $completenessThreshold * 100, "% complete\n" if $n_low_completeness;
}


