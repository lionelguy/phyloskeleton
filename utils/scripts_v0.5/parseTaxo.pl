#!/usr/bin/perl -w

=head1 NAME

parseTaxo.pl - parses NCBI taxonomy, returns tab file with given ranks

=head1 SYNOPSIS

parseTaxo.pl [-l|--localFolder </path/to/localFolder>] [-r|--ranks <rank1,rank2,rank3,...>] idFile

=head1 INPUT

=over

=item B<-l> I<path>, B<--localFolder> I</path/to/localFolder>

Path to local folder where to store taxdump files.

=item B<-r> I<ranks>, B<--ranks> I<rank1,rank2,rank3,...>

Ranks to be parsed for each taxid, comma separated. Default: superkingdom,phylum,class,order,family,genus

=item I<idFile>

File containing the taxids to be parsed, one per line. Mandatory.

=back

=head1 DESCRIPTION

The script parses a taxonomy file from NCBI. It first downloads files from NCBI. Then, for each given taxid, it retrieves all its ancestry, and keeps the given ranks. It also returns the genetic code, which is useful for downstream annotation.

=head2 Output

A tab-separated file on STDOUT, with the taxid, genetic code, scientific name, and the given ranks. If ranks or genetic code are not found, returns NA.

=head1 DEPENDENCIES

=over

=item BioPerl

=back

=head1 AUTHOR

Lionel Guy (L<lionel.guy@imbim.uu.se>)

=head1 DATE

Wed 29 Jul 2015 15:30:39 CEST

=head1 COPYRIGHT AND LICENSE

Copyright 2015 Lionel Guy

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

# libraries
use strict;
use Pod::Usage;
use Getopt::Long;
use File::Basename;
# use Bio::Taxon;
# use Bio::Tree::Tree;
use Time::HiRes;

use PhyloSkeleton::Taxonomy qw/ downloadTaxdump parseTaxo /;
use PhyloSkeleton::Tabfiles qw/ writeTable /;

# options
my $nodes;
my $names;
my $ranks = 'superkingdom,phylum,class,order,family,genus';
my $help;
GetOptions(
    'l|localFolder=s' => \$localFolder,
    'r|ranks=s'       => \$ranks,
    'h|help'          => \$help,
);
my $idFile = shift @ARGV;

# Check that options are correctly set: exits if there is no input file; exit
# unless taxonomy file missing
pod2usage(-exitval => 1, -verbose => 2) if $help;
pod2usage(-exitval => 2, -message => "No id file input or file not found\n")
  unless ($idFile && -e $idFile);
pod2usage(-exitval => 3, -message => "No taxonomy file input\n") unless $nodes;


# Parse ranks from the given ranks
my @ranks = split(/,/, $ranks);
my %ranks = map { $_ => 1 } @ranks;
print STDERR "Considering following ranks: ", join(',', @ranks) . "\n";

# Parse ids from given file
my @ids;
open IDS, '<', $idFile or die "Cannot open $idFile: $!\n";
while (<IDS>){
  chomp;
  unless (/^[0-9]+?/) {
    warn "Not a numerical id, skipping: $_\n";
  } else {
    push @ids, $_;
  }
}
print STDERR "Parsed ", scalar(@ids), " ids\n";

# Download taxdump 
my $ok = downloadTaxdump('localFolder'        => $localFolder, 
			 'refreshIfOlderThan' => 7);

my $tref = parseTaxo('nodes' => "$localFolder/nodes.dmp",
		     'ranks' => \@ranks,
		     'taxids'=> \@ids);
writeTable($tref);
