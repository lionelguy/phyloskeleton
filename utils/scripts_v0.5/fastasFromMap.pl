#!/usr/bin/perl -w

=head1 NAME

fastasFromMap.pl - Parse fasta files and a map and produces fasta per hmm

=head1 SYNOPSIS

fastasFromMap.pl -f|--fasta <folder> -t|--selection-tab <file> -r|--rrnas -m|--selection-map <file> [-i|--ignore-paralogs]

=head1 INPUT

=over

=item B<-f>, B<--fasta> I<folder>

Folder where the output fasta should be stored.

=item B<-t>, B<--selection-tab> I<file>

A selection tab file as produced by annotate.pl

=item B<-r>, B<--rrnas> I<file>

Extract fastas for 16S and 23S rRNAs

=item B<-m>, B<--seletion-map> I<file>

A map of which genes belong to which HMM in each genome, as produced by attributeHMMs.pl

=item B<-i>, B<--ignore-paralogs>

If set, ignores paralogs and allows more than one hit per marker. Useful to draw single-gene trees with several paralogs.

=back

=head1 DESCRIPTION

=head2 Output

One file per HMM found in the selection map. 

=head1 EXAMPLES

fastasFromMap.pl

=head1 DEPENDENCIES

=over

=item Phylogenomics package

=item annotate.pl

=item attributeHMMs.pl

=item BioPerl

=back

=head1 AUTHOR

Lionel Guy (L<lionel.guy@imbim.uu.se>)

=head1 DATE

Mon Jan 25 13:50:46 CET 2016

=head1 COPYRIGHT AND LICENSE

Copyright 2016 Lionel Guy

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

# libraries
use strict;
use Pod::Usage;
use Getopt::Long;
use Bio::SeqIO;

my ($fastaDir, $selectionTab, $map, $dorrnas, $ignoreParalogs);
my $help;
my $debug = 0;

GetOptions('f|fasta=s'         => \$fastaDir,
	   't|selection-tab=s' => \$selectionTab,
	   'r|rrnas'           => \$dorrnas,
	   'm|selection-map=s' => \$map,
	   'i|ignore-paralogs' => \$ignoreParalogs,
	   'd|debug=s'         => \$debug,
	   'h|help'            => \$help,
);

pod2usage(-exitval => 1, -verbose => 2) if $help;
pod2usage(-exitval => 2, 
	  -message => "Argument selection-tab required\n") 
  unless ($selectionTab);
pod2usage(-exitval => 2, 
	  -message => "Argument selection-map required\n") unless ($map);
pod2usage(-exitval => 2, 
	  -message => "Argument fasta required\n") unless ($fastaDir);

# Variables
my %map;
my %seqs;
my %uids;
my @orgs;

# Read map
print STDERR "Reading map $map...";
open MAP, '<', $map or die "Could not open map $map: $!\n";
my $ngenes = 0;
while (<MAP>){
  chomp;
  my ($org, $hmm, $pid) = split;
  die "Two or more ids for hmm $hmm in org $org\n" 
    if ($map{$hmm}{$org} && !$ignoreParalogs);
  $map{$hmm}{$org}{$pid}++;
  $seqs{$pid}++;
  $ngenes++;
}
print STDERR " found $ngenes ids for ", scalar(keys(%map)), " hmms\n";

# Read selection tab, read fastas
print STDERR "Reading fastas in $selectionTab...";
open TAB, '<', $selectionTab or die "Could not open $selectionTab: $!\n";
my $cnt = 0;
my $nids = 0;
my $title = <TAB>;
chomp $title;
while (<TAB>){
  chomp;
  $cnt++;
  last if ($debug && $cnt > $debug);
  my @a = split(/\t/, $_);
  my ($shortName, $proteomefile, $r16S_fasta, $r23S_fasta, $prefix) 
    = ($a[21],    $a[23],        $a[24],      $a[25],      $a[26]);
  die "Not all info found at $_\n" 
    unless ($shortName && $proteomefile && $prefix);
  push @orgs, $shortName;
  # Open fasta files for each organism, including rRNAs if wanted
  my @fastas = ($proteomefile);
  if ($dorrnas){
    foreach my $file ($r16S_fasta, $r23S_fasta){
      if ($file && $file ne "-"){
	push @fastas, $file;
      }
    }
  }
  foreach my $fasta (@fastas){
    my $fastaIn = Bio::SeqIO->new(-file => $fasta);
    while (my $seq = $fastaIn->next_seq){
      my $id = $seq->id;
      # if ($shortName eq 'Sinorhizobium_fredii_NGR234' && $id eq "ACP24710.1"){
      # 	print "HERE\n";
      # }
      if ($seqs{$id}){
	$seqs{$id} = $seq;
	$nids++;
	# Check that ids are unique, that a previous record is not erased
	die "Non-uniqe ids: $id is present in $fasta file and one more file"
	  if $uids{$id};
	$uids{$id}++;
      }
    }
  }
}
print STDERR "recorded $nids ids in $cnt fastas\n";


## Check that everyone's here
foreach my $key (keys %seqs){
  #print "Key: $key, value: ", $seqs{$key}, "\n";
  if (!ref($seqs{$key}) && !$seqs{$key}->isa("Bio::Seq")) {
    die "Seq $key has no fasta record associated\n";
  }
}

## Dispatch the sequences in their respective files; recode description to 
# get the name in it
print STDERR "Dispatching fastas...\n";
my $nhmms = 0;
foreach my $hmm (keys %map){
  $nhmms++;
  my ($norgs, $npids) = (0, 0);
  print STDERR "  working on HMM $hmm...";
  my $fastaOut = Bio::SeqIO->new(-file => ">$fastaDir/$hmm.fasta",
				 -format => 'fasta');
  foreach my $org (@orgs){
    if ($map{$hmm}{$org}){
      $norgs++;
      foreach my $pid (keys %{ $map{$hmm}{$org} }){
	$npids++;
	my $seq = $seqs{$pid};
	#my $oldid = $seq->id;
	#$seq->id($org);
	$seq->desc($org . "|" . $seq->desc);
	$fastaOut->write_seq($seq);
      }
    }
  }
  print STDERR " found $npids markers in $norgs organisms\n";
}
print STDERR "Done for $nhmms HMMs, data is in $fastaDir\n";

