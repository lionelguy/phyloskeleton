#!/usr/bin/perl -w

=head1 runRaxmlStandard.pl

=head2 USAGE

runRaxmlStandard.pl [-m raxml_model] [-f output_folder] [-t number_threads] [-s] [-b binary-suffix] [-o other_args] [-d] [-q] aln_files

-m raxml_model: see raxmlHPC-PTHREADS-SSE3 -h for more info
-f output folder: output folder to store results
-t number_threads: number of threads
-s: use slow, non-parametric bootstraps. This implies running three
    raxml commands (bootstraps, best tree and joining)
-b binary: which binary to use. Give only what comes after 'raxmlHPC-', e.g.
    PTHREADS-AVX2 (default).
-o other_args: Other arguments to pass to RAxML, e.g. '-q partitions' to 
    perform partitioned run. Protect with quotes. If it links to files, 
    keep in mind that runRaxmlStandard.pl goes into the -f folder so give
    the right relative paths (in general just add ../ in front of the file)
-d: dry run? Just prints the raxml command
-q: quiet? Silences raxml output

=cut

use strict;
use File::Basename;
use Getopt::Std;
use Bio::AlignIO;

our $opt_m = "GTRGAMMA";      # Raxml model
our $opt_f = "raxml";         # output folder
our $opt_t = 3;               # number of threads
our $opt_s;                   # slow bootstraps
our $opt_b = "PTHREADS-AVX2"; # specific brand of raxml to use
our $opt_o = '';              # other arguments to pass to raxml. Quote-protect
our $opt_d;
our $opt_q;
&getopts('m:f:t:b:o:sd');
usage() unless @ARGV;

mkdir($opt_f) unless -d $opt_f;


my (@alnFiles) = @ARGV;
foreach my $aln (@alnFiles){
    # remove suffixes
    my @suffix = qw(\.aln \.kaln \.fasta);
    my ($name, $path, $suffix) = fileparse($aln, @suffix);
    $name =~ s/\.fasta$//g;
    print STDERR "\n\n$name: converting";
    # output "relaxed phylip" format
    my $in  = Bio::AlignIO->new(-file   => "$aln",
				-format => "fasta");
    my $out = Bio::AlignIO->new(-file   => ">$opt_f/$name.phy",
				-format => "phylip",
				-idlength => 60);
    while( my $aln = $in->next_aln() ){ $out->write_aln($aln); }
    # perform raxml; have to cd to the folder, otherwise does segfault
    chdir($opt_f) or die "Cannot chdir to $opt_f\n";
    ## Build command
    # binary
    my $bin;
    if ($opt_t > 1){
	$bin = "raxmlHPC-$opt_b -T $opt_t";
    } else {
	$bin = "raxmlHPC";
    }
     # if slow bootstrap, has to perform 3 distinct operations
    my $cmd;
    if ($opt_s){
	# Bootstraps
	$cmd = "$bin -b 12345 -N 100 -p 12345 -m $opt_m -s $name.phy " . 
	    " -n $name.bs $opt_o\n";
	# Best tree
	$cmd .= "$bin -p 12345 -m $opt_m -s $name.phy -n $name.best $opt_o\n";
	# Merge
	$cmd .= "$bin -f b -m $opt_m -s $name.phy -z RAxML_bootstrap.$name.bs ".
	    "-t RAxML_bestTree.$name.best -n $name $opt_o";
    } else {
	$cmd = "$bin -f a -x 12345 -N 100 -p 12345 -m $opt_m " . 
	    "-s $name.phy -n $name $opt_o";
    }
    if ($opt_q) {
	$cmd .= " > /dev/null"
    }
    print STDERR "; running raxml\n";
    print "$cmd\n";
    system($cmd) unless ($opt_d);
    chdir("..") or die "Cannot chdir to ..\n";
#    die;
}

sub usage{
    system("perldoc $0");
    exit;
}
