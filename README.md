># phyloSkeleton #

Lionel Guy (<guy.lionel@gmail.com> / <lionel.guy@imbim.uu.se> / @LionelGuy)

## Contents ##

[TOC]

## Introduction  ##

With the current rate of genome sequencing, placing newly sequenced organisms in a robust phylogenomic tree is key to understanding their evolutionary origin and taxonomic placement. The major trade-off when drawing phylogenies from concactenated alignments is density vs. tractability: the tree should be as dense as possible, to correctly place new organisms, but the phylogenetic inference should be tractable using the best possible methods. 

phyloSkeleton allows the user to quickly pick up representative genomes from NCBI and JGI, with variable density in different parts of the tree, and to add user-generated sequences. It also allows to gather data, annotate (using prodigal or prokka) unannotated genomes, find selected panorthologs (using HMMer), and gather fasta files that can then be aligned and concatenated.

phyloSkeleton is still under development, everything might not work exactly as expected. Feedback, bug reports, feature requests are welcome through the issue system.

## Description ##

phyloSkeleton mainly works according to the following steps:

1. Gathering taxids and retrieving taxonomy information for all assemblies from NCBI (and JGI, if specified)
2. Selecting representative assemblies (see Selection priorities section below)
3. Gathering data online from NCBI (and JGI)
4. Annotating unannotated genomes (CDS and 16S/23S rRNAs if requested)
5. Searching the proteomes/genomes for specific marker genes
6. Gathering markers or marker domains in separate fasta files

The fasta files can then be aligned and concatenated.

One central idea in designing phyloSkeleton is also that this can be a recursive process: a first attempt can be done to only select assemblies (running with `--donotgather`), the resulting selection table (`<prefix>.selection.tab`) examined and potentially modified within a spreadsheet editor, re-including excluded genomes or excluding others. Then, the same pipeline can be run again using the selection table as `--selection-tab`, and the data gathered and annotated. Markers can be identified, and manually curated (`<prefix>.map`). The pipeline can be then run again using the curated map as input (`--marker-map`).

## Dependencies ##

### External software ###

Executables should be available in the user's `PATH`. In addition, it is recommended (but not necessary) that the user installs a multiple sequence alignment program and a phylogeny inference one. See the tutorial below.

#### prodigal ####

A gene prediction software by Doug Hyatt. Tested with v. 2.6.1. Download at: 
<https://github.com/hyattpd/Prodigal>


#### prokka ####

A rapid prokaryotic genome annotation programme by Torsten Seemann. Optional, can be replaced just by prodigal (see above). Tested with v. 1.12-beta. Download at:
<https://github.com/tseemann/prokka>.

#### HMMer ####

HMMER: biosequence analysis using profile hidden Markov models, by Sean Eddy and coworkers. Tested with v. 3.1b2. Available from <http://hmmer.org/>.

#### barrnap ####

BAsic Rapid Ribosomal RNA Predictor, by Torsten Seeman. Tested with v. 0.7 Download at <https://github.com/tseemann/barrnap>.

### Perl libraries ###

Required:

* Test::Simple (>= 1.0)
* File::Share (>= 0.25)
* Archive::Extract (>= 0.76)
* Text::CSV (>= 1.32)
* Parallel::ForkManager (>= 1.17)
* BioPerl (tested with 1.6.924). See <http://bioperl.org/INSTALL.html>

Recommended (required for some utilities):

* Time::HiRes for parseTaxo.pl
* Graph::Easy for colorTree.pl
* Bio::NEXUS for newick2nexus.pl

### Linux Utilities ###

* curl
* tar
* gzip
  
## Installation ##

1. First make sure that the dependencies that you require above are properly installed. 

### git ###

1. Choose an appropriate location, e.g. your home:

		$ cd $HOME
	
1. Clone the latest version of the repository:

		$ git clone http://bitbucket.org/lionelguy/phyloskeleton.git
    	$ ls phyloskeleton

1. Optionally, add the folder in your `PATH`. The scripts should be kept at their original location. 
1. Alternatively, the package can be built and installed as explained in the following section

### From source packages ###

1. Source packages are availble from Bitbucket: go to the [source](https://bitbucket.org/lionelguy/phyloskeleton/src) page and in the 'releases' folder. There are the sources, starting from version 0.8.

1. Download the package, untar it, go to the newly created folder:

        $ tar xvzt PhyloSkeleton-<version>.tar.gz
		$ cd PhyloSkeleton-<version>
		
1. Build and install the package. Testing will ask whether to run the network-dependent tests, which may fail becaues of a lack of internet connection. Installing might require to have root access. 

		$ perl Makefile.PL
		$ make
		$ make test
		$ make install

1. The source package has been built with ExtUtils::MakeMaker 7.24. Refer to its [manual](http://search.cpan.org/~bingos/ExtUtils-MakeMaker-7.24/lib/ExtUtils/MakeMaker/Tutorial.pod) for more information.
   
### Post installation steps ###

To further test the installation, go to the  `t` folder and run the `test.sh` script from the phyloSkeleton main folder. **WARNING**: this will download the taxonomy database (35 MB), the summary of all genome assemblies (20 MB), and many genomes from the NCBI (>50 MB). The full run will take a few minutes, depending on the speed of the internet connection.

	$ cd t
	$ bash test.sh

## Quick example (example 0) ##

1. In this quick example, we'll do a quick phylogeny of Cyanobacteria (taxid:1117), using one representative per family. We'll assume phyloSkeleton is installed in `$HOME/bin/phyloSkeleton/`, and that all dependencies were installed. We'll also assume that `mafft` and `RAxML` are installed and available in the path.

		$ PS_HOME=$HOME/bin/phyloSkeleton
		$ perl $PS_HOME/phyloSkeleton.pl \
			--output-folder example0 \
			--result-prefix cyanos \
			--ncbi-selection $PS_HOME/share/cyanos.csv \
			--ingroup Cyanobacteria \
			--ingroup-level phylum \
			--ingroup-selection-level family \
			--best-match-only

1. This commands wil output results in the folder (`--output-folder`) 'example0' with the prefix (`--result-prefix`) 'cyanos', using the assembly selection table (`--ncbi-selection`) 'cyanos.csv' shipped with phyloSkeleton. A similar file can be downloaded from the NCBI (see in the other examples below). It samples the phylum (`--ingroup-level`) Cyanobacteria (`--ingroup`) at family (`--ingroup-selection-level`) level, and retrieves only the best match for each marker in each organism. By default (not explicitly shown in the options) it uses 15 ribosomal proteins as markers (see later). 

1. After running phyloSkeleton, let's have a look at the resulting files, and let's make a list of the markers and align them separately, using xargs to better use multi-core machines.

		$ cd example0
		$ ls
		$ mkdir mafft trimal
		$ tail -n+2 result.map | cut -f2 | sort | uniq > hmm.list
		$ cat hmm.list | xargs -I {} -n1 -P3 sh -c 'mafft-linsi --anysymbol --thread 4 "cyanos.fasta/$1.fasta" > "mafft/$1.mafft"' -- {}

1. Concatenate the results, using the utility `concatenateRenameAlignment.pl`, and the recently obtained map and the aligned fasta as input.

		$ perl $PS_HOME/utils/concatenateRenameAlignment.pl \
			--mapping cyanos.map \
			mafft/*.mafft \
			> cyanos_concat.fasta

1. Run RAxML (through the provided wrapper `runRaxmlStandard.pl`) on 3 threads under the PROTGAMMALG mode, using the output folder `raxml_cyanos_concat`. This will take a few minutes to complete.
	
		$ runRaxmlStandard.pl -m PROTGAMMALG -t 3 -f raxml_cyanos_concat cyanos_concat.fasta

1. The resulting newick tree can be visualized with FigTree, for example:

		$ figtree raxml_cyanos_concat/RAxML_bipartitions.cyanos_concat &


## Detailed examples ##

These examples aims at obtaining a small but reliable phylogeny of the gammaproteobacterial order Legionellales, using a concatenation of 15 ribosomal proteins and both 16S and 23S rRNA sequences.

* **Example 1** shows how to gather data from NCBI and to modify the initial selection and run the pipeline again.
* **Example 2** shows how to use data from NCBI and JGI and how to remove paralogs.
* **Example 3** shows how to use user-defined data only.
* **Example 4** combines data from NCBI, JGI and user-defined data.

An example of data post-processing (aligning, concatenating, running a tree and coloring it is provided for data from example 2, using the following additional software:

1. [**mafft**](http://mafft.cbrc.jp/alignment/software/) to perform multiple sequence alignment.
1. [**trimAl**](http://trimal.cgenomics.org/) to trim poorly aligned parts of an alignment.
1. [**RAxML**](http://sco.h-its.org/exelixis/web/software/raxml/index.html) to infer a maximum-likelihood phylogenetic tree. We'll run a partitioned phylogeny, with one partition for each gene, following LG model for amino acids and GTR for DNA.
1. [**FigTree**](http://tree.bio.ed.ac.uk/software/figtree/) to display the obtained trees. 

All these are necessary to run the tutorial end-to-end, but each one can be replaced by another similar software. 

See also the `t/test.sh` file.

### Preparation step: Folder and scripts ###

1. Prepare a folder to store data, for example in your home, and go to that folder. We are also creating a `genomicData` folder to store genomes and proteomes locally and a `taxonomy` folder to store taxdump from NCBI.

		$ mkdir $HOME/phyloSkeletonTutorial
	    $ cd $HOME/phyloSkeletonTutorial
		$ mkdir genomicData taxonomy
	
1. Unless phyloSkeleton has been installed with `make install`, add the phyloSkeleton main folder and its utilites subfolder to your PATH variable, either temporarily or by adding it to your .bashrc or .bash_profile files. For example, if the phyloSkeleton folder is in $HOME/bin:

        $ export PATH=$PATH:$HOME/bin/phyloSkeleton::$HOME/bin/phyloSkeleton/utils

### Example 1: Gather data from NCBI, modifying the selection ###

#### Download genome browser tab from NCBI ####

1. Go to <http://www.ncbi.nlm.nih.gov/genome/browse/>
1. Type in "Legionellales (taxid:118969)" in the search box under "Genome information by organism". Push enter or click on "Search by organism".
1. Click on the "Prokaryotes" tab.
1. Click on "Download selected records", on the right side of the page. Save the resulting page in the newly created folder in Excel (.csv) format, as "legionellales_ncbi.csv".
1. **WARNING**: phyloSkeleton expects unique assembly IDs for each assembly and will break otherwise. It appears that a few assemblies retrieved that way have no assembly ID, and should be removed. It can conveniently be done as follows:

        $ perl -i.bak -F, -lane 'print unless $F[7] eq "-"' legionellales_ncbi.csv

1. You can instead copy a file from the `share` folder

		$ cp $HOME/bin/phyloSkeleton/share/legionellales.csv legionellales_ncbi.csv

#### Preparing the selection levels file ####

1. Prepare `selLevels.tab`: we can use the file in share/selLevels.tab, which will select at class level by default (should not be necessary since we sample only in one order), at family level in the Legionellales order, at species level in the Legionellaceae family, and all Tatlockia isolates ('name' level). 

1. The file should have a title with the following column names: 'group' (a taxon name), 'groupLevel' (taxonomic rank of the group, e.g. class, phylum) and 'selectionLevel' (a lower taxonomic level, e.g. genus). If the last element is 'name', then all organisms will be selected. The file looks like this:

		#group          groupLevel  selectionLevel
		default         -           class
		Legionellales   order       genus
		Legionellaceae  family      species	
		Tatlockia       genus       name
		
1. Save it under `selLevels.tab` (or copy it from the share folder):

		$ cp $HOME/bin/phyloSkeleton/share/selLevels.tab .

#### Run phyloSkeleton to select assemblies ####

First, we are only selecting the assemblies, without gathering any data. 

1. We choose to output results in the `example1` folder with the prefix `legio1`, using the NCBI selection tab that we just prepared. We also choose to store the `assembly_summary_genbank.txt`, which is downloaded automatically, in the `genomicData` folder, for later reuse. The taxonomy dump files, downloaded automatically from NCBI, will be stored in `taxonomy` folder, also for later reuse. The parsed taxonomy information for the assemblies will be stored in the `example1/taxids.tab` file. We are using selection rules in `selLevels.tab` and we are not gathering

		$ phyloSkeleton.pl \
			--output-folder example1 \
			--result-prefix legio1 \
			--ncbi-selection legionellales_ncbi.csv \
			--ncbi-all-projects genomicData/assembly_summary_genbank.txt \
			--taxonomy-path taxonomy \
			--taxonomy-table example1/taxids.tab \
			--levels selLevels.tab \
			--donotgather

1. Let's go into the folder and look at the resulting files, particularly at selection table `legio1.selection.tab`. Let's remove all species of Legionella which start with a 'w', by changing the `0_included` in the 'excluded' column by `9_user`. 

		$ cd example1
		$ ls
		$ oocalc legio1.selection.tab &
		$ perl -pe 's/(.*)(0_included)(.*Legionella w.*)/${1}9_user$3/' legio1.selection.tab > legio1_edited.selection.tab
		$ cd ..

#### Re-run phyloSkeleton to gather data, annotate and find markers ####

1. Now rerun phyloSkeleton.pl with the modified selection table `legio1_edited.selection.tab`and run the whole pipeline. We select the same output folder but use a different prefix, and store the genomic data in the fodler created above (`genomicData`). We will keep only the best match in each genome. We run analyses on three threads. 

		$ phyloSkeleton.pl \
			--output-folder example1 \
			--result-prefix legio2 \
			--selection-tab example1/legio1_edited.selection.tab \
			--genomic-data-path genomicData \
			--taxonomy-path taxonomy \
			--taxonomy-table example1/taxids.tab \
			--best-match-only \
			--cpus 3 
		
1. Have a look at the resulting files: 
   * The updated selection is `legio2.selection.tab`.  
   * Alignments from hmmer are in `legio2.hmm_aligns`
   * Single-gene marker maps in `legio2.single_marker_maps`. 
   * Fasta files containing markers are in `example1/legio2.fasta`. 
   * The other files show number and evalue of homologs for each marker and organism (`legio2.n.matrix` and `legio2.eval.matrix`, respectivley). 
   * The file `legio2.map` contains a map of protein ids, organisms and markers and can be input in `concatenateRenameAlignments.pl` after multiple alignment.

		$ cd example1
		$ ls

### Example 2: Gather data from NCBI and JGI, removing paralogs ####

#### Download genome list from JGI ####

1. Go to <https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=FindGenomes&page=genome>. Log in if necessary.
1. In the Keyword field, enter "Proteobacteria", and select "Phylum" i the Filters field. Click "Go".
1. At the bottom of the page, in the "Table Configuration" section, select the following columns (at least, but feel free to add more), and click on "Redisplay":
    * NCBI Taxon ID
	* JGI Project ID / ITS PID
    * Is Public
    * Bioproject Accession
    * Biosample Accesssions
    * Scaffold count

1. Then filter again, by entering "Legionellales" in the "Filter" field, and choosing "Order" in the "Filter column". Click on apply.
1. Click on "Select All" (takes a while) then on "Export". Save the file as "legionellales_jgi.tab".
1. The file can also be copied from the `share` folder: 

		$ cp $HOME/bin/phyloSkeleton/share/taxontablejgi.xls legionellales_jgi.tab

#### Running the phyloSkeleton pipeline ####

We'll be running phyloSkeleton as before, but this time using the JGI as a source, providing the file we just downloaded as selection table (`--jgi-selection`), and credentials to the JGI (`--jgi-login` and `--jgi-password`). We'll also include the rRNAs in the analysis (`--annotate-rrnas`):

	$ phyloSkeleton.pl \
		--output-folder example2 \
		--result-prefix legio1 \
		--genomic-data-path genomicData \
		--taxonomy-path taxonomy \
		--taxonomy-table example2/taxids.tab \
		--ncbi-selection legionellales_ncbi.csv \
		--ncbi-all-projects genomicData/assembly_summary_genbank.txt \
		--jgi-selection legionellales_jgi.tab \
		--jgi-login <jgi_login> \
		--jgi-password <jgi_password> \
		--levels selLevels.tab \
		--annotate-rrna
 		   
The whole JGI part can be skipped by removing the --jgi-* options. 


#### Looking at results and fixing paralog issues ####

1. Have a look at some of the result files in the output folders. The `legio1.selection.tab` contains all assemblies, including a 'excluded' column that tells whether the assembly has been included; Two matrices `legio1.n.matrix` and `legio1.eval.matrix`, which contain the number of hits over the threshold per marker and per organisms, and the actual evalues, respectively:

		$ cd example2
		$ oocalc legio1.selection.tab &
		$ oocalc legio1.n.matrix &
		$ oocalc legio1.eval.matrix &
		
1. Have also a look at the map file:

		$ oocalc legio1.map &
		
1. L. tunisiensis has two good hits for Ribosomal_L14. Examine the alignments for this marker: 

		$ cat legio1.single_marker_maps/Ribosomal_L14.map
		$ perl -lane 'print if ($F[4] > 1)' legio1.single_marker_maps/Ribosomal_L14.map
		$ sed -n '/Query:.*Ribosomal_L14/,/Query/p' legio1.hmm_aligns/Legionella_tunisiensis_LegM.ali 
		
1. CALJ01000291.1\_9 and CALJ01000291.1\_10 are located next to each other and match two ends of the Ribosomal_L14 marker, the second one between residues 1 and 55, the first one between residues 58 and 122 (out of 122). It is relatively safe to assume that there is a frameshift in this open reading frame. Whether the frameshift is real or a sequencing artefact remains to be decided, but for phylogenomic purposes it would be wise either to completely remove the marker or to fuse the proteins. Let's draw a single-gene tree anyway (i.e. align and run RAxML):

		$ cd legio1.fasta
		$ mafft-linsi --anysymbol --thread 4 Ribosomal_L14.fasta > Ribosomal_L14.mafft
		$ runRaxmlStandard.pl -m PROTGAMMALG -t 4 -f . Ribosomal_L14.mafft
		
1. Now examine the result with figtree:

		$ figtree RAxML_bipartitions.Ribosomal_L14.mafft &
		
1. Although the tree is not very infromative (support values are quite low due to the very short alignment, the two copies are not completely away from each other, which makes it likely that it is not a distant paralog that was picked up by HMMer. It is however safer to just remove the whole hit in the general map.

		$ cd ..
		$ grep -v -e "Legionella_tunisiensis.*Ribosomal_L14" legio1.map > legio1.edited.map	
		$ cd ..
	
#### Generating the fasta files again  ####

Now run phyloSkeleton again, using the selection table produced in the first run and the markers map that we just edited.

	$ phyloSkeleton.pl \
		--output-folder example2 \
		--result-prefix legio2 \
		--selection-tab example2/legio1.selection.tab \
		--genomic-data-path genomicData \
		--marker-map example2/legio1.edited.map \
		--annotate-rrnas


#### Aligning and running phylogenies ####

##### Align with mafft and trim alignments with trimal  #####

1. First prepare the folders, and list the hmms:

		$ cd example2
		$ mkdir mafft trimal
		$ tail -n+2 legio1.edited.map | cut -f2 | sort -u > hmm.list
		
1. Align with mafft-linsi. To better distribute work on many-core systems, this is done with xargs, so that each mafft instance is running on cores, running 3 instances in parallel (-P3): 

		$ cat hmm.list | xargs -I {} -n1 -P3 sh -c 'mafft-linsi --anysymbol --thread 4 "legio2.fasta/$1.fasta" > "mafft/$1.mafft"' -- {}

1. Trim with trimal, with automated1 setting, with the same xargs command, running 12 trimal each on 1 core.

		$ cat hmm.list | xargs -I {} -n1 -P12 trimal -in mafft/{}.mafft -out trimal/{}.trimal.fasta -fasta -automated1

##### Concatenate alignments (concatenateRenameAlignment.pl) #####

1. Attribute the correct gene model to each alignment (i.e. LG for proteins and DNA for rRNAs):
   
		$ awk '{if ($1 ~ "S_rRNA"){model="DNA"} else {model="LG"}; print $1"\t"model}' hmm.list > geneModels.tab

1. Use the markers.mapping file to map back proteins to their right organism and the geneModels tab to produce a partition table useable by raxml:

		$ concatenateRenameAlignment.pl \
			-m legio1.edited.map \
			-k 3,1 \
			-p partitions.raxml \
			-g geneModels.tab \
			-s .trimal.fasta \
			trimal/* \
			> concat.fasta
			
1. At the end of the run, the script gives the amount of missing data, and the number of alignments and species. Check that these are right.

##### Run maximum-likelihood with RAxML #####

1. Using the wrapper `runRaxmlStandard.pl`, inputting the parition file

		$ runRaxmlStandard.pl -m PROTGAMMALG -t 15  -o "-q ../partitions.raxml" -f raxml_cat_lg_concat concat.fasta
		
1. See the result in the tree folder:

		$ ls raxml_cat_lg_concat/
		
1. Show the tree before applying colors

		$ figtree raxml_cat_lg_concat/ RAxML_bipartitions.concat &

#### Showing tree ####

##### Preparing colors (colorTree.pl & figtreeAddColors.pl) #####

1. The tree is colored in the following fashion: 
   1. Genera of the order Legionellales are colored in hues of blue
   1. Species of the Legionella genus are colored in hues of red
   1. All organisms with species name starting with "w" are in green (note the Perl regexp `.*_w.*`, that should match the whole name).
   
1. The corresponding `legio.color.conf` file should look like this (tab-separated):

        #group  column  value         color toneBy
		default order   Legionellales blues genus	
		legios  family  Legionella    reds  species
		w       name    .*_w.*        green	
		
1. Make sure the fields are separated by tabs, copy/pasting can replace them by spaces. There is a version of the color.conf in the test folder of the distribution:
	
		$ cp $HOME/bin/phyloSkeleton/share/color.conf .
	
1. The available colors and palettes (or set of hues or tones; available for display at <http://colorbrewer2.org/> are displayed with 

		$ colorTree.pl --show-colors
		$ colorTree.pl --show-palettes

1. Run the colorTree.pl file, using the selection tab and this color.conf as inputs:

		$ colorTree.pl -r color.conf legio2.selection.tab > colors.tab

##### Showing the colored tree (FigTree) #####

1. First, the tree (in Newick format) must be converted to nexus format:

		$ newick2nexus.pl -r -i raxml_cat_lg_concat/RAxML_bipartitions.concat > raxml_cat_lg_concat/RAxML_bipartitions.concat.nex
		
1. The tree is then colored and displayed:

		$ figtreeAddColors.pl -p colors.tab -i raxml_cat_lg_concat/RAxML_bipartitions.concat.nex -o raxml_cat_lg_concat/RAxML_bipartitions.concat.colored.nex
		$ figtree raxml_cat_lg_concat/RAxML_bipartitions.concat.colored.nex &
	
### Example 3: using user-defined data and using a different set of markers ###

#### Preparing a file with your data ####

1. In this example, we cover a number of different ways to add your data, but in the simplest case, where you only want to add genomes stored locally, the necessary columns are 'assembly' (a unique id), 'source' (e.g. 'local'), 'name', and 'genomeFile'.

1. Here we play on 6 variations of local genomes, one from NCBI and one from JGI. The "easy" case mentioned above is X3. To use this, you will need to correct the path to `share`.

		proteomeFile	ftp_path	expected_result
		X1	local	Hodgkinia cicadicola A	573658	-	-	-	-	-	excluded
		X2	local	Hodgkinia cicadicola Dsem	-	-	-	../share/genomes/CP001226.fna	-	-	included_with_warnings
		X3	local	Hodgkinia cicadicola TETULN	-	Bacteria	11	../share/genomes/CP008699.fna	-	-	included
		X4	local	Hodgkinia cicadicola TETUND1	573658	-	-	../share/genomes/CP007232.fna	-	-	included_with_taxo
		X5	local	Nasuia deltocephalinicola str NAS ALF	-	-	-	../share/genomes/CP006059.fna	../share/genomes/CP006059.faa	-	included
		X6	local	Nasuia deltocephalinicola strain PUNC	-	-	-	-	../share/genomes/CP013211.faa	-	included
		GCA_001693655.1 	genbank	Ketogulonicigenium vulgare	-	-	-	-	-	ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/001/693/655/GCA_001693655.1_ASM169365v1	included
		2693429884	jgi	Roseovarius azorensis DSM 100674	1244108	-	-	-	-	-	included

1. The file can be copied from `share`: 

		$ cp $HOME/bin/phyloSkeleton/share/userGenomes.tab .

#### Running phyloSkeleton ####

1. We run phyloSkeleton with an extended set of markers, 'Bact109'. It would be also possible to point to a file containing hmm profiles, but 'RP15', 'Bact109', 'Bact139', 'Arch137' and 'Arch162' are reserved words corresponding to sets of profiles shipped with phyloSkeleton. They are located in `share`. The JGI credentials are required because we want to use one specific assembly from JGI.

		phyloSkeleton.pl \
			--output-folder example3 \
			--result-prefix userData \
			--taxonomy-path taxonomy \
			--selection-tab userGenomes.tab \
			--jgi-login <jgilogin> \
			--jgi-password <jgipasswd> \
			--hmms Bact109 \
			--completeness 0.3

### Example 4: Mixing user-defined data and a selection from NCBI and JGI ###

1. In this last case, we combine all data source.

		phyloSkeleton.pl \
			--output-folder example4 \
			--result-prefix legionellales \
			--genomic-data-path genomicData \
			--selection-tab userGenomes.tab \
			--genomic-data-path genomicData \
			--taxonomy-path taxonomy \
			--taxonomy-table example4/taxids.tab \
			--ncbi-selection legionellales_ncbi.csv \
			--ncbi-all-projects genomicData/assembly_summary_genbank.txt \
			--jgi-selection legionellales_jgi.tab \
			--jgi-login $JGILOGIN \
			--jgi-password $JGIPW \
			--levels selLevels.tab \
			--annotate-rrnas \
			--best-match-only \
			--cpus 3


## Manual for phyloSkeleton.pl ##

This is from the POD documentation of phyloSkeleton.pl, and can be accessed through

	$ perldoc phyloSkeleton.pl 
	

### SYNOPSIS

Starting an analysis from scratch:

phyloSkeleton.pl 
    \[-o|--output-folder <folder>\] 
    \[-p|--result-prefix <string>\] 
    \[-s|--selection-tab <file> | -n|--ncbi-selection <file>\]
    \[-m|--marker-map <file>\]
    \[-a|--ncbi-all-projects <file>\] 
    \[-J|--jgi-selection <file> -L|--jgi-login <login> -P|--jgi-password <pw>\] 
    \[-t|--taxonomy-table <file>\] 
    \[-r|--ranks <rank1,rank2,...>\] 
    \[-T|--taxonomy-path <folder>\] 
    \[-G|--genomic-data-path <folder>\] 
    \[--ingroup <string> --ingroup-level <string>
      \[--ingroup-selection-level <string> ---outgroup-selection-level <string>\]
        | -l|--levels <file>\] 
    \[-A|--annotation-software <string>\] 
    \[-N|--annotate\_rrnas\] 
    \[-f|--force-annotate\] 
    \[-H|--hmms <file>\] 
    \[-e|--evalue-threshold <real>\] 
    \[-c|--completeness-threshold <real>\] 
    \[-b|--best-match-only\] 
    \[-R|--refresh-if-older\] 
    \[--donotgather\] \[--donotannotate\] \[--donotattribute\]  
    \[-C|--cpus <integer>\] 
    \[--threads-per-process <integer>\]

Updating:

phyloSkeleton.pl -s|--selection-tab <file>

### INPUT

- **-o**|**--output-folder** _folder_

    Folder to store output files. Created if not existing. See output for the details of the output files.

- **-p**|**--result-prefix** _string_

    A prefix given to result files and folders, at the maker attribution stage. Default: 'result'.

- **-s**|**--selection-tab** _file_

    File containing user-selected assemblies to add to the selection. Tab-separated file with a header and the following columns (at least): 

    - 'assembly': a unique id
    - 'name': a name representative of the organism
    - 'source': can be either 'local', 'genbank' or 'jgi'
    - 'genomeFile' or, if the source is 'genbank', a 'ftp\_path' column or nothing if source is 'jgi'

    Optional columns can all be included at user's discretion, but a few columns are worth mentioning:

    - taxid: if this one is set, the following columns will be added: gcode, some "ranks" columns (corresponding to the '-r|--ranks' option), taxostring
    - gcode: if no taxid is present and the organism doesn't have a standard genetic code (11).
    - selectionGroup: useful to group organism and color them later
    - genomeFile and proteomeFile: very useful for local data: point to a fasta file containing the genome and proteome, respectively.
    - shortName: will be set automatically if not set.

- **-m**|**--marker-map** _file_

    A marker map, as output by the pipeline and (eventually) modified by the user to remove paralogs. Running this script with a map and a corresponding --selection-tab file will bypass the whole selection, annotation and HMM attribution sections and only create fasta files. The user will be warned if there are markers with more than one copy per organism. 

- **-n**|**--ncbi-selection** _file_

    Tab-separated file obtained from NCBI genome browser. See below for details.

- **-a**|**--ncbi-all-projects** _file_

    Tab-separated file from NCBI Genbank (`assembly_summary_genbank.txt`). See below for details.

- **-J**|**--jgi-selection** _file_

    Tab-separated file with a selection of genomes from JGI. See below for details.

- **-L**|**--jgi-login** _string_ and **-P**|**--jgi-password** _string_

    Credentials to access the JGI website. Transmitted in clear.

- **-t**|**--taxonomy-table** _file_

    This option can be used as input or output. When running phyloSkeleton.pl for the first time with a given set of assembly lists (see above), use it as output, pointing to a non-existing file. The taxids contained in the assembly lists will be parsed and a taxonomy table will be output. In subsequent runs of phyloSkeleton.pl with the same assembly lists, the taxonomy table can be used as input, to skip parsing the taxids, which can be time consuming.

- **-r**|**--ranks** _rank1,rank2,..._

    Comma-separated list of ranks to parse, that can be used for selection. Any of the ranks used in --levels or the selection criteria should be present here. By default: superkingdom,phylum,class,order,family,genus,species.

- **-T**|**--taxonomy-path** _folder_

    A folder to store taxdump files from NCBI, or where existing files already are. By default current folder (not recommended). 

- **--ingroup** _clade_

    An clade representing an ingroup.

- **--ingroup-level** _rank_

    The rank of the ingroup.

- **--ingroup-selection-level** _rank_

    The rank at which to select the ingroup from. Genus by default.

- **--outgroup-selection-level** _rank_

    The rank at which to select the outgroup from. Family by default.

- **-l**|**--levels** _file_

    A tab-separated file describing at which rank taxon should be sampled, with a title with the following column names: 'group' (a taxon name), 'groupLevel' (taxonomic rank of the group, e.g. class, phylum) and selectionLevel (a lower taxonomic level, e.g. genus). If the last element is "name", then all organisms will be selected. 

    Example (file must be tab-separated, beware of copy-pasting): 

         #group          groupLevel  selectionLevel
         default         -           class
         Legionellales   order       family
         Legionellaceae  family      genus
         Tatlockia       genus       species
         Fluoribacter    genus       name

- **-G**|**--genomic-data-path** _folder_

    Where to store genomic data. If --ncbi-all-projects is not set, then the downloaded file will be stored there too. By default: genomicData folder in the current folder.

- **-A**|**--annotation-software** _string_

    The annotation software to use. Either 'prodigal' (default) or 'prokka'. The corresponding program must be present on the user's path.

- **-A**|**--annotate-rrnas**

    Boolean. Should rRNA genes (16 and 23S) also be annotated?

- **-f**|**--force-annotate**

    Boolean. Should genomes already annotated be annotated again? (Doesn't apply to annotations downloaded from NCBI or JGI).

- **-H**|**--hmms** _file_

    A file containing HMM profiles of the markers to use in the marker search phase. These profiles can be generated by the user using the tools available in HMMer, particularly hmmbuild. By default, uses a set of 15 "universal" ribosomal proteins. This collection, as well as two bacteria- and archaea-specific ones, are located in the distribution folder, in the 'share' subfolder. They can be accessed using the reserved variable 'RP15', 'Bact109', 'Bact139', 'Arch137' and 'Arch162', respectively.

- **-e**|**--evalue-threshold** _real_ 

    E-value threshold to include matches for markers. Default: 1e-10.

- **-c**|**--completeness-threshold** _real_

    Completeness threshold to include genomes in the analysis: genomes that include a lower fraction of genes will be excluded. Default: 0.8.

- **-b**|**--best-match-only**

    Should only the best match be included in the main map? This allows to directly run the pipeline to the end and run the multiple alignment files, but it's done at one's own risks: a better paralog might have a slightly higher e-value.

- **-R**|**--refresh-if-older** _integer_

    If files automatically downloaded (taxdump and assembly\_summary\_genbank.txt), refresh them if older than this number of days. Default: 7.

- **--no-excluded**

    If set, assemblies not selected will not be printed. Otherwise, all assemblies are printed and the step at which they were excluded is indicated in the column 'excluded'.

- **--donotgather**

    Boolean. If set, doesn't gather data from external sources (and doesn't go further in the pipeline). Useful to get a first idea of the assembly selection.

- **--donotannotate**

    Boolean. If set, doesn't annotate the genomes (and doesn't go further in the pipeline). Useful to get a first idea of the assembly selection, and how much is available online.

- **--donotattribute**

    Boolean. If set, doesn't search ofr marker genes (and doesn't go further in the pipeline). Useful to get an idea of the assembly selection, and how much is available after annotation.

- **--donotcreatefasta**

    Boolean. If set, doesn't create fasta files per marker. 

- **-C**|**--cpus** _integer_

    Number of CPUs to use to annotate and to search for markers. The program uses Parallel::ForkManager to annotate several genomes in parallel. Default: 4.

- **--threads-per-process** _integer_

    Number of threads to use per process. If --cpus is set to 15 and --threads-per-process to 3, then 5 processes (of prokka or barrnap) will be launched in parallel. Default: 4.

- **-h**|**--help**

    Displays help message

### OUTPUT (in --output-folder)

#### Selection file (<prefix>.selection.tab)

A tab file file with the selection result. It includes a 'excluded' column that tells whether the assembly is still considered ('0\_included') or whether it has been rejected. The stage at which an assembly has been excluded is noted ('1\_...', '2\_...', etc) and a brief reason why is given.

#### Taxonomy table (--taxonomy-table)

The file is parsed from NCBI's taxdump using all taxids from the above files. Once the original input files have been parsed once and a taxonomy table has been created with --taxonomy

#### Marker maps

##### <prefix>.map

A five-column tab-separated file that gives, for each found marker homolog, 

1. Which organism it belongs to 
1. Which marker it is homologous to
1. The protein id that can be found in the fasta files
1. The evalue of the match, as calculated by hmmersearch
1. The number of homologs for the same marker in the same organism.

##### <prefix>.n.matrix

A organism x marker matrix, giving the number of marker homologs.

##### <prefix>.eval.matrix

A similar organism x marker matrix, giving the evalues 

#### Fasta folder (<prefix>.fasta)

Contains the fasta files for each marker.

### DESCRIPTION

Select genomes from Genbank and JGI, picking up the best representatives at various taxonomic levels depending on clades.

#### Selection priorities

First, the RefSeq category (as decided by NCBI) is taken into account. A genome is preferred if it is:

1. Reference
2. Representative
3. None of the above

Then, the assembly level and the source are decisive:

1. Complete (NCBI)
2. Chromosome (NCBI)
3. Finished (JGI)
4. Chromosome with gaps (NCBI)
5. Scaffold (NCBI)
6. Contig (NCBI)
7. Permanent Draft (JGI)
8. Draft (JGI)

Finally, if there are more than one organism with the same assembly and representativity level, the one with the most proteins is included.

A drawback of the two-step selection system is that an incomplete representative genome could be favored over a complete, non-representative. 

#### Obtaining the input files

##### --ncbi-selection

This list (filtered following the user's choice) gives the range of the genomes selected. Go to the genome browser at NCBI:
[http://www.ncbi.nlm.nih.gov/genome/browse/#tabs-proks](http://www.ncbi.nlm.nih.gov/genome/browse/#tabs-proks)
Filter at will and download the selected records to a .csv file

**WARNING**: phyloSkeleton expects unique assembly IDs for each assembly and will break otherwise. It appears that a few assemblies retrieved that way have no assembly ID, and should be removed. It can conveniently be done as follows:

	$ perl -i.bak -F, -lane 'print unless $F[7] eq "-"' <file>.csv

##### --jgi-selection

This file shows all sequencing projects within the scope of the phylogenomic study. Go to
https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=FindGenomes&page=genomeSearch

1. Search for Bacteria or Archaea with the "Domain" filter or for a more specific Phylum ("Phylum" filter). Click on "Go".
2. After the search is done, it is possible to refine the taxonomic level by scrolling all the way down the page in the "Table Configuration" section and adding more taxonomy columns ("Phylum" to "Species"), clicking on "Display Genomes Again". It will then be possible to filter with those new columns.  
3. In the "Table Configuration" Section, add at least the following fields: 
  - NCBI Taxon ID
  - JGI Project ID / ITS PID
  - Is Public
  - Bioproject Accession 
  - Biosample Accesssions
  - Scaffold count 
4. Select all rows
5. Export to xls


## Contents of the package ##

Only a short description is provided here. For detailed information about each script, refer to its documentation, which can be in general obtained with:

    perldoc lib/Phyloskeleton.pm

The main script (see manual above) is phyloSkeleton.pl.

### Utilities ###

#### utils/csv2tsv.pl ####

Convert comma-separated file into tab-separated files. 

#### utils/concatenateRenameAlignment.pl ####

Concactenates aligned fasta files, either using a map file as produced by attributeHMMs.pl, or using ids in the file (if they are organism ids).

#### utils/colorTree.pl ####

Given a few rules (e.g. according to taxonomic distributions), attribute specific colors to organisms or tones of colors ("palettes") to groups of organisms.

#### utils/runRaxmlStandard.pl ####

A wrapper around RAxML, allows (among others) to run slow bootstraps and a ML tree in one command.

#### utils/newick2nexus.pl ####

Converts newick trees to nexus ones.

#### utils/figtreeAddColors.pl ####

Automatically attributes (groups of) colors to groups and subgroups, given (for example) a tab file giving the taxonomic ranks of a bunch of organisms.

#### utils/scripts_v0.5 ####

Scripts as used prior to v0.5. For historical reasons only. Might disappear without warning.

### Data (in 'share') ###

The share folder contains several sets of HMM profiles, among which:

1. **Arch162.hmm**, containing 162 HMM profiles of archaeal panorthologs. From [Rinke et al., 2013][rinke]
1. **Arch137.hmm**, containing 137 HMM profiles of archaeal panorthologs. , derived from the previous, but where profiles matching mostly the same genes were trimmed down. It is recommended to use Arch137 to avoid overemphasizing multi-domain proteins like the two subunits of the RNA polymerase B: in most archaea, using the standard Bact139 will result in retrieving the same protein 5 or 6 times for these two proteins, respectively.
1. **Bact139.hmm**, containing 139 HMM profiles of domains bacterial panorthologs. From [Rinke et al., 2013][rinke]
1. **Bact109.hmm**, containing 109 HMM profiles of bacterial panorthologs, derived from the previous, but where profiles matching mostly the same genes were trimmed down. It is recommended to use Bact109 to avoid overemphasizing multi-domain proteins like the two subunits of the RNA polymerase B: in most bacteria, using the standard Bact139 will result in retrieving the same protein 5 or 6 times for these two proteins, respectively.
1. **RP15.hmm**, containing the profiles of 15 ribosomal proteins often located in a single operon, useful for metagenomics analysis. Adapted from [Brown et al., 2015][brown], where the authors use 16 proteins.

It also contains sample input data for phyloSkeleton.pl:

1. **legionellales.csv**: an example of a selection of assemblies downloaded from NCBI, passed with '--ncbi-selection'.
1. **taxontablejig.xls**: an example of a selection of assemblies downloaded from JGI, passed with '--jgi-selection'.
1. **userGenomes.tab**: an example of user-provided assemblies, passed with '--selection-tab'.
1. **selLevels.tab**: an example of selection rules, to be passed with '--levels'.
1. **selection_test.tab**: an example of the output of phyloSkeleton.pl with '--donotgather', that is doing only the selection of representative assemblies. This file can be further used by inputting it with '--selection-tab' to gather data and find markers.
1. **taxids**: a list of taxids retrieved from legionellales.csv and taxontablejgi.xls.
1. **taxids.tab**: a taxonomy table with taxids, genetic code and ranks, that can be output by (and input to) phyloSkeleton.pl with option '--taxonomy-table'.
1. **test.map**: an example of the output of phyloSkeleton.pl, after marker identification. This file can be input again with '--marker-map'.

The folder also contains the phyloSkeleton logo files ('pictures').

#### t/test.sh & t folders and subfolders ####

The bash script test.sh contains the instructions to run several tests, which can be used as examples.

## Acknowledgments ##

* Thanks to Katarzyna Zaremba-Niedzwiedzka (katarzyna.zaremba@icm.uu.se) for her contribution to the figtreeAddColors.pl script.
* Thanks to Lisa Klasson, Eric Hugoson and Dennis Leenheer for their feedback and testing.
* The palettes used by colorTree.pl were designed by Cynthia Brewer.

## Citation ##

A description of phyloSkeleton is [published in Bioinformatics][phyloPaper]. Please use the following 

	@article{
		doi:10.1093/bioinformatics/btw824,
		author = {Guy, Lionel},
		title = {phyloSkeleton: taxon selection, data retrieval and marker identification for phylogenomics}
		journal = {Bioinformatics},
		year = {2017},
		volume = {33},
		number = {8},
		pages = {1230-1232},
		doi = {10.1093/bioinformatics/btw824},
		URL = { + http://dx.doi.org/10.1093/bioinformatics/btw824},
		eprint = {https://academic.oup.com/bioinformatics/article-pdf/33/8/1230/13146921/btw824.pdf}
	}

## FAQ ##

None yet.

## Caveats ##

* attributeHMMs.pl picks the first hit above the threshold as the true homolog, which can be very wrong. If a marker has several copies in a specific genome, it might pick the wrong one.
* Be gentle while running gatherData.pl, since it makes many requests to the NCBI and JGI websites. Do not overload them, or you might end up on their blacklist.

## Changes ##

### current ###

### v1.1.1 ###

* Corrected a bug in colorTree.pl --show-palettes
* Adapted NCBI input file treatment to reflect changes in how NCBI formats results
* Adapted JGI hyperlinks

### v1.1 ###

* Changed how Taxonomy.pm finds a temp folder, using Perl module instad of non-portable /tmp/.
* Updated citation (issue 27).
* Minor corrections to README.md (issue 26).
* Corrected a bug in phyloSkeleton.pl which failed to create the folder to ncbi-all-projects if not existing (issue 28).
* Corrected a bug where Arch162 was not recognized as a reserved word in --hmms
* Corrected colorTree.pl to remove very pale hues
* To alleviate the fact that a few of the domains in Arch162 and Bact139 match mostly in the same protein, leading to the same protein being often duplicated (or more) in the concatenated alignment, added two hmm collections, which mostly match single genes: Arch137 and Bact109. 

### v1.0.1 ###

* Fixed a bug that allowed genomes with low completeness to be included in the gene maps. 
* Fixed a few tests
* Updated on the JGI API issue
* Fixed Makefile.PL to get to work when not running network tests

### v1.0 ###

* Fixed most of bugs related to using one's own genome tabs. 
* Improved the general stucture of phyloskeleton.pl. 
* Rewrote README to give more examples and clean older text
* Got a working Makefile.PL

### v0.8 ###

Major code rewriting, modularizing the whole code and removing individual scripts. This is the first release candidate for v1.

### v0.5 ###

* Added an alternative CDS annotator, prodigal, to simplify the installation process (prokka can be dependencies-heavy). Switch in annotate.pl
* parseTaxo.pl now passed the genetic code from the taxdump files, and adds it to the tab files, where it can be parsed by the annotator in annotate.pl.### v0.1 ###

Initial release with this README file.

### v0.4 ###

* Added to the tutorial how to add one's own data.
* Corrected a bug in attributeHMMs.pl, which would print rRNA rows multiple times

### v0.3 ###

If more than one hit per marker and per organism is found by attributeHMMs.pl, a warning is raised, and single-gene maps are produced. It is then easy to produce single-gene trees and check which paralog to remove.

### v0.2 ###

Complete tutorial and readme file.

## Contact ##

First use the issue system <http://bitbucket.org/lionelguy/phyloskeleton/issues>.

[rinke]: http://www.nature.com/nature/journal/v499/n7459/full/nature12352.html "Rinke, C, et al. (2013). Insights into the phylogeny and coding potential of microbial dark matter. Nature 499, 431–437 (25 July 2013) doi:10.1038/nature12352"
[brown]: http://www.nature.com/nature/journal/v523/n7559/full/nature14486.html "Brown, C, et al. (2015). Unusual biology across a group comprising more than 15% of domain Bacteria. Nature 523, 208-211 (09 July 2015) doi:10.1038/nature14486"
[phyloPaper]: https://academic.oup.com/bioinformatics/article-lookup/doi/10.1093/bioinformatics/btw824 "Guy, L. (2017). phyloSkeleton: taxon selection, data retrieval and marker identification for phylogenomics. Bioinformatics 2017, advanced access (05 January 2017) doi:10.1093/bioinformatics/btw824"
