=head1 NAME

Taxonomy - Downloads, parses and extracts info from NCBI Taxonomy

=head1 SYNOPSIS

my $localFolder = '/path/to/myfolder'
downloadTaxdump('local' => $localFolder,
                'refreshIfOlderThan' => 7);

=head1 DEPENDENCIES

=over

=item BioPerl

=item Archive::Extract

=back

=head1 AUTHOR

Lionel Guy (L<lionel.guy@imbim.uu.se>)

=head1 DATE

Fri Oct  7 15:35:45 CEST 2016

=head1 COPYRIGHT AND LICENSE

Copyright 2016 Lionel Guy

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

# Code starts here

package PhyloSkeleton::Taxonomy;

use strict;
use warnings;
use Carp;

use File::Basename;
use File::Fetch;
use File::Temp qw/ tempfile tempdir /;
use Cwd;
use Archive::Extract;
use Digest::MD5 qw( md5_hex md5 );
use List::MoreUtils;

use Bio::Taxon;
use Bio::Tree::Tree;

use FindBin qw/$Bin/;
use lib "$Bin/../lib";

use PhyloSkeleton::TabFiles qw/ readTable /;
use PhyloSkeleton qw/ ranks /;

require Exporter;
our @ISA = qw/ Exporter /;
our @EXPORT_OK = qw/ downloadTaxdump parseTaxonomy addTaxonomy hasTaxonomy/;

=head2 downloadTaxdump

 Title   : downloadTaxdump
 Usage   : downloadTaxdump('local'              => $localFolder,
                           'refreshIfOlderThan' => 7);
 Function: Download taxdump files from NCBI. Provides options to do it only 
           if newer than a certain number of days.
 Returns : 1 if updated, 0 if error, 2 if not updated
 Args    : Named parameters 
             local   => A path to a local storage path. Default is pwd.
             refreshIfOlderThan => If the local copy is older than x days. 
                                   Default 7 days.
             uri     => Remote location of the taxdump file. Default:
                        ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump.tar.gz
                        Assumes there is a companion .md5 file.

=cut

sub downloadTaxdump {
  my $cwd = getcwd();
  my %args = (
    'remoteFolder' => 'ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy',
    'filename'     => 'taxdump.tar.gz',
    'localFolder'  => $cwd,
    'refreshIfOlderThan' => 7,
    @_);

  my $localFolder = $args{'localFolder'};
  print STDERR "downloadTaxDump: Checking local copy\n";
  # Check if there is a local copy (requires both nodes.dmp and names.dmp)
  if (-s "$localFolder/nodes.dmp" && -s "$localFolder/names.dmp"){
    my $timediff = time - (stat("$localFolder/nodes.dmp"))[9];
    if ($timediff < ($args{'refreshIfOlderThan'} * 3600 * 24)){
      # File is present and more recent, return 2
      print STDERR "  Found a copy in $localFolder that is more recent than " . 
	"$args{'refreshIfOlderThan'} days\n";
      return 2;
    }
  }

  # If needed, download and check md5
  print STDERR "  Downloading from $args{'remoteFolder'}\n";
  my $ff = File::Fetch->new(uri => "$args{'remoteFolder'}/$args{'filename'}");
  $ff->fetch( to => $localFolder );
  croak "Error downloading taxdump file from $args{'uri'}: " . $ff->error() 
    if $ff->error();

  # Check md5
  print STDERR "  Retrieve and check md5 sums\n";
  my $ffmd5 = File::Fetch->
    new(uri => "$args{'remoteFolder'}/$args{'filename'}.md5");
  $ffmd5->fetch( to => $localFolder );
  croak "Error downloading taxdump file from $args{'uri'}: " . $ffmd5->error() 
    if $ffmd5->error();
  open my $fhmd5, '<', "$localFolder/$args{'filename'}.md5" or 
    croak "Cannot open md5 file $localFolder/$args{'filename'}.md5";
  my $line = <$fhmd5>;
  chomp $line;
  my ($md5_remote, $file) = split(/\s+/, $line);

  # Open targz file to calculate md5
  open my $fh, '<', "$localFolder/$args{'filename'}"
    or croak "Can't open $localFolder/$args{'filename'}: $!";
  binmode ($fh);
  my $md5_local = Digest::MD5->new->addfile($fh)->hexdigest;
  #my $md5_local = md5_hex($file);
  croak "md5 local:  $md5_local and \nmd5 remote: $md5_remote\nnot the same" 
    unless ($md5_remote eq $md5_local);

  # Untar: tar -xvzf taxdump.tar.gz
  print STDERR "  Extract tar.gz file\n";
  my $ae = Archive::Extract->new( archive => "$localFolder/$args{'filename'}" );
  my $ok = $ae->extract( to => $localFolder );
  croak "Error while extracting taxdump archive at $localFolder:" . $ae->error()
    unless $ok;

  print STDERR "  Done\n";
  return 1;
}

=head2 parseTaxonomy

 Title   : parseTaxonomy
 Usage   : parseTaxonomy('nodes' => '/path/to/nodes.dmp',
                     'ranks' => [ qw/ phylum class genus / ],
                     'taxids'=> [ qw/ 1143 22354 45567 1125 /]);
 Function: For each taxid, parses taxonomy from a taxdump file and 
 Returns : A hashref containing the ranks for each given taxid
 Args    : Named parameters 
             nodes => The nodes.dmp file downloaded from NCBI. Mandatory.
             names => The names.dmp file from NCBI. If not set, the program
                      will try to look in the same folder as 'nodes'
             ranks => An arrayref to ranks. By default: superkingdom, phylum,
                      class, order, family, genus, species
             taxids=> An arrayref to taxids for which to report taxonomy. 
                      Mandatory.

 See also: downloadTaxdump

=cut

sub parseTaxonomy {
  my %args = ('nodes' => undef,
	      'names' => undef,
	      'ranks' => [ qw/superkingdom phylum class order family genus species/ ],
	      'taxids'=> undef, 
	      @_);

  croak "No nodes indicated" unless $args{'nodes'};
  croak "No taxids indicated" unless $args{'taxids'};

  # Get a ranks hash
  my %ranks = map { $_ => 1 } @{ $args{'ranks'} }; 

  # Set names if not explicitly given, look in the same directory as nodes.dmp
  unless ($args{'names'}){
    my $dn = dirname($args{'nodes'});
    $args{'names'} = $dn . '/names.dmp';
  }

  # Initiate Taxo database
  print STDERR "Initializing database, it will take a few minutes... ";
  my $tempdir = tempdir( CLEANUP => 1 );
  my $dbh = Bio::DB::Taxonomy->new(-source    => 'flatfile',
				   -directory => $tempdir,
				   -nodesfile => $args{'nodes'},
				   -namesfile => $args{'names'});
  croak "Couldn't initiate taxonomy database from node file  $args{'nodes'}" 
    unless $dbh;


  # Print result to a temp file and then read from it. Could be made better.
  my ($fh, $filename) = tempfile();

  # Print title of the output file
  print $fh "taxid\tgcode\tname\t", join("\t", @{ $args{'ranks'} }), "\n";

  # Now retrieve the requested ids in the taxonomy, using Bio::Tree::Tree object
  my $tree_functions = Bio::Tree::Tree->new();
  my $nIds = 0;
  my @missingIds;

  foreach my $id (@{ $args{'taxids'} }){
    my %ancestry;
    my $taxon = $dbh->get_taxon(-taxonid => $id);
    # Warn and skip if taxon not found
    unless ($taxon){
      push @missingIds, $id;
      next;
    }

    # Retrieve lineage, and get requested names for each of the requested ranks
    my @lineage = $tree_functions->get_lineage_nodes($taxon);

    # Include the current taxon, just in case we got the species or genus taxid
    foreach my $parent ($taxon, @lineage){
      my $rank = $parent->rank;
      $ancestry{$rank} = $parent->scientific_name 
	if ($rank && $ranks{$rank});
    }

    # Searches genetic code
    my $gcode = 'NA';
    $gcode = $taxon->genetic_code if ($taxon->genetic_code);
    # Print results, tab-separated
    print $fh "$id\t$gcode\t" . $taxon->scientific_name;
    foreach my $rank (@{ $args{'ranks'} }) {
      if ($ancestry{$rank}){
	print $fh "\t", $ancestry{$rank}
      } else {
	print $fh "\tNA";
      }
    }
    print $fh "\n";
  }
  close($fh);

  # Report taxids not found in the database
  if (@missingIds){
    print STDERR "The following ids were not found in $args{'nodes'} and their".
      " taxonomy won't be output. That is generally OK:\n";
    foreach (@missingIds){
      print STDERR "  $_\n";
    }
  }

  # Now read the table that we've just printed.
  my $tref = PhyloSkeleton::TabFiles::readTable('file'    => $filename, 
						'rowname' => 'taxid');
  # And return
  return $tref;
}

=head2 addTaxonomy

 Title   : addTaxonomy
 Usage   : addTaxonomy('gref' => $gref,
                       'tref' => $tref);
 Function: Add taxonomic info to a genome ref table. Adds following columns:
             - a column for each of the ranks contained in tref. 'NA' if missing
             - 'taxostring', a string collapsing all above cells.
             - genetic code
 Returns : A hash ref to the updated table
 Args    : Named parameters 
             gref   => Hash ref to an existing table containing a taxid column
             tref   => Hash ref to the output of parseTaxonomy

 See also: parseTaxonomy, readTable

=cut

# Add taxonomy info for each assid
sub addTaxonomy {
  my %args = ( 'gref' => undef,
	       'tref' => undef,
	       'ranks' => [ PhyloSkeleton::ranks() ],
	       @_);

  croak "No gref indicated" unless $args{'gref'};
  croak "No tref indicated" unless $args{'tref'};

  print STDERR "Adding taxonomy info for each assembly...";

  my $gref = \%{ $args{'gref'} };
  my $nassid = 0;
  my $nremoved = 0;
  my @missingIds;
  my %toAdd;

  # Read the gref, gather taxo info into toAdd.
  foreach my $assid (@{ $gref->{'rows'} }){
    $nassid++;
    # Get taxonomy
    my $taxid = $gref->{'byrow'}{$assid}{'taxid'};
    # croak "No taxid in $assid\n" unless $taxid;
    ## Three cases: 
    # a) the taxid is not given or not valid => remove the
    #    assembly from the gref. 
    # b) the taxid is '-' => return '-' values for all fields
    # c) the taxid is present, numerical and valid => return ranks
    # Case b) first
    my %res;
    if ($taxid && $taxid eq '-'){
      # Return '-' values
      $res{'taxostring'} = $gref->{'byrow'}{$assid}{'taxostring'} ? 
	$gref->{'byrow'}{$assid}{'taxostring'} : '-';
      $res{'gcode'} = $gref->{'byrow'}{$assid}{'gcode'} ? 
	$gref->{'byrow'}{$assid}{'gcode'} : '-';
      foreach my $rank (@{ $args{'ranks'} }){
	$res{$rank} = $gref->{'byrow'}{$assid}{$rank} ? 
	  $gref->{'byrow'}{$assid}{$rank} : '-';
      }
    }
    # Case a)
    elsif(!($taxid && $taxid > 0 && $args{'tref'}{'byrow'}{$taxid})) {
      ## Consider returning '-' values and excluding them rather than 
      # removing them from gref
      push @missingIds, "$assid => $taxid";
      $gref = PhyloSkeleton::TabFiles::removeRow('table'   => $gref,
						 'rowname' => $assid);
      $nremoved++;
      next;
    }
    # Case c)
    else {
      my $taxostring = "";
      foreach my $rank (@{ $args{'ranks'} }){
	croak "Could not find rank $rank in columns of tref\n" 
	  unless $args{'tref'}{'byrow'}{$taxid}{$rank};
	my $taxon = $args{'tref'}{'byrow'}{$taxid}{$rank};
	$res{$rank} = $taxon;
	$taxostring .= $taxon . ';';
      }
      $taxostring =~ s/;$//;
      $res{'taxostring'} = $taxostring;
      $res{'gcode'} = $args{'tref'}{'byrow'}{$taxid}{'gcode'};
    }
    # Now get the vaules into gref: Check if the column is already present
    foreach my $value (keys %res){
      if ($gref->{'bycol'}{$value}){
	$gref->{'byrow'}{$assid}{$value} = $res{$value};
      } else {
	$toAdd{$value}{$assid} = $res{$value};
      }
    }
  }

  # Now add columns
  foreach my $col ('gcode', @{ $args{'ranks'} }, 'taxostring'){
    if ($toAdd{$col}){
      $gref =  PhyloSkeleton::TabFiles::addColumn('table' => $gref, 
						  'colname' => $col, 
						  'col' => \%{ $toAdd{$col} });
    }
  }
  print STDERR " seen $nassid assemblies, removed $nremoved without taxid.\n";

  # Inform if columns removed
  carp("Following assemblies with taxids were removed: \n  " . 
       join("\n  ", @missingIds) . "\n") if (@missingIds);
  # Return
  return $gref;
}

=head2 _findSamplingLevel

 Title   : _findSamplingLevel
 Usage   : _findSamplingLevel('taxid'    => 9606
                              'taxonomy' => $tref,
                              'selectionLevels' => $sref,
                              'ranks' => [ qw/phylum class order/ ]);
 Function: For a specific taxon, finds the sampling group and the
           selection group it also belongs to. For example, if the 
           selection level is set to genus in the order Legionellales, 
           Coxiella burnetii isolates' selection group would thus be the
           genus Coxiella.
 Returns : 
 Args    : Named parameters 
             taxid           => A taxid
             taxonomy        => A hashref to a taxonomy table
             selectionLevels => A hashref to a selection levels table
             ranks           => The ranks to go through
             
 See also: parseSelectionLevels, parseTaxonomy

=cut

sub _findSamplingLevel {
  my %args = ( 'taxid'           => undef,
               'selectionLevels' => undef,
               'taxonomy'        => undef,
               'ranks'           => undef,
               @_);

  croak "No taxid parameter passed" unless $args{'taxid'};
  croak "No selectionLevels parameter passed" unless $args{'selectionLevels'};
  croak "No taxonomy parameter passed" unless $args{'taxonomy'};

  my $debug = 0;
  my $taxid = $args{'taxid'};
  my $selLevRef = $args{'selectionLevels'}; 
  my $taxidsref = $args{'taxonomy'};
  my @ranks = @{ $args{'ranks'} };
  
  my $samplingRank = $selLevRef->{'byrow'}{'other'}{'selectionLevel'};
  my $samplingGroup = 'other'; # default
  my $samplingLevel;
  # Go up the ranks of the taxid, until we get a match to one of the rank
  #  in the selLevRef
  foreach my $rank (reverse @ranks){
    if ($selLevRef->{'byrow'}{ $taxidsref->{'byrow'}{$taxid}{$rank} }) {
      $samplingGroup = $taxidsref->{'byrow'}{$taxid}{$rank};
      $samplingRank = $selLevRef->{'byrow'}{$samplingGroup}{'selectionLevel'};
      $samplingLevel = $taxidsref->{'byrow'}{$taxid}{$samplingRank};
      # Now if the name at sampling rank is NA, go down the ranks from that
      # rank until we find something that is not NA. Worst case take the name
      if ($samplingLevel eq 'NA'){
	my $samplingRankId = List::MoreUtils::first_index {$_ eq $samplingRank}
	  @ranks;
	unless ($samplingRankId < 1){
	  foreach my $i (($samplingRankId-1)..0){
	    $samplingLevel = $taxidsref->{'byrow'}{$taxid}{$ranks[$i]};
	    last unless ($samplingLevel eq 'NA')
	  }
	}
	$samplingLevel = $taxidsref->{'byrow'}{$taxid}{'name'} 
	  if ($samplingLevel eq 'NA')
      }
      last;
    }
  }
  # If no sampling level was found, use default one
  unless ($samplingLevel){
    $samplingLevel = $taxidsref->{'byrow'}{$taxid}{$samplingRank};
  }
  unless ($samplingLevel){
    croak "No sampling level for taxid $taxid\n";
  }
  #croak "No sampling level for taxid $taxid\n" unless $samplingLevel;
  print STDERR "  For taxid $taxid ($taxidsref->{$taxid}{'name'}), ", 
    "found sampling level $samplingLevel, rank $samplingRank in group ", 
      "$samplingGroup\n" if $debug;
  return ($samplingLevel, $samplingGroup, $samplingRank);
}

=head2 hasTaxonomy

 Title   : hasTaxonomy
 Usage   : hasTaxonomy('table' => $gref,
                       'id' => 'GCA_XXXX');
 Function: Check whether a specific row has taxonomic information
 Returns : 1 if success, 0 otherwise
 Args    : Named parameters 
             table   => A table ref
             id      => An id (rowname) for this table
             ranks   => Taxonomic ranks to check
             any     => Is it OK with any of these? Otherwise looks for all 

 See also:

=cut

sub hasTaxonomy {
  my %args = ('table' => undef,
	      'id'    => undef,
	      'ranks' => [ PhyloSkeleton::ranks() ],
	      'any'   => 0,
	      @_);

  my @ranks = @{ $args{'ranks'} };
  my $gref = $args{'table'};
  croak "No table given" unless $gref;
  my $id = $args{'id'};
  croak "No id given" unless $id;
  croak "No id $id in table" unless $gref->{'byrow'}{$id};

  my $found = 0;
  foreach my $rank (@ranks){
    $found++
      if ($gref->{'byrow'}{$id}{$rank} && $gref->{'byrow'}{$id}{$rank} ne '-');
  }
  my $retval;
  if ($args{'any'}){
    $retval = $found > 0 ? 1 : 0;
  } else {
    $retval = $found == scalar(@ranks) ? 1 : 0;
  }
  return $retval;
}


1;
