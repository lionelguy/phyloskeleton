=head1 NAME

TabFiles - Read and parses, and prints tab-separated files

=head1 SYNOPSIS

use TabFiles qw/ detectMacEncoding /;

# Just checking
my $hasMacNewLines = detectMacEncoding('file'    => $filename,
                                       'replace' => 0);

# Replaces mac end-of-lines with Unix ones, saves original files with 
# .backup extension
detectMacEncoding('file'    => $filename,
                  'replace' => 1, 
                  'backup'  => '.backup');

use TabFiles qw/ readTable /;

# Just reading
my $res1 = readTable('file' => $filename);
# Main entry point, by row
my $cell1 = $res1->{'byrow'}{1}{'a'};
# Also by column, same result
$cell1 = ${ $res1->{'bycol'}{'a'}{1} }             

# Reading with row names, a selection of columns
my @cols = qw/ id b c /;
my $res3 = readTable('file'    => $filename,
                     'rowname' => 'id',
                     'columns' => \@cols);
my @columns = @{ $res3->{'columns'} };

# Convert comma-separated value-file to tab-separated


=head1 AUTHOR

Lionel Guy (L<lionel.guy@imbim.uu.se>)

=head1 DATE

Tue Sep 20 14:34:58 CEST 2016

=head1 COPYRIGHT AND LICENSE

Copyright 2016 Lionel Guy

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

# Code begins here:

package PhyloSkeleton::TabFiles;

# Libraries
use strict;
use warnings;
use Carp;
use List::Util qw(sum);
use Text::CSV;
use Text::ParseWords;

require Exporter;
our @ISA = qw/Exporter/;
our @EXPORT_OK = qw/newTable readTable writeTable mergeTables addColumn addRow removeColumn removeRow getColumn detectMacEncoding csv2tsv /;

=head2 newTable

 Title   : newTable
 Usage   : newTable('columns' => [ qw/a b c/ ], 
                    'rowname' => 'a');

 Function: Creates a new, empty table
 Returns : A hashref
 Args    : Named parameters 
             columns   => Arrayref to the column names. Mandatory
             rowname   => Column that gives row names. By default the first col.

 See also:

=cut

sub newTable {
  my %args = ('columns' => undef,
	      'rowname' => undef,
	      @_);

  # Initiate
  my %res;
  my $rowname_idx;

  # Validate
  croak "No columns argument" unless $args{'columns'};
  my @columns = @{ $args{'columns'} };
  croak "Columns argument is length 0" unless scalar(@columns); 
  if ($args{'rowname'}) {
    foreach my $i (0..$#columns){
      $rowname_idx = $i if ($columns[$i] eq $args{'rowname'});
    }
    croak "rowname not among columns" unless defined $rowname_idx;
  } else {
    $rowname_idx = 0;
    $args{'rowname'} = $columns[0]
  }

  # Populate
  $res{'ncol'} = scalar(@columns);
  $res{'nrow'} = 0;
  $res{'columns'} = \@columns;
  $res{'rows'} = [];
  $res{'rowname_idx'} = $rowname_idx;
  $res{'rowname'} = $args{'rowname'};
  foreach my $col (@columns){
    $res{'bycol'}{$col} = undef;
  }
  
  # Return
  return \%res;
}

=head2 readTable

 Title   : readTable
 Usage   : my $tab_ref = readTable('file'     => $myfile,
                                   'columns'  => qw/id name size/;
                                   'rowname'  => 1,
                                   'sep'      => '\t');
 Function: Parse a tab- or comma-separated file. Title is parsed by default. If
           title is parsed, names are computerized, so that it leaves
           alphanumeric characters. Everthing else is replaced with '_'.
 Returns : A hash ref, with the following keys:
             byrow, bycol: giving access to the data by row, or column first. 
                'byrow' is the row number if 'rowname' is not defined. 
                The access by columns is done by reference. E.g:
                   $tab_ref->{'byrow'}{'AE12345'}{'assembly'} (scalar)
                 will return the content of the cell in 'assembly' column and
                 'AE12345' row. It is identical to 
                   ${ $tab_ref->{'bycol'}{'assembly'}{'AE12345'} }
             ncol, nrow: returns the number of columns/rows (scalar)
             columns, rows: returns the column/row names (list)
             rowname, rowname_idx: the name and index of the column holding the
                rownames
           Warning: if the hashref is modified, the ncol and columns arguments
           might not be correct anymore. Use the add/removeRow/Column methods.
 Args    : Named parameters:
             file    => File to parse (mandatory, no default)
             columns => Columns to parse (arrayref). By default parses
                        all columns.
             columnNameMap => A hashref to rename some columns (keys).
             rowname => Which column to parse rowids. By default 'undef'. Will 
                        carp if non-unique. If no title is set or if not
                        matching a column name, will use the nth column.
             sep     => Field separator, used to split fields. '\t' by default.
             quote   => Quote character, potentially bordering text fields.
             comment => Comment character. '#' by default
             title   => Boolean. Parse title? If not set, the 'columns' argument
                        can be used to name columns.
             titleChar => Character preceding title line. undef by default.
             titleLC => Set all column names to lowercase.
             skip    => Skip so many lines before parsing title (if any). 0 by 
                       default.
             lenientRowLength => Allows rows to have more or less cells than 
                                 title. 0 by default.
             removeTrailingSpace => Remove trailing spaces in cells. 0 by def.
             convertMacEncoding  => If set, will convert the table file to 
                                    unix encoding first. True by default.

=cut

sub readTable {
  my %args = ('file'    => undef,
	      'columns' => undef,
	      'columnNameMap' => undef,
	      'rowname' => undef,
	      'sep'     => '\t',
	      'quote'   => undef,
	      'comment' => '#',
	      'title'   => 1,
	      'titleChar' => undef,
	      'titleLC' => 0,
	      'skip'    => 0,
	      'lenientRowLength'    => 0,
	      'removeTrailingSpace' => 0,
	      'convertMacEncoding'  => 1,
	      @_);
  
  # Check that there is a file
  croak "No file argument" unless $args{'file'};

  # Warn if lenient without proper definition of columns to parse.
  carp "Using 'lenient' without 'columns' or 'title' is at your own risk: it " .
    "will ignore any cell that has a column index larger than the first line"
      if ($args{'lenient'} && !($args{'columns'} || $args{'title'}));

  # Initialize values
  my %res;

  my @col_names;
  my %selected_cols;
  my @selected_cols_idxs;

  my @row_names;
  my %row_names;
  
  my $rowname_idx;

  my ($ncols, $nselcols, $nrows) = (0, 0, 0);

  # Put selected column names in a hash
  %selected_cols = map { $_ => 1 } @{ $args{'columns'} } if ($args{'columns'});

  # Check whether there might be problems with mac encoding
  my $hasMacEncoding = detectMacEncoding(
      'file' => $args{'file'},
      'replace' => $args{'convertMacEncoding'});
  carp "File $args{'file'} looks like a mac-encoded file, with return " . 
    "carriages (\\r) instead of newlines (\\n) as end-of-line characters. " . 
      "The original file has been recoded and a backup is available " . 
	"($args{'file'}.mac)\n" if $hasMacEncoding;
  # Open file
  open my $fh, '<', $args{'file'} or croak "Could not open $args{'file'}: $!";
  # Skip if requested
  if ($args{'skip'} && $args{'skip'} > 0){
    foreach my $i (1..$args{'skip'}){
      my $line = <$fh>;
    }
  }

  ##############################################################################
  # Parse title 
  if ($args{'title'}){
    my $line = <$fh>;
    chomp $line;
    # Remove trailing whitespaces?
    #$line = s/\s+$//;

    # To lowercase if required
    $line = lc($line) if $args{'titleLC'};

    # Remove first character in the title
    if ($args{'titleChar'}){
      my $pattern = $args{'titleChar'};
      $line =~ s/^$pattern//;
    }

    # Split title
    if ($args{'quote'}){
      @col_names = quotewords "$args{'sep'}", 0, $line;
    } else {
      @col_names = split(/$args{'sep'}/, $line);
    }
    my %seen_col_names;
    # By default all columns are selected; then if colnames are specified, 
    # check where they are. Also checks that the rowname is in the lot
    foreach my $i (0..$#col_names){
      # Remove quote chars
      if ($args{'quote'} && 
	  $col_names[$i] =~ /^$args{'quote'}.*$args{'quote'}$/){
	$col_names[$i] =~ s/^$args{'quote'}//;
	$col_names[$i] =~ s/$args{'quote'}$//;
      }

      # Remove trailing white space at the end of cell
      $col_names[$i] =~ s/\s+$// if ($args{'removeTrailingSpace'});

      # Remove non-alphanum characters
      $col_names[$i] =~ s/[^\w$args{'sep'}]/_/g;

      # Skip unwanted columns, record name first
      $seen_col_names{$col_names[$i]}++;
      next if (%selected_cols && !$selected_cols{$col_names[$i]});

      # Rename col name if necessary, before pushing
      $col_names[$i] = $args{'columnNameMap'}{$col_names[$i]}
	      if $args{'columnNameMap'}{$col_names[$i]};

      # Push indexes of the selected columns
      push @selected_cols_idxs, $i;
      $rowname_idx = $i
        if (defined($args{'rowname'}) && ($col_names[$i] eq $args{'rowname'}));

    }

    # Set number of columns.
    $ncols = scalar(@col_names);
    $nselcols = scalar(@selected_cols_idxs);
    # Carp if we haven't found all columns in column names.
    foreach my $col (keys %selected_cols){
      carp "Column $col not found in title, which contains: " . 
        join(", ", keys(%seen_col_names)) . "\n" unless $seen_col_names{$col};
    }
      
    # Check that the column giving rowname is aboard.
    croak "Column indicated as source for rownames not in the column list"
      if (defined($args{'rowname'}) && !defined($rowname_idx));
  }

         
  ##############################################################################
  # Parse the content of the file
  while (<$fh>){
    next if ($args{'comment'} && /^$args{'comment'}/);
    chomp;
    my @a;
    # A bit of a mess, but there are very annoying cases with quotes
    # everywhere. Quotewords does a decent job when the text is actually
    # quoted, but a bad one when it's not
    if ($args{'quote'}){
      @a = quotewords "$args{'sep'}", 0, $_;
    } else {
      @a = split(/$args{'sep'}/, $_);
    }
    # Remove quotes and trailing white space at the end of cell
    foreach my $i (0..$#a){
      $a[$i] = '' unless defined($a[$i]);
      $a[$i] =~ s/\s+$// if $args{'removeTrailingSpace'};
      $a[$i] =~ s/^$args{'quote'}(.+)$args{'quote'}$/$1/ if $args{'quote'}; 
    }
    
    ############################################################################
    # At the first line, counting columns, setting names if no title
    if ($nrows++ == 0 && !$args{'title'}){
      $ncols = scalar(@a);
      if ($args{'columns'}){
	@col_names = @{ $args{'columns'} };
	croak "Number of column names (" . scalar(@col_names) . 
	  ") provided not the same as number of columns: $ncols\n" 
	    unless scalar(@col_names) == $ncols;
	} else {
	  @col_names = (1..scalar(@a));
	}
      # Set selected col indexes to all and set rowname index
      @selected_cols_idxs = (0..$#a);
      $rowname_idx = $args{'rowname'} if (defined($args{'rowname'}));
    } # end of reading first line

    ############################################################################
    # Treat each row now
    
    # Check that number of cols == $ncols
    croak "Not the same number of columns (". scalar(@a) . 
      ") in row $nrows (should be $ncols)" 
	unless ($args{'lenient'} || scalar(@a) == $ncols);

    # Find row name 
    my $row_name = $nrows;
    $row_name = $a[$rowname_idx] if (defined($args{'rowname'}));
    push @row_names, $row_name;
    croak "Row names non-unique ($row_name). Choose another column or " . 
      "leave blank" if $row_names{$row_name};
    $row_names{$row_name}++;
   

    # Now go through selected columns in the row, and populate the res hash
    foreach my $i (@selected_cols_idxs){
      # If rowname is defined, 
      if (defined($args{'rowname'})){
        $res{'byrow'}{$row_name}{$col_names[$i]} = $a[$i];
        $res{'bycol'}{$col_names[$i]}{$row_name} = 
          \$res{'byrow'}{$row_name}{$col_names[$i]};
        # Check unique, croak otherwise
      } else {
        $res{'byrow'}{$nrows}{$col_names[$i]} = $a[$i];
        $res{'bycol'}{$col_names[$i]}{$nrows} = 
          \$res{'byrow'}{$nrows}{$col_names[$i]};
      }
    }  # Done reading this line
  } # Done reading lines

  # Add column and row number and names to result
  $res{'ncol'} = scalar(@selected_cols_idxs);
  $res{'nrow'} = $nrows;
  my @columns = @col_names[@selected_cols_idxs];
  $res{'columns'} = \@columns;
  $res{'rows'} = \@row_names;

  # Add rowname_idx if present
  if (defined($args{'rowname'})){
    $res{'rowname_idx'} = $rowname_idx ;
    $res{'rowname'} = $args{'rowname'};
  }

  # Carp if nrow is 0
  carp "File $args{'file'} seems to have 0 rows of data. Check the input file."
    if ($nrows < 1);

  # Return
  return \%res;
}

=head2 addColumn

 Title   : addColumn
 Usage   : addColumn('table' => $table,
                     'col'   => $href);
 Function: Adds a column to an existing table, updating column names and number
 Returns : A href to the updated table
 Args    : Named parameters 
             table   => A href to an exising table.
             colname => A name for the column.
             col     => Column, under the form of a href with _exactly_ the
                        same keys as table.

=cut

sub addColumn {
  my %args = ('table'   => undef,
	      'colname' => undef,
	      'col'     => undef,
	      @_);

  croak "Argument table not defined" unless $args{'table'};
  croak "Argument col not defined" unless $args{'col'};
  croak "Argument colname not defined" unless $args{'colname'};

  my $tref = $args{'table'};
  my %col  = %{ $args{'col'} };
  my $colname = $args{'colname'};
  
  croak "Table and col don't have the same length" 
    unless scalar(keys(%{ $tref->{'byrow'} } )) == scalar(keys(%col));
  foreach my $key (keys %col){
    croak "Column key $key not found in table" unless $tref->{'byrow'}{$key};
    $tref->{'byrow'}{$key}{$colname} = $col{$key};
    $tref->{'bycol'}{$colname}{$key} = 
          \$tref->{'byrow'}{$key}{$colname};
  }

  push @{ $tref->{'columns'} }, $colname;
  $tref->{'ncol'}++;

  return $tref;
}

=head2 addRow

 Title   : addRow
 Usage   : addRow('table' => $table,
                  'row'   => $href);
 Function: Adds a row to an existing table, updating row names and number
 Returns : A href to the updated table
 Args    : Named parameters 
             table   => A href to an exising table.
             row     => Row, under the form of a href with _exactly_ the
                        same keys as table.

=cut

sub addRow {
  my %args = ('table'   => undef,
	      'row'     => undef,
	      @_);

  croak "Argument table not defined" unless $args{'table'};
  croak "Argument row not defined" unless $args{'row'};

  my $tref = $args{'table'};
  my %row  = %{ $args{'row'} };

  # Define rowname - from the table or first column
  my $rowname;
  if ($tref->{'rowname'}){
    $rowname = $row{$tref->{'rowname'}};
    croak "Rowname $rowname already present in the table." 
      if $tref->{'byrow'}{$rowname};
  } else {
    $rowname = $row{$tref->{'columns'}[0]};
  }

  croak "Table and row don't have the same length" 
    unless $tref->{'ncol'} == scalar(keys(%row));
  foreach my $key (keys %row){
    croak "Row key $key not found in table" 
      unless exists($tref->{'bycol'}{$key});
    $tref->{'byrow'}{$rowname}{$key} = $row{$key};
    $tref->{'bycol'}{$key}{$rowname} = 
          \$tref->{'byrow'}{$rowname}{$key};
  }

  push @{ $tref->{'rows'} }, $rowname;
  $tref->{'nrow'}++;

  return $tref;
}



=head2 removeColumn

 Title   : removeColumn
 Usage   : removeColumn('table'   => $table,
                        'colname' => $colname);
 Function: Removes a column in a table, updating column names and number
 Returns : A href to the updated table
 Args    : Named parameters 
             table   => A href to an exising table.
             colname => A name for the column.

=cut

sub removeColumn {
  my %args = ('table'   => undef,
	      'colname' => undef,
	      @_);

  my $tref = $args{'table'};
  my $colname = $args{'colname'};

  croak "Argument table not defined" unless $args{'table'};
  croak "Argument colname not defined" unless $args{'colname'};
  carp "Column $colname not found in table" unless $tref->{'bycol'}{$colname};

  # Remove from columns
  my @new_cols;
  foreach my $col (@{ $tref->{'columns'} }){
    push @new_cols, $col unless ($col eq $colname);
  }
  $tref->{'columns'} = \@new_cols;
  $tref->{'ncol'}--;

  # Remove from data itself
  delete $tref->{'bycol'}{$colname};
  foreach my $row (@{ $tref->{'rows'} }){
    delete $tref->{'byrow'}{$row}{$colname};
  }

  return $tref;
}

=head2 removeRow

 Title   : removeRow
 Usage   : removeRow('table'   => $table,
                     'rowname' => $rowname);
 Function: Removes a row in a table, updating row names and number
 Returns : A href to the updated table
 Args    : Named parameters 
             table   => A href to an exising table.
             rowname => A name for the column.

=cut

sub removeRow {
  my %args = ('table'   => undef,
	      'rowname' => undef,
	      @_);

  my $tref = $args{'table'};
  my $rowname = $args{'rowname'};

  croak "Argument table not defined" unless $args{'table'};
  croak "Argument rowname not defined" unless $args{'rowname'};
  carp "Row $rowname not found in table" unless $tref->{'byrow'}{$rowname};

  # Remove from rows
  my @new_rows;
  foreach my $row (@{ $tref->{'rows'} }){
    push @new_rows, $row unless ($row eq $rowname);
  }
  $tref->{'rows'} = \@new_rows;
  $tref->{'nrow'}--;

  # Remove from data itself
  delete $tref->{'byrow'}{$rowname};
  foreach my $col (@{ $tref->{'columns'} }){
    delete $tref->{'bycol'}{$col}{$rowname};
  }

  return $tref;
}

=head2 getColumn

 Title   : getColumn
 Usage   : getColumn('table'  => $href,
                     'column' => 'a');
 Function: Get a column from a table
 Returns : An arrayref
 Args    : Named parameters 
             table   => A href to a table
             column  => Column to extract

=cut

sub getColumn {
  my %args = ('table'  => undef,
	      'column' => undef,
	      @_);
  # Validate
  croak "No table argument provided" unless $args{'table'};
  croak "No column argument provided" unless $args{'column'};
  my @ids;
  foreach my $row (@{ $args{'table'}->{'rows'} }){
    my $id = $args{'table'}->{'byrow'}{$row}{ $args{'column'} };
    push @ids, $id;
  }
  return \@ids;
}


=head2 mergeTables

 Title   : mergeTables
 Usage   : mergeTables('tables'  => [ ($tbl1, $tbl2, ...) ],
                       'columns' => 'union');
 Function: Merge several tables.
 Returns : A href to the 
 Args    : Named parameters 
             tables   => Tables (href) to merge. The first one is used as
                         "reference".
             columns  => One of 'union', 'intersection', 'reference'
             rowname  => The column to use for rownames. By default uses the
                         same column as for the first table. Croaks if
                         the ids are not unique.


=cut

sub mergeTables {
  my %args = ('tables'  => undef,
	      'columns' => 'union',
	      'rowname' => undef,
	      @_);

  my %valid_col_parameters = ( 'union' => 1, 'intersection' => 1, 
			       'reference' => 1);
  # Parameters
  croak "No tables parameter set" unless $args{'tables'};
  croak "Parameter columns should be one of " . 
    join(", ", keys %valid_col_parameters) . "\n" 
      unless $valid_col_parameters{$args{'columns'}};

  # Initiate, fill in unset parameters
  my %res;
  my $rowname = $args{'rowname'};
  my $rowname_idx;
  my $ref_tref = (@{ $args{'tables'} })[0];
  unless ($args{'rowname'}){
    $rowname = $ref_tref->{'rowname'};
    croak "No rowname defined and no rowname in first table" 
      unless (defined($ref_tref->{'rowname'}));
  }

  # Find columns to add
  my (@cols, @seencols);
  my %cols;
  my $ntables = 0;
  # Case: reference: take column names from first table
  if ($args{'columns'} eq 'reference'){
    @cols = @{ $ref_tref->{'columns'} };
    %cols = map { $_ => 1 } @cols;
  } else {
    # Collect column names
    foreach my $table (@{ $args{'tables'} }){
      $ntables++;
      foreach my $col (@{ $table->{'columns'} }){
	push @seencols, $col unless $cols{$col};
	$cols{$col}++;
      }
    }
    # Case: union, take everything
    if ($args{'columns'} eq 'union'){
      @cols = @seencols;
    } else {
      # Case: intersection, take only columns present in all
      foreach my $col (@seencols){
	if ($cols{$col} == $ntables){
	  push @cols, $col;
	} else {
	  delete $cols{$col};
	}
      }
    }
  }

  # Check rowname and id, populate
  my %rows;
  my @rows;
  foreach my $table (@{ $args{'tables'} }){
    croak "Column for rowname not present in at least one table" 
      unless $table->{'bycol'}{$rowname};
    foreach my $row (@{ $table->{'rows'} }){
      my $rowid = $table->{'byrow'}{$row}{$rowname};
      croak "Row id $rowid non unique" if $rows{$rowid};
      $rows{$rowid}++;
      push @rows, $rowid;
      foreach my $col (@cols){
	my $value = '-';
	$value = $table->{'byrow'}{$row}{$col} 
	  if (exists($table->{'byrow'}{$row}{$col}));
	$res{'byrow'}{$rowid}{$col} = $value;
	$res{'bycol'}{$col}{$rowid} = \$res{'byrow'}{$rowid}{$col};
      }
    }
  }

  # Attributes (columns, rows, ncol, nrow, rowname, rowname_idx)
  foreach my $i (0..$#rows){
    $rowname_idx = $i if ($rows[$i] eq $rowname);
  }

  $res{'rowname'} = $rowname;
  $res{'columns'} = \@cols;
  $res{'rows'} = \@rows;
  $res{'ncol'} = scalar(@cols);
  $res{'nrow'} = scalar(@rows);

  # return
  return \%res;
}

=head2 sortTable


 Title   : sortTable
 Usage   : sortTable('table' => $hashref,
                      'file'  => $filename);

 Function: Sorts table, both columns according to column ranking,
           and rows, according to columns to be sorted.
 Returns : 1 in case of success.
 Args    : Named parameters 
             table   => Hashref to the table to print.   
             file    => File to write to. Mandatory, no default.
             title   => Print title? 1 by default.
             sep     => Field separator, "\t" by default
             missing => Character to print if no value in the cell.
                        Default: "-".

=cut

sub sortTable {


}

=head2 writeTable

 Title   : writeTable
 Usage   : writeTable('table' => $hashref,
                      'file'  => $filename);

 Function: Writes tables obtained by readTable.
 Returns : 1 in case of success.
 Args    : Named parameters 
             table   => Hashref to the table to print.   
             file    => File to write to. Mandatory, no default.
             title   => Print title? 1 by default.
             sep     => Field separator, "\t" by default
             missing => Character to print if no value in the cell.
                        Default: "-".

=cut

sub writeTable {
  my %args = ('table'   => undef,
              'file'    => undef,
	      'fh'      => undef,
              'title'   => 1,
              'sep'     => "\t",
              'missing' => "-",
              @_);

  croak "No file or fh name given" unless ($args{'file'} || $args{'fh'});
  croak "No hashref to a table given" unless $args{'table'};

  # File
  my $fh = $args{'fh'};
  unless ($fh){
    open $fh, '>', $args{'file'} or 
      croak "Cannot write to file $args{'file'}";
  }
  my $g = $args{'table'};

  # Title
  if ($args{'title'}){
    print $fh join($args{'sep'}, @{ $g->{'columns'} }), "\n";
  }

  # Go through rows of the table
  foreach my $rowname (@{ $g->{'rows'} }){
    # Skip rows that don't exist anymore
    next unless $g->{'byrow'}{$rowname};
    # Go through columns of the row. Print one row at a time
    my @values;
    foreach my $colname (@{ $g->{'columns'} }){
      my $value = $args{'missing'};
      $value = $g->{'byrow'}{$rowname}{$colname} 
        if (defined($g->{'byrow'}{$rowname}{$colname}));
      push @values, $value;
    }
    print $fh join($args{'sep'}, @values), "\n";
  }
  close $fh;
  return 1;
}

=head2 detectMacEncoding

 Title   : detectMacEncoding
 Usage   : detectMacEncoding('file'    => $filename,
                             'replace' => 1,
                             'backup'  => '.mac');
 Function: Detect files that have \r as line ends (Mac encoding) and carps.
           Replace by \n if required, and saves the old file with the
           'backup' extension. If 'backup' is omitted, no backup is done.
 Returns : 0 if no Mac encoding is found. 1 if the encoded has been successfully
           performed.
 Args    : Named parameters 
             file    => File to parse. Mandatory, no default.
             replace => Replace \r by \n? Default
             backup

=cut

sub detectMacEncoding {
  my %args = ('file' => undef,
	      'replace' => 1,
	      'backup'  => '.mac',
	      @_);
	
	# Return value
	my $isMacEncoded;
	
	# Check that there is a file
  croak "No file argument" unless $args{'file'};
  
  # Open file, reads first line, if traces of \r and no \n then replace
  open my $fh, '<', $args{'file'} 
    or croak "Could not open file $args{'file'}: $!\n";
  my $first = <$fh>;
  my $isDoneReading = 1 unless (<$fh>);
  close($fh);
  my $hasLineEnd = chomp($first);
  my $hasReturnCarriage = ($first =~ /\r/);
  # Carp and return 0 if there are both CR and LE
  if ($hasReturnCarriage && $hasLineEnd){
    carp "Both line ends and carriage returns found. Not replacing".
    return 0;
  }
  # If only CR, copy to backup file, replace and print to new file
  if ($hasReturnCarriage && $isDoneReading && !$hasLineEnd){
    $isMacEncoded++;
    # Print to backup file
    if ($args{'backup'}){
      my $backupFile = $args{'file'} . $args{'backup'};
      open my $bak, '>', $backupFile 
        or croak "Could not write to $backupFile: $!\n";
      print $bak $first;
      close $bak;
    } 
    # Replace input file
    if ($args{'replace'}){
      $first =~ tr/\r/\n/;
      open $fh, '>', $args{'file'} 
        or croak "Could not write to $args{'file'}: $!\n";
      print $fh $first;
      close $fh;
    }
    return 1;
  } else {
    return 0;
  }
}


=head2 csv2tsv

 Title   : csv2tsv
 Usage   : csv2tsv('input' => $input_file,
                   'output'=> $ouput_file);
 Function: Convert CSV to TSV, taking care of embbedded commas and newlines
 Returns : 1 if success.
 Args    : Named parameters:
             input      => Input file. If undef, reads from STDIN
             output     => Output file. If undef, writes to STOUT
             quote_char => A character to protect text fields. None by default. 
                           You may need to escape the character ("\"").

=cut

sub csv2tsv {
  my %args = ('input'      => undef,
	      'output'     => undef,
	      'quote_char' => undef,
	      @_);
  # Input
  my $fhin;
  if ($args{'input'}){
    open $fhin, '<', $args{'input'} 
      or croak "Cannot open file $args{'input'}";
  } else {
    $fhin = \*STDIN;
  }

  # Output
  my $fhout;
  if ($args{'output'}){
    open $fhout, '>', $args{'output'} 
      or croak "Cannot open file $args{'output'}";
  } else {
    $fhout = \*STDOUT;
  }

  # Initiate reading and writing
  my $csv = Text::CSV->new( { binary => 1, 
			      eol => $/,
			      #allow_loose_quotes => 1,
			      #escape_char => undef,
			      #allow_loose_escapes => 1,
			    } )
    or die "Cannot use CSV: ".Text::CSV->error_diag ();
  my $tsv = Text::CSV->new ({ binary => 1, 
			      quote_char => $args{'quote_char'},
			      sep_char => "\t", 
			      eol => "\n" })
    or die "Cannot use TSV: ".Text::CSV->error_diag ();

  # Go through rows, print
  while (my $row = $csv->getline($fhin)) {
    my @fields = @$row;
    my @newfields;
    foreach my $field (@fields) {
      $field =~ s/\n//g;
      $field =~ s/\t//g;
      push @newfields, $field;
    }
    $tsv->print($fhout, \@newfields);
  }
  close $fhout if($args{'output'});
  return 1;
}

1;

