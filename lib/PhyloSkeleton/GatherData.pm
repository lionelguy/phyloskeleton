=head1 NAME

GatherData - Retrieving genome files from online resources

=head1 SYNOPSIS

my $res = getData('source'   => 'genbank',
                  'id'       => $assemblyId,
                  'path'     => 'ftp://path/to/data',
                  'shortName'=> 'Legionella pneumophila'); 

=head1 DESCRIPTION

This package gathers tools to retrieve data from the internet. So far the only sources are Genbank and JGI. Although the former is relatively stable, the second one is very experimental and might fail. It also relies on 'curl', which might not be available on all systems.

=head1 DEPENDENCIES

=over

=item curl

Necessary (so far) to retrieve data from JGI

=back

=head1 AUTHOR

Lionel Guy (L<<lionel.guy@imbim.uu.se>>)

=head1 DATE

Tue Nov  1 18:08:10 CET 2016

=head1 COPYRIGHT AND LICENSE

Copyright 2016 Lionel Guy

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

# Code begins here:

package PhyloSkeleton::GatherData;

# Libraries
use strict;
use Pod::Usage;
use Getopt::Long;
use File::Basename;
use File::Fetch;
use File::Path qw/make_path/;
use Carp;
use Cwd qw/ abs_path /;

use FindBin qw/$Bin/;
use lib "$Bin/lib";

use PhyloSkeleton qw/ sortTable /;
use PhyloSkeleton::Taxa qw/shortenName computerizeName/;
use PhyloSkeleton::TabFiles qw/ addColumn /;

# Export
require Exporter;
our @ISA = qw/Exporter/;
our @EXPORT_OK = qw/initJGI getData/;

# Constants
my %sources = ('jgi' => 1,
	       'genbank' => 1);


=head2 getData

 Title   : getData
 Usage   : getData('gref' => $gref);
 Function: Get remote data and store them locally
 Returns : An updated gref and the number of downloaded genomes
 Args    : Named parameters 
             gref        => A genome table, that is required to have a 
                            shortName column
             genomicData => A local folder to store data.
             jgilogin    => Valid JGI login, used if some genomes have 'jgi' as
                            source
             jgipassword => Password to JGI
             cookies     => Where to store cookies from JGI login

=cut

sub getData {
  my %args = ('gref'        => undef, 
	      'genomicData' => undef,
	      'jgilogin'    => undef,
	      'jgipassword' => undef,
	      'jgicookies'  => '.',
	      @_);

  my %toAdd;
  my $gref = $args{'gref'};
  croak "No 'gref' argument provided" unless $gref;
  croak "No 'genomicData' argument provided" unless $args{'genomicData'};
  my $initJGI;
  my $downloaded = 0;
  foreach my $id (@{ $gref->{'rows'} }){
    # Look if already set and don't set if it is; check that the paths exist
    my ($genomefile, $proteomefile) = ('-', '-');
    if ($gref->{'byrow'}{$id}{'genomeFile'}){
      $genomefile = $gref->{'byrow'}{$id}{'genomeFile'};
      croak "Given path $genomefile does not exist" 
	unless ($genomefile eq '-' || -e "$genomefile");
    }
    if ($gref->{'byrow'}{$id}{'proteomeFile'}){
      $proteomefile = $gref->{'byrow'}{$id}{'proteomeFile'};
      croak "Given path $proteomefile does not exist" 
	unless ($proteomefile eq '-' || -e "$proteomefile");
    }

    # Look only at assemblies not excluded and for which none of 
    # genomefile or proteomefile is set
    if ((!$gref->{'byrow'}{$id}{'excluded'} || 
	 ($gref->{'byrow'}{$id}{'excluded'} && 
	  ($gref->{'byrow'}{$id}{'excluded'} =~ /^0_/))) &&
	(($genomefile eq '-') && ($proteomefile eq '-'))){

      # Make sure that they have a shortName
      my $shortName = $gref->{'byrow'}{$id}{'shortName'};
      croak "No short name available for id $id" 
	unless ($shortName && $shortName ne '-');

      # Gather from remote
      my $ftpPath = $gref->{'byrow'}{$id}{'ftp_path'};
      my $cnt = 0;
      # JGI
      if ($gref->{'byrow'}{$id}{'source'} eq "jgi"){
	if ($args{'jgilogin'} && $args{'jgipassword'} && $args{'jgicookies'}){
	  unless ($initJGI){
	    initJGI('login'         => $args{'jgilogin'},
		    'password'      => $args{'jgipassword'},
		    'cookiesfolder' => $args{'jgicookies'});
	    $initJGI++;
	  }
	  ($genomefile, $proteomefile, $cnt) = 
	    _getJGIdata('jgiid'         => $id, 
			'shortName'     => $shortName, 
			'localFolder'   => $args{'genomicData'},
			'login'         => $args{'jgilogin'},
			'cookiesfolder' => $args{'jgicookies'},
			'debug'         => $args{'debug'});
	} else {
	  $gref->{'byrow'}{$id}{'excluded'} = '2_noJGIcreds';
	  carp "source at JGI, but no credentials provided. Skipping";
	}
      } 
      # Genbank
      elsif ($gref->{'byrow'}{$id}{'source'} eq "genbank"){
	croak "No FTP path available for id $id"
	  unless ($ftpPath && $ftpPath ne '-');
	($genomefile, $proteomefile, $cnt) = 
	  _getGenbankData('ftpPath'     => $ftpPath, 
			  'localFolder' => $args{'genomicData'},
			  'shortName'   => $shortName,
			  'debug'       => $args{'debug'});
      } else {
	$gref->{'byrow'}{$id}{'excluded'} = '2_sourceUnknown';
	carp "Source $gref->{'byrow'}{$id}{'source'} unknown. Skipping";
      }
      $downloaded += $cnt;
    }

    # What is returned is the relative path, we want absolute paths
    $genomefile = abs_path($genomefile) unless ($genomefile eq '-');
    $proteomefile = abs_path($proteomefile) unless ($proteomefile eq '-');

    # Report results: to toAdd if column is not present, directly to table
    # otherwise
    if ($gref->{'bycol'}{'genomeFile'}){
      $gref->{'byrow'}{$id}{'genomeFile'} = $genomefile;
    } else {
      $toAdd{'genomeFile'}{$id} = $genomefile;
    }
    if ($gref->{'bycol'}{'proteomeFile'}){
      $gref->{'byrow'}{$id}{'proteomeFile'} = $proteomefile;
    } else {
      $toAdd{'proteomeFile'}{$id} = $proteomefile;
    }

    # Exclude if no genomefile has been found and it is not already excluded
    $gref->{'byrow'}{$id}{'excluded'} = '2_nogenome' 
      if ($genomefile eq '-' && $proteomefile eq '-' && 
	  $gref->{'byrow'}{$id}{'excluded'} 
	  && $gref->{'byrow'}{$id}{'excluded'} =~ /^0_/ );
  }
  foreach my $key (keys %toAdd){
    $gref = addColumn('table'   => $gref,
		      'colname' => $key,
		      'col'     => $toAdd{$key});
  }
  print STDERR "Done: downloaded $downloaded genomes\n";
  return (sortTable('table' => $gref), $downloaded);
}

=head2 _getGenbankData

 Title   : _getGenbankData
 Usage   : _getGenbankData('ftpPath'     => 'ftp://path/to/folder,
                           'localFolder' => '/path/to/genomes',
                           'shortName'   => 'Spneu');
 Function: First check whether the data is available locally, and if not 
           get genomic and possibly proteomic data from genbank, and store it 
           locally
 Returns : A genome and a proteome fasta file, if available
 Args    : Named parameters 
             ftpPath     => A path to the ftp folder at genbank
             localFolder => A folder to store the file locally
             shortName   => Short name, computer-friendly, for the genome

 See also: getData

=cut

sub _getGenbankData {
  my %args = ('ftpPath'     => undef,
	      'localFolder' => '.',
	      'shortName'   => undef,
	      @_);

  # Silence Fetch warnings:
  $File::Fetch::WARN = 0;
  
  # Validate options
  croak "No ftpPath given" unless $args{'ftpPath'};
  croak "No shortName given" unless $args{'shortName'};
  my $debug = $args{'debug'};
  my ($assid, $dirs, $suffix) = fileparse($args{'ftpPath'});
  my $assemblyFolder = "$args{'localFolder'}/$args{'shortName'}";
  my $downloaded = 0;
  my ($genomefile, $proteomefile) = ('-', '-');

  # Inform
  print STDERR "  Working on $args{'shortName'}...";

  # Skip genome if folder is present and there is a .fna file associated
  unless (-d $assemblyFolder && -e "$assemblyFolder/${assid}_genomic.fna"){
    make_path "$assemblyFolder" unless (-d $assemblyFolder);

    # Local repository disabled so far
    # # Present in local data? 
    # if ($localData{$assid}){
    #   print STDERR " get from local repo...";
    #   my $cmd = "cp $localSrc/$assid/*.f?a.gz $assemblyFolder/";
    #   print STDERR "$cmd\n" if $debug;
    #   system $cmd;
    # } else {

    # Download and gunzip both if present
    $downloaded++;
    foreach my $ext ('genomic.fna', 'protein.faa'){
      print STDERR "fetching $ext file...";
      my $ff = File::Fetch->new(uri => "$args{'ftpPath'}/${assid}_$ext.gz");
      $ff->fetch( to => $assemblyFolder );
      if ($ff->error()){
	if ($ext eq 'genomic_fna'){
	  carp "Error downloading fna from $args{'ftpPath'}: " . $ff->error();
	} else {
	  print STDERR " not found...";
	}
      } else {
	print STDERR " found...";
      }
      if (-e "$assemblyFolder/${assid}_$ext.gz"){
	print STDERR " unzip... ";
	my $cmd = "gunzip $assemblyFolder/${assid}_$ext.gz";
	print STDERR "$cmd\n" if $debug;
	system($cmd);
      }
    }
  }

  # Check whether there is at least one or the other file.
  if (-e "$assemblyFolder/${assid}_genomic.fna"){
    print STDERR " found an fna file...";
    $genomefile = "$assemblyFolder/${assid}_genomic.fna";
  } else {
    print STDERR " found no suitable genome file...";
  }
  if (-e "$assemblyFolder/${assid}_protein.faa"){
    print STDERR " found an faa file...";
    $proteomefile = "$assemblyFolder/${assid}_protein.faa";
  }
  print STDERR " done\n";
  return ($genomefile, $proteomefile, $downloaded);
}

=head2 initJGI

 Title   : initJGI
 Usage   : initJGI('login'    => 'myname',
                   'password' => '123456');
 Function: Sign in at jgi
 Returns : 0 if failure (hopefully), 1 if success
 Args    : Named parameters 
             login    => A valid login to JGI
             password => ...
             cookies  => A folder to store cookies.
 See also:

=cut
# Init by logging in. Not sure if it works on its own or if a signon in a 
# browser is also needed

sub initJGI{
  my %args = ('login'         => undef,
              'password'      => undef,
              'cookiesfolder' => '.',
	      @_);
  # Check that correct credentials have been provided
  unless ($args{'login'} && $args{'password'}){
    print STDERR "  Skipping: sources at JGI, but no credentials provided\n";
    return 0;
  }
  print STDERR "  Signing in to JGI\n";
  my $cmd = "curl -s https://signon.jgi.doe.gov/signon/create --data-urlencode \'login=$args{'login'}\' --data-urlencode \'password=$args{'password'}\' -c $args{'cookiesfolder'}/cookies";
  print STDERR "$cmd\n" if $args{'debug'};
  open my $fh, '-|', $cmd or croak "Could not run command $cmd\n";
  my $logonres = <$fh>;
  my $result = 0;
  $result = 1 if ($logonres =~ /success/);
  return $result;
}


=head2 _getJGIdata

 Title   : _getJGIdata
 Usage   : _getJGIdata('jgiid'         => $jgiid,
                       'shortName'     => 'StrepPneu',
                       'localFolder'   => '/path/to/my/data',
                       'login'         => $login,
                       'cookiesFolder' => '/some/other/path')

 Function: Get data from JGI. This is not 100% bulletproof, but should be 
           relatively conservative and return (-, -, 0) if unsure.
           Requires curl installed on the computer.
 Returns : The genomefile, proteomefile and 1 if downloaded
 Args    : Named parameters 
             jgidid        => The JGI id
             shortName     => A short, computer-friendly name to store data
             localFolder   => A root folder to store data
             login         => login name
             cookiesFolder => Where cookies were stored by initJGI

 See also: initJGI, _getGenbankData, getData

=cut

# Get data from JGI. This is not 100% bulletproof, but should be relatively
# conservative and return '-' if unsure
sub _getJGIdata {
  my %args = ('jgiid'         => undef,
	      'shortName'     => undef,
	      'localFolder'   => '.',
	      'login'         => undef,
	      'cookiesfolder' => '.',
	      @_);
  my $jgiid = $args{'jgiid'};
  croak "No jgiid" unless $jgiid;
  my $shortName = $args{'shortName'};
  croak "No shortName for $jgiid" unless $shortName;
  my $assemblyFolder = "$args{'localFolder'}/$shortName";
  my $cookies = "$args{'cookiesfolder'}/cookies";
  my $debug = $args{'debug'};
  my $filename;
  my $tarOutputFolder;
  my ($genomefile, $proteomefile) = ('-', '-');
  my $downloaded = 0;

  ##############
  # Get portalid
  my $query =  "https://genome.jgi.doe.gov/ext-api/search-service/export?core=genome&query=" . $jgiid . "&searchIn=JGI%20Projects&searchType=Keyword&showAll=false&externallySequenced=true&sortBy=displayNameStr&showRestricted=false&showOnlyPublished=false&showSuperseded=true&sortOrder=asc&rawQuery=false&showFungalOnly=false&programName=all&programYear=all&superkingdom=--any--&scientificProgram=--any--&productCategory=--any--&start=0&rows=1000000";
  my $cmd = "curl -s \"$query\" -b $cookies -c $cookies";
  print STDERR "  Working on $shortName (id: $jgiid). retrieving portal ID... ";
  print STDERR "$cmd\n" if $debug;
  open XLS, '-|', $cmd or die "Cannot open io to $jgiid: $!\n";
  my $portalid;
  while (<XLS>){
    chomp;
    # Remove double quotes as they are in the way
    s/\"//g;
    if (/=HYPERLINK\([^,]+,([^,]+)\)/){
      $portalid = $1;
      close(XLS);
      last;
    }
  }
  unless ($portalid) {
    print STDERR "\n  Portal ID for $jgiid not found\n";
    return ('-', '-', '0');
  }
  print STDERR "Found portal ID $portalid... ";

  #########
  # Get xml
  # Skip if there is already a project
  if (-d $assemblyFolder && -e "$assemblyFolder/$shortName.fna"){
    print STDERR "\n  $assemblyFolder/$shortName exists, skipping\n";
  } else {
    my $cmd = "curl -s \"https://genome.jgi.doe.gov/ext-api/downloads/get-directory?organism=$portalid\" -b $cookies -c $cookies";
    print STDERR "getting XML...";
    print STDERR "$cmd\n" if $debug;
    open(XML, '-|', $cmd) or  
      print STDERR "  Cannot open io to $portalid: $!\n" && 
	return ('-', '-', '0');
    my $isReading = 0;
    my $url;

    ##########
    # Parse XML
    while (<XML>){
      chomp;
      if (/User $args{'login'} does not have access to/){
	print STDERR "  ERR: could not get $shortName (portalid $portalid)\n";
	return ('-', '-', '0');

      }
      $isReading = 0 if /<\/folder>/;
      $isReading++ if /name=\"IMG Data\"/;
      if (/filename="(\S+)\.tar\.gz".*url="(\S+)"/ && $isReading){
	$filename = $1;
	$url = $2;
	$url =~ s/&amp;/&/g;
	#last;
      }
    }
    close XML;
    my $exit = $? >> 8;
    unless ($filename && $url && $exit < 8) {
      print STDERR "  Could not find a relevant file for $shortName (portalid ".
	"$portalid)\n";
      return ('-', '-', '0');
    }
    # Grab the file
    $cmd = "curl -s \"https://genome.jgi.doe.gov/$url\" -b $cookies -c $cookies > $args{'localFolder'}/$filename.tar.gz";
    print STDERR " getting $filename.tar.gz...";
    print STDERR "$cmd\n" if $debug;
    my $retval = system($cmd);
    if ($retval){
      print STDERR "  ERR: could not retrieve tar file from JGI: $retval\n";
      return ('-', '-', '0');
    }
    $downloaded++;

    #######
    # untar
    # Test that on linux!
    $cmd = "tar -C $args{'localFolder'} -xvzf $args{'localFolder'}/$filename.tar.gz 2>&1 ";
    print STDERR "$cmd\n" if $debug;
    print STDERR " untarring";
    open(my $fh, '-|', $cmd) or 
      print STDERR "  ERR: could not untar tar file: $retval\n" &&
	return ('-', '-', '0');

    # Parse the tax output to get the tar output folder
    while (<$fh>){
      chomp;
      if (/\s*(\d+)\//){
	$tarOutputFolder = $1;
      }
    }
    close $fh;
    print STDERR ", tar output folder $tarOutputFolder... ";

    ##########################################
    # rename the folder and ID the right file; 
    print STDERR " moving...";
    if (-d $assemblyFolder){
      $cmd = "mv $args{'localFolder'}/$tarOutputFolder/* $assemblyFolder/";
    } else {
      $cmd = "mv $args{'localFolder'}/$tarOutputFolder $assemblyFolder";
    }
    print STDERR "$cmd\n" if $debug;
    $retval = system($cmd);
    if ($retval){
      print STDERR "  ERR: could not move file: $retval\n";
      return ('-', '-', '0');
    }
  }

  ##################################################
  # Loop through files, identify genome and proteome
  opendir my $dh, $assemblyFolder 
    or die "Cannot open dir $assemblyFolder: $!\n";
  my ($faa, $fna);
  while (my $file = readdir $dh){
    #print STDERR "  $file\n" if $debug;
    if ($tarOutputFolder && $file =~ /$tarOutputFolder(\..+)/){
      my $suffix = $1;
      $cmd = "mv $assemblyFolder/$file $assemblyFolder/$shortName$suffix";
      print STDERR "$cmd\n" if $debug;
      system($cmd) == 0 || die "Moving failed: $?\n";
      $file = "$shortName$suffix";
    }
    if ($file =~ /^$shortName(\..+)/){
      my $suffix = $1;
      if ($suffix eq ".genes.faa.gz"){
	system "gunzip $assemblyFolder/$shortName$suffix";
	$faa = "$assemblyFolder/$shortName.genes.faa";
      } elsif ($suffix eq ".genes.faa"){
	$faa = "$assemblyFolder/$shortName.genes.faa";
      } elsif ($suffix eq ".fna.gz"){
	system "gunzip $assemblyFolder/$shortName$suffix";
	$fna = "$assemblyFolder/$shortName.fna";	
      } elsif ($suffix eq ".fna"){
	$fna = "$assemblyFolder/$shortName.fna";
      }
    }
  }
  if ($faa){
    print STDERR "  found an faa file...";
    $proteomefile = $faa;
  }
  if ($fna){
    print STDERR "  found an fna file...";
    $genomefile = $fna;
  } else {
    print STDERR "  found no suitable genome file...";
  }
  print STDERR " done\n";
  return ($genomefile, $proteomefile, $downloaded);
}


1;
