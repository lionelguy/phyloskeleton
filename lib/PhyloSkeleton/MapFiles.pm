=head1 NAME

MapFiles - Read and write (protein) map files


=head1 DEPENDENCIES

=head1 AUTHOR

Lionel Guy (L<<lionel.guy@imbim.uu.se>>)

=head1 DATE

Mon Nov 21 13:17:46 CET 2016

=head1 COPYRIGHT AND LICENSE

Copyright 2016 Lionel Guy

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

# Code begins here:

package PhyloSkeleton::MapFiles;

# Libraries
use strict;
use warnings;
use Carp;

# Export
require Exporter;
our @ISA = qw/Exporter/;
our @EXPORT_OK = qw/ readMap writeMap /;

=head2 readMap

 Title   : readMap
 Usage   : readMap('file' => value1);
 Function: Read mapping files, which are tab-separated files with a title,
           preceded with a '#'. It must contain the following columns:
           'marker', 'organism', 'pid' and can contain more
 Returns : A hashref to the map, and a hashref with just the seen pids. 
           The hasherf to the map has the following structure:
              $h->{'marker'}{'organism'}{'pid'}{'extracol1'} = extracolval1
           Where extracol is any extra column in the file. If there are
           none then just '1' as the value to pid.
 Args    : Named parameters 
             file   => file where the map is stored

 See also:

=cut

sub readMap {
  my %args = ('file' => undef,
              @_);
  
  # Check variables
  croak "No 'file' argument" unless $args{'file'};
  
  # Initiate
  my %map;
  my %seqs;
  my %columns = ( 'marker'   => 1, 
                  'organism' => 1, 
                  'pid'      => 1); 
  
  # Open and read
  open my $fh, '<', $args{'file'} or croak "Could not open $args{'file'}: $!";
  
  # Title
  my $title = <$fh>;
  chomp $title;
  $title =~ s/^#//;
  my @cols = split(/\t/, $title);
  my @extracols;
  
  # Check that necessary columns are there
  foreach my $col (keys %columns){
    croak "No column $col in $args{'file'}" unless grep { /^$col$/ } @cols;
  }
  
  # Map columns
  foreach my $i (0..$#cols){
    push @extracols, $cols[$i] unless $columns{$cols[$i]};
    $columns{$cols[$i]} = $i;
  }
  
  # Read main file
  while (<$fh>){
    chomp;
    next if (/^#/);
    my @row = split;
    my $marker = $row[$columns{'marker'}];
    my $organism = $row[$columns{'organism'}];
    my $pid = $row[$columns{'pid'}];    
    # Populate
    if (@extracols){
      foreach my $extracol (@extracols){
        $map{$marker}{$organism}{$pid}{$extracol} = $row[$columns{$extracol}];
      } 
    } else {
      $map{$marker}{$organism}{$pid}++;
    }
    $seqs{$pid}++;
  }
  
  close($fh);
  # Return
  return (\%map, \%seqs);
}

1;
