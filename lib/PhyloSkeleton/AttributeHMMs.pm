=head1 NAME

AttributeHMMs - Attribute HMMs from a set of profiles to a set of genomes

=head1 DEPENDENCIES

=over

=item Parallel::Forkmanager

=item File::Share

=item BioPerl

=item HMMER (tested with 3.1b2)

=back

=head1 AUTHOR

Lionel Guy (L<lionel.guy@imbim.uu.se>)

=head1 DATE

Mon Nov 14 08:55:34 CET 2016

=head1 COPYRIGHT AND LICENSE

Copyright 2016 Lionel Guy

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

# Code begins here:

package PhyloSkeleton::AttributeHMMs;

# Libraries
use strict;
use warnings;
use Carp;

use Parallel::ForkManager;
use Bio::SeqIO;
use File::Share ':all';
use File::Path qw/make_path/;
use Scalar::Util qw(looks_like_number);

use PhyloSkeleton qw/ sortTable /;
use PhyloSkeleton::TabFiles qw/ newTable writeTable addColumn addRow /;

# Export
require Exporter;
our @ISA = qw/Exporter/;
our @EXPORT_OK = qw/ attributeHMMs /;

=head2 attributeHMMs

 Title   : attributeHMMs
 Usage   : attributeHMMs('gref' => $gref,
                         'hmms' => $hmms,
                         'prefix => 'myRun',
                         'output_folder => '/path/to/results');
 Function: For each given proteime, runs hmmersearch with the hmm profiles. 
           It then goes through all the hits and selects the best. If more than
           one hit is found that exceeds the threshold, it can create single-
           gene maps, that can be used to draw single-gene trees and eliminate 
           paralogs.
 Returns : A modifed gref and the number of searched proteomes. The only
           further annotations is the eventual modification of the 'excluded' 
           column and a new 'completeness' column with the fraction of 
           found markers. 
 Output  : All in output_folder. (f) means file, (d) directory
             <pref>.map (f)         => Main map, five columns: organism, marker,
                                       protein id, e-value, number of paralogs.
             <pref>.n.matrix (f)    => Matrix markers x organisms, with the 
                                       number of found matches for each
             <pref>.eval.matrix (f) => Matrix markers x organisms, as above,
                                       with e-values for each hit, 
                                       comma-separated
             <pref>.hmm_aligns (d)  => The HMM alignments are stored there
             <pref>.single_marker_maps (d) => Maps for single markers, useful to
                                       identify paralogs. Columns as in main map

 Args    : Named parameters 
             gref                 => A genome table reference to search for 
                                     hmms. Mandatory.
             prefix               => Prefix for the files. 'result' by default.
             hmms                 => A HMM profile file, as input to hmmsearch.
                                     Uses 15 Ribosomal proteins by default.
             output_folder        => Folder to store output. 'result' by def.
             annotate_rrnas       => Build maps for rRNAs as well?
             all_single_gene_maps => Output all single gene maps (not only those
                                     that have paralogs).
             evalue_threshold     => E-value threshold for hmmsearch. Default: 
                                     1e-6.
             completeness_threshold  => Threshold to warn and exclude genomes 
                                        with low number of genes. Default: 0.8.
             best_match_only      => If set, records only the best match in the
                                     main map. The individual gene maps are
                                     unaffected.
             cpus                 => Number of CPUs to use in total. Default 4.


 See also:

=cut

sub attributeHMMs {
  my %args = ('gref'                   => undef,
	      'prefix'                 => undef,
	      'output_folder'          => 'result',
	      'hmms'                => dist_file('PhyloSkeleton', 'RP15.hmm'),
	      'annotate_rrnas'         => 0,
	      'all_single_gene_maps'   => 0,
	      'evalue_threshold'       => 1e-6,
	      'completeness_threshold' => 0.8,
	      'best_match_only'        => 0,
	      'cpus'                   => 4,
	      @_);

  # Parse and validate options
  my $gref = $args{'gref'};
  my $output_folder = $args{'output_folder'};
  croak "No gref set" unless $gref;
  my $resPrefix = $args{'prefix'};
  unless ($resPrefix){
    $resPrefix = $output_folder eq '.' ? 'result' : $output_folder;
  }
  my $hmms = $args{'hmms'};
  my $dorrnas = $args{'annotate_rrnas'};
  my $forceSingleGeneMaps = $args{'all_single_gene_maps'};
  my $threads = $args{'cpus'};
  my $debug = $args{'debug'};

  # Variables
  my @orgs;
  my %orthologs;
  my $res_ref;
  my %seenids;

  # Store result file, parse sequentially after all hmms ran
  my %resultfiles;
  my %rRNAs;

  # Read HMM profiles
  my $hmms_ref = _parseHMMs('file' => $hmms);
  croak "No HMMs found in $hmms" unless ($hmms_ref);

  # Start ForkManager
  my $pm = new Parallel::ForkManager($threads);

  # Prepare run on finish
  $pm->run_on_finish(sub {
		       my ($pid, $exit_code, $ident, $exit_signal, $core_dump, 
			   $retvalref) = @_;
		       if ($retvalref){
			 $resultfiles{$retvalref->[0]} = $retvalref->[1];
		       } else {
			 croak "No value returned from pm sub: $exit_code\n";
		       }
		     });

  my $cnt = 0;
  foreach my $id (@{ $gref->{'rows'} } ){
    last if ($debug && $cnt > $debug);
    # Next if excluded
    next unless ($gref->{'byrow'}{$id}{'excluded'} =~ /^0_/);
    # Validate info for this assembly
    my $shortName = $gref->{'byrow'}{$id}{'shortName'};
    croak "No shortName provided for $id" 
      unless ($shortName && $shortName ne '-');
    my $proteomefile = $gref->{'byrow'}{$id}{'proteomeFile'};
    croak "No proteome file available for $id" 
      unless ($proteomefile && -e $proteomefile);
    my $r16S_fasta = $gref->{'byrow'}{$id}{'rRNA16S'};
    carp "No file for 16S rRNA for $id" 
      unless (!$dorrnas || ($r16S_fasta && -e $r16S_fasta));
    my $r23S_fasta = $gref->{'byrow'}{$id}{'rRNA23S'};
    carp "No file for 23S rRNA for $id" 
      unless (!$dorrnas || ($r23S_fasta && -e $r23S_fasta));
    # my $prefix = $gref->{'byrow'}{$id}{'prefix'};
    # croak "No prefix for $id" unless ($prefix && $prefix ne '-');

    print STDERR "  working on $shortName...\n";
    $cnt++;

    # Stack the names to get the right order
    push @orgs, $shortName;
    # Get rRNAs if so set
    _getrRNAs($shortName, $r16S_fasta, $r23S_fasta, \%rRNAs) if $dorrnas;
    ## Start PM. Run hmmsearch, push the short name and the result file
    my $pid = $pm->start and next;
    my $resultfile = _runHmmsearch($proteomefile, 
				   $hmms, 
				   "$output_folder/$resPrefix.hmm_aligns",
				   $args{'evalue_threshold'},
				   $shortName, 
				   $debug);
    my @return = ($shortName, $resultfile);
    print STDERR "  done with $shortName\n";
    $pm->finish(0, \@return);
    ## PM finished
  }
  $pm->wait_all_children;
  print STDERR "Done running hmmsearch for $cnt proteomes. Parsing the files\n";
    
  # Parse result files and check completeness along the way. Populate
  # the resref hashref
  my %completeness;
  my %exclude_names;
  foreach my $id (@{ $gref->{'rows'} }){
    my $shortName = $gref->{'byrow'}{$id}{'shortName'};
    if ($shortName && $shortName ne '-' && $resultfiles{$shortName}){
      print STDERR "  working on $shortName...";
      my $nhmms;
      ($res_ref, $nhmms) = _parseHMMalign($resultfiles{$shortName}, 
					  \%orthologs, 
					  \%seenids,
					  $shortName, 
					  $args{'evalue_threshold'});
      my $completeness = $nhmms / scalar(@$hmms_ref);
      $completeness{$id} = $completeness;
      if ($completeness < $args{'completeness_threshold'}){
	$gref->{'byrow'}{$id}{'excluded'} = '3_lowCompleteness';
	$exclude_names{$shortName}++;
      }
      print STDERR "done\n";
    } else {
      $completeness{$id} = '-'
    }
  }

  # Update gref with new column
  $gref = addColumn('table' => $gref,
                    'colname' => 'completeness',
                    'col' => \%completeness);

  # Print results, first matrices
  print STDERR "Searched $cnt proteomes. Now printing matrices\n";
  my ($nMatrix_ref, $evalMatrix_ref, $paralogs_ref) = 
    _createMatrices($res_ref, \@orgs, $hmms_ref, \%rRNAs);
  writeTable('file' => "$output_folder/$resPrefix.eval.matrix",
             'table' => $evalMatrix_ref);
  writeTable('file' => "$output_folder/$resPrefix.n.matrix",
             'table' => $nMatrix_ref);

  # Maps
  print STDERR "Now printing maps\n";
  _createPrintMaps("$output_folder/$resPrefix.map", 
		   "$output_folder/$resPrefix.single_marker_maps", 
		   $res_ref, 
		   \@orgs, 
		   $hmms_ref, 
		   $paralogs_ref, 
		   \%exclude_names,
		   \%rRNAs, 
		   $forceSingleGeneMaps,
		   $args{'best_match_only'});
  print STDERR "Finished\n";
  return (sortTable('table' => $gref), $cnt);
}

=head2 _parseHMMs

 Title   : _parseHMMs
 Usage   : _parseHMMs('file' => $file);
 Function: Parses the names of HMM profiles in a file containing several.
 Returns : An array ref.
 Args    : Named parameters 
             file   => File containing the HMM profiles

 See also:

=cut

sub _parseHMMs {
  my %args = ('file' => undef,
	      @_);
  my @names;
  my $nmarker = 0;
  print STDERR "Reading HMM profiles...";
  open HMMS, '<', $args{'file'} or die "Cannot open $args{'file'}\n";
  while (<HMMS>){
    next unless /^NAME/;
    my ($bla, $name) = split;
    push @names, $name;
    $nmarker++;
  }
  print STDERR " found $nmarker profiles in $args{'file'}\n";
  return \@names;
}

=head2 _parseHMMalign

 Title   : _parseHMMalign
 Function: Parses (roughly) the output of hmmsearch
 Returns : A hashref to results and the number of markers found
 Args    : file, result hashref, shortName

=cut

sub _parseHMMalign {
  my ($file, $res_ref, $seenids_ref, $shortName, $evalueThreshold) = @_;
  open HMMER, '<', $file or croak "Cannot open file $file: $!\n";
  my $nfound = 0;
  while (<HMMER>){
    next if /^#/;
    my ($pid, $dash, $hname, $hid, $evalue) = split;
    croak ("No pid at $_") unless $pid;
    # Skip if evalue under threshold
    if ($evalue < $evalueThreshold){
      $nfound++ unless $res_ref->{$hname}{$shortName};
      # Skip if evalue is lower
      # push to an hash of hashes, pid as key
      $res_ref->{$hname}{$shortName}{$pid} = $evalue;
      # Make sure that we haven't recorded the same pid for two genomes
      croak "non-unique ID: $pid both in $shortName and $seenids_ref->{$pid}\n" 
	if ($seenids_ref->{$pid} && $seenids_ref->{$pid} ne $shortName);
      $seenids_ref->{$pid} = $shortName;
    }
  }
  return ($res_ref, $nfound);
}

=head2 _runHmmsearch

 Title   : _runHmmsearch
 Function: Runs hmmsearch
 Returns : The file name of the resulting tab
 Args    : proteomefile, hmm name array, output result folder, evalue threshold
           shortName and prefix. And debug.

=cut

sub _runHmmsearch {
  my ($proteomefile, $hmms, $hmmResFolder, $evalueThreshold, 
      $shortName, $debug) = @_;
  make_path($hmmResFolder) unless -d ($hmmResFolder);
  croak "E-value $evalueThreshold doesn't look like a number" 
    unless (looks_like_number($evalueThreshold));
  my $cmd = "hmmsearch -E $evalueThreshold --tblout $hmmResFolder/$shortName.tab --cpu 1 $hmms $proteomefile > $hmmResFolder/$shortName.ali";
  print STDERR "$cmd\n" if $debug;
  system($cmd);
  return "$hmmResFolder/$shortName.tab";
}

=head2 _getrRNAs

 Title   : _getrRNAs
 Function: Get ids for rRNAs
 Returns : An updated rRNA hash ref
 Args    : shortName, fasta files for 16S and 23S, and the original rRNAs hash

=cut

sub _getrRNAs {
  my ($shortName, $r16S_fasta, $r23S_fasta, $rRNAs_r) = @_;
  if ($r16S_fasta && $r16S_fasta ne "-"){
    my $r16Sin = Bio::SeqIO->new(-file => $r16S_fasta,
				 -format => 'fasta');
    my $seq = $r16Sin->next_seq;
    $rRNAs_r->{$shortName}{'16S_rRNA'} = $seq->id;
    die "More than one sequence in $r16S_fasta\n" if ($r16Sin->next_seq);
  }
  if ($r23S_fasta && $r23S_fasta ne "-"){
    my $r23Sin = Bio::SeqIO->new(-file => $r23S_fasta,
				 -format => 'fasta');
    my $seq = $r23Sin->next_seq;
    $rRNAs_r->{$shortName}{'23S_rRNA'} = $seq->id;
    die "More than one sequence in $r23S_fasta\n" if ($r23Sin->next_seq);
  }
  return $rRNAs_r;
}

=head2 _createMatrices

 Title   : _createMatrices
 Function: Generates two tables, one with the number of markers, the other one
           with the e-values
 Returns : Two table refs
 Args    : result, organism, HMM and rRNA hashrefs

=cut

sub _createMatrices {
  my ($res_ref, $orgs_ref, $hmms_ref, $rRNAs_r) = @_;
  # Store paralogs in this hash;
  my %paralogs;
  # Initiate the two tables
  my @columns = @$hmms_ref;
  @columns = (@columns, '16S_rRNA', '23S_rRNA') if (scalar(keys %{$rRNAs_r} ));
  my $nMatrix = newTable('columns' => [ ('shortName', @columns) ],
                         'rowname' => 'shortName');
  my $evalMatrix = newTable('columns' => [ ('shortName', @columns) ],
                            'rowname' => 'shortName');
  
  # Populate tables
  foreach my $org (@{ $orgs_ref }){
    my %nRow;
    $nRow{'shortName'} = $org;
    my %evalRow;
    $evalRow{'shortName'} = $org;
    foreach my $hmm (@columns){
      # Skip
      # $res_ref->{$hmm}{$org} is  an array of hashes
      # dereference first, then sort by increasing evalues, check
      # how many and warn if more than one; add to a hash with paralog-
      # containing hash, and come back to these later
      my %pids;
      my $n = 0;
      my $evals = '-';
      if ( $res_ref->{$hmm}{$org} ){
	%pids = %{ $res_ref->{$hmm}{$org} };
	$n = scalar(keys %pids);
	# Tag if more than one
	$paralogs{$hmm}++ if $n > 1;
	my @evalues = sort { $a <=> $b } values %pids;
	$evals = join(',', @evalues);
      }
      $nRow{$hmm} = $n;
      $evalRow{$hmm} = $evals;
    }
    if (scalar(keys %{ $rRNAs_r } )){
      foreach my $mol ('16S_rRNA', '23S_rRNA'){
        $evalRow{$mol} = '-';
        my $n = 0;
	$n = 1 if ($rRNAs_r->{$org}{$mol});
	$nRow{$mol} = $n;
      }
    }
    $nMatrix = addRow('table' => $nMatrix,
                      'row'   => \%nRow);    
    $evalMatrix = addRow('table' => $evalMatrix,
			 'row'   => \%evalRow);
  }
  # Return paralog-containing hash
  return ($nMatrix, $evalMatrix, \%paralogs);
}

=head2 _createPrintMaps

 Title   : _createPrintMaps
 Function: Creates and prints maps for global och single-gene maps. 
 Returns : Two table refs, each table has five columns: organism, hmm, pid,
           e-value from hmmsearch, and the number of proteins for the same
           organism and marker.
 Args    :

=cut

sub _createPrintMaps {
  my ($map, $out_folder, $res_ref, $orgs_ref, $hmms_ref, $paralogs_ref, 
      $exclude_names_ref, $rRNAs_r, $forceSingleGeneMaps, $bestMatchOnly) = @_;
  open MAP, '>', $map or croak "Cannot open map $map: $!\n";
  # Print title
  my $title = "#organism\tmarker\tpid\tevalue\tnparalogs\n";
  print MAP $title;
  
  # Checks that the single-map folder exists, creates it otherwise
  make_path($out_folder) if ($out_folder && !(-d $out_folder) && 
	($paralogs_ref || $forceSingleGeneMaps));
  
  # Loop over markers first, then organisms
  foreach my $hmm (@{ $hmms_ref }){
    my $doSingleMap = 
      $out_folder && ($paralogs_ref->{$hmm} || $forceSingleGeneMaps);
    print STDERR "  Marker $hmm has paralogs for $paralogs_ref->{$hmm} " . 
      "organisms\n" if ($paralogs_ref->{$hmm});
      
    # Open single-marker file and print title
    if ($doSingleMap){
      open SINGLE, '>', "$out_folder/$hmm.map";
      print SINGLE $title;
    }
    foreach my $org (@{ $orgs_ref }){
      # Don't include orgs for which completeness is below threshold
      next if ($exclude_names_ref->{$org});
      # $res_ref->{$hmm}{$org} is now a hash of hashes
      # dereference first, then sort by increasing evalues, check
      # how many and warn if more than one; add to a hash with paralog-
      # containing hash, and come back to these later
      if ($res_ref->{$hmm} && $res_ref->{$hmm}{$org}){
        my %pids = %{ $res_ref->{$hmm}{$org} };
	my @pids = sort { $pids{$a} <=> $pids{$b} } keys %pids;
	my $npids = scalar(@pids);
	foreach my $i (0..$#pids){
	  # Print to the main map, but not if the best match only is requested
	  # and this is not the lowest evalue; to the single map in all cases
	  print MAP "$org\t$hmm\t$pids[$i]\t$pids{$pids[$i]}\t$npids\n" 
	    unless($bestMatchOnly && $i > 0);
	  print SINGLE "$org\t$hmm\t$pids[$i]\t$pids{$pids[$i]}\t$npids\n"
	    if ($doSingleMap);
	}
      }
    }
    close SINGLE;
  }
  if ($rRNAs_r){
    foreach my $mol ('16S_rRNA', '23S_rRNA'){
      if ($forceSingleGeneMaps){
        open SINGLE, '>', "$out_folder/$mol.map";
        print SINGLE $title;
      }
      foreach my $org (@{ $orgs_ref }){
	      if ($rRNAs_r->{$org}{$mol}){
	        print MAP "$org\t$mol\t" . $rRNAs_r->{$org}{$mol} . "\t-\t1\n";
	        print SINGLE "$org\t$mol\t" . $rRNAs_r->{$org}{$mol} . "\t-\t1\n"
	          if($forceSingleGeneMaps);
	      }
      }
    }
    close SINGLE if $forceSingleGeneMaps;
  }
  close MAP;
}

1;
