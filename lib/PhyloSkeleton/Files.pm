=head1 NAME

Files - Handling files in PhyloSkeleton

=head1 SYNOPSIS

downloadIfOlder('localFile' => $myLocalFile,
                'remoteFile'=> 'http://some.file/tab.txt'
                'age' => 7);

=head1 AUTHOR

Lionel Guy (L<lionel.guy@imbim.uu.se>)

=head1 DATE

Thu Oct 27 17:12:32 CEST 2016

=head1 COPYRIGHT AND LICENSE

Copyright 2016 Lionel Guy

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

# Code begins here:

package PhyloSkeleton::Files;

# Libraries
use strict;
use warnings;
use Carp;
use File::Temp qw/ tempfile tempdir /;
use File::Fetch;
use File::Copy;

# Export
require Exporter;
our @ISA = qw/Exporter/;
our @EXPORT_OK = qw/downloadIfOlder/;

=head2 downloadIfOlder

 Title   : downloadIfOlder
 Usage   : downloadIfOlder('localFile' => $myLocalFile,
                           'remoteFile'=> 'http://some.file/tab.txt'
                           'age' => 7);
 Function: Downloads a file to a specific file if another file is not present 
           or older than so many days ('age')
 Returns : 1 if success
 Args    : Named parameters 
             localFile   => A local file
             remoteFile  => Remote uri to be passed to File::Fetch
             age         => Refresh the file if the local file is older than
                            so many days. 0 always refresh, -1 never refreshes
                            Default: 7

=cut

sub downloadIfOlder {
  my %args = ('localFile' => undef,
	      'remoteFile'=> undef,
	      'age'       => 7,
	      @_);
  my $download = 0;
  
  # If file doesn't exists
  if (! (-s $args{'localFile'})){
    $download++;
  } elsif (-s $args{'localFile'} && $args{'age'} != -1){
    # If it exists and the age is not set to -1
    my $timediff = time - (stat("$args{'localFile'}"))[9];
    # If timediff is greater than age => download
    if ($timediff > $args{'age'} * 3600 * 24){
      $download++;
    }
  }
  if ($download){
    print STDERR "Downloading $args{'remoteFile'} to $args{'localFile'}\n";
    my $ff = File::Fetch->new(uri => $args{'remoteFile'});
    my $where = $ff->fetch( to => tempdir( CLEANUP => 1 ));
    croak "Error downloading taxdump file from $args{'uri'}: " . $ff->error() 
      if $ff->error();
    move($where, $args{'localFile'}) or croak "Could not move file $where " . 
      "to $args{'localFile'}: $!";
  }
  return $download;
}
1;
