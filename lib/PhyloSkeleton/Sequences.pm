=head1 NAME

Sequences - Handle fasta files and prepare input for phylogeny inference

=head1 DEPENDENCIES

=over

=item BioPerl

=back

=head1 AUTHOR

Lionel Guy (L<lionel.guy@imbim.uu.se>)

=head1 DATE

Mon Nov 21 15:34:55 CET 2016

=head1 COPYRIGHT AND LICENSE

Copyright 2016 Lionel Guy

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

package PhyloSkeleton::Sequences;

# Libraries
use strict;
use warnings;
use Carp;
use File::Path qw/make_path/;

use Bio::SeqIO;

use PhyloSkeleton::MapFiles qw/ readMap /;

# Export
require Exporter;
our @ISA = qw/Exporter/;
our @EXPORT_OK = qw/ fastasFromMap /;

=head2 fastasFromMap

 Title   : fastasFromMap
 Usage   : fastasFromMap('table' => $gref,
                         'map'   => 'path/to/result.map');
 Function: Create per-marker fasta files from a map and an assembly table that
           contains proteome files.
 Returns : The number of files created and a modified table ref.
 Args    : Named parameters:
             table         => A assembly table, containing the location of 
                              the proteomefiles.
             map           => The map of markers, as produced by attributeHMMs.
             fasta_folder  => A folder to output fasta files. Default: 'fasta'.
             include_rrnas => Should rRNAs (16 and 23S) be included?

 See also: attributeHMMs

=cut

sub fastasFromMap {
  my %args = ('table'           => undef,
	      'map'             => undef,
	      'fasta_folder'    => 'fasta',
	      'include_rrnas'   => 0,
	      @_);

  # Validate options
  my $gref = $args{'table'};
  croak "No 'table' set" unless $gref;
  my $mapfile = $args{'map'};
  croak "No 'map' set" unless $mapfile;
  my $fastaDir = $args{'fasta_folder'};
  make_path($fastaDir) unless (-d $fastaDir);
  my $dorrnas = $args{'include_rrnas'};
  my $best_match_only = $args{'best_match_only'};
  my $debug = $args{'debug'};

  # Variables
  my %uids;
  my @orgs;

  # Read map
  print STDERR "Reading map $mapfile...";
  my ($map, $seqs) = readMap('file' => $mapfile);
  print STDERR " found ", scalar(keys(%{ $seqs })), " ids for ", 
    scalar(keys(%{ $map })), " hmms\n";

  # Read fastas through gref
  print STDERR "Reading fastas in...";
  my $cnt = 0;
  my $nids = 0;
  foreach my $assid (@{ $gref->{'rows'} }){
    # Go away if not included
    next unless ($gref->{'byrow'}{$assid}{'excluded'} =~ /^0_/);
    $cnt++;
    last if ($debug && $cnt > $debug);
    # Get the info needed for each id
    my $shortName    = $gref->{'byrow'}{$assid}{'shortName'};
    #my $prefix       = $gref->{'byrow'}{$assid}{'prefix'};
    my $proteomefile = $gref->{'byrow'}{$assid}{'proteomeFile'};
    my $r16S_fasta   = $gref->{'byrow'}{$assid}{'rRNA16S'};
    my $r23S_fasta   = $gref->{'byrow'}{$assid}{'rRNA23S'};
    # Make sure we have what we need
    croak "No shortName for id $assid" unless ($shortName && $shortName ne '-');
    #croak "No prefix for id $assid" unless ($prefix && $prefix ne '-');
    croak "No proteomeFile for id $assid" 
      unless ($proteomefile  && $proteomefile ne '-');
    # Push the org name
    push @orgs, $shortName;
    # Open fasta files for each organism, including rRNAs if wanted
    my @fastas = ($proteomefile);
    if ($dorrnas){
      foreach my $file ($r16S_fasta, $r23S_fasta){
        if ($file && $file ne "-"){
	  push @fastas, $file;
	}
      }
    }
    # Traverse the fasta and retrieve the selected pids
    foreach my $fasta (@fastas){
      my $fastaIn = Bio::SeqIO->new(-file => $fasta);
      while (my $seq = $fastaIn->next_seq){
	my $pid = $seq->id;
	if ($seqs->{$pid}){
	  $seqs->{$pid} = $seq;
	  $nids++;
	  # Check that ids are unique, that a previous record is not erased
	  croak "Non-uniqe ids: $pid is present in $fasta file and " . 
	    "one more file" if $uids{$pid};
	  $uids{$pid}++;
	}
      }
    }
  }
  print STDERR "recorded $nids ids in $cnt fastas\n";

  # Check that everyone's here
  # foreach my $key (keys %{ $seqs }){
  #   #print "Key: $key, value: ", $seqs->{$key}, "\n";
  #   #unless ($seqs->{$key}{'primary_id'} && $seqs->{$key}->isa("Bio::Seq")) {
  #   unless (ref($seqs->{$key}) && $seqs->{$key}->isa("Bio::Seq")) {
  #     croak "Seq $key has no fasta record associated\n";
  #   }
  # }
  
  # Dispatch the sequences in their respective files; recode description to 
  # get the name in it
  print STDERR "Dispatching fasta records...\n";
  my $nhmms = 0;
  foreach my $hmm (keys %{ $map }){
    $nhmms++;
    my ($norgs, $npids) = (0, 0);
    print STDERR "  working on HMM $hmm...";
    my $fastaOut = Bio::SeqIO->new(-file => ">$fastaDir/$hmm.fasta",
				 -format => 'fasta');
    foreach my $org (@orgs){
      if ($map->{$hmm}{$org}){
        $norgs++;
        my %pids = %{ $map->{$hmm}{$org} };
        carp "WARNING: more than one copy for marker $hmm in org $org. " . 
          "Any attempt to concatenate the proteins based on the same map " . 
          "will fail. Remove the extra paralog in $mapfile and rerun the " .
          "pipeline with the updated map" if (scalar (keys %pids) > 1);
        foreach my $pid (keys %pids){
	  $npids++;
	  my $seq = $seqs->{$pid};
	  #my $oldid = $seq->id;
	  #$seq->id($org);
	  $seq->desc($org . "|" . $seq->desc);
	  $fastaOut->write_seq($seq);
        }
      }
    }
    print STDERR " found $npids markers in $norgs organisms\n";
  }
  print STDERR "Done for $nhmms HMMs, data is in $fastaDir\n";
  return ($gref, $nhmms);

}

1;
