=head1 NAME

Taxa - Taxon manipulation utilities

=cut

package PhyloSkeleton::Taxa;
use strict;
use warnings;
use Carp;
use List::Util qw/ max /;

require Exporter;
our @ISA = qw/Exporter/;
our @EXPORT_OK = qw/shortenName shortenNameWGene parseGI computerizeName completeNameNCBI/;

=head2 computerizeName

Remove unwanted strange characters and keeps only alphannumeric and underscores.

=cut

sub computerizeName {
  my $name = shift @_;
  $name =~ s/\W+/_/g;
  return $name;
}

=head2 shortenName

Shortens a taxon name:
- removes potentially trailing uidXXXX
- changes Candidatus to Ca
- shortens to 30 chars
- removes potential trailing underscore

=cut 
sub shortenName {
    my ($long, $maxchar) = @_;
    $maxchar = 30 unless $maxchar;
    my @bits = split(/_/, $long);
    # remove uid
    pop @bits if ($bits[$#bits] =~ /^uid/);
    $bits[0] = 'Ca' if ($bits[0] eq 'Candidatus');
    my $name = join('_', @bits);
    # replace remaining strange characters by underscores
    $name =~ s/\W/_/g;
    # shorten to max char
    $name = substr($name, 0, $maxchar-1) if (length($name) >= $maxchar);
    # Remove trailing underscore
    $name =~ s/_+$//;
    return $name;
}

=head2 shortenNameWGene

Shortens a taxon name, as in shortenName, but add gene name at the end. Limits the length of the name to maxchar.

=cut
sub shortenNameWGene {
    my ($long, $gene, $maxchar) = @_;
    $maxchar = 30 unless $maxchar;
    my @bits = split(/_/, $long);

    # remove uid
    pop @bits if ($bits[$#bits] =~ /^uid/);
    $bits[0] = 'Ca' if ($bits[0] eq 'Candidatus');
    my $name = join('_', @bits);

    # add gene, shorten to maxchar char
    croak "Gene name too long $gene\n" if length($gene) > ($maxchar-3);
    $name = substr($name, 0, $maxchar-2-length($gene)) 
	if (length($name) >= $maxchar-1-length($gene));

    # Remove trailing underscore
    $name =~ s/_+$//;
    return $name . "_" . $gene;
}

=head2 parseGI

Parse a GI header like gi|332797784|ref|YP_004459284.1| and returns the second field by default.

=cut
sub parseGI {
    my ($id, $field) = @_;
    $field = 2 unless $field;
    my @fields = split(/\|/, $id);
    croak "Not enough fields in $id" unless ($field <= scalar(@fields));
    return $fields[$field-1];
}

=head2 completeNameNCBI

Form the organism name, the intraspecific name and the isolate contained in the assembly_summary_genbank file, returns a complete name

=cut

sub completeNameNCBI {
  my ($on, $in, $is) = @_;
  # Parse the identifier of in:
  if ($in){
    $in =~ s/^\w+=//;
    # add the strain unless there is substantial overlap
    $on .= " " . $in unless _hasOverlap($on, $in, 5);
  }
  # add the isolate unless overlap
  $on .= " " . $is if ($is && ! _hasOverlap($on, $is, 5));
  return $on;
}

=head2 _hasOverlap

Finds occurences of substrings (min length by default 5) of a string in
another string. Returns 1 if there is a match, 0 otherwise.

Input: first string, second string, minimum overlap.

=cut

sub _hasOverlap {
  my ($s1, $s2, $min_ol) = @_;
  # Avoid bad surprises when doing regexp
  $s1 =~ s/\W/_/g;
  $s2 =~ s/\W/_/g;
  $min_ol = 5 unless $min_ol;
  my $l2 = length($s2);
  my $imax = max($l2, $min_ol);
  #print STDERR "$s1 <=> $s2\n";
  my $match;
  for my $i (0..($imax-$min_ol)){
    my $sub = substr($s2, $i, $min_ol);
    #print STDERR "  $sub\n";
    return 1 if ($s1 =~ /$sub/i);
  }
  return 0;
}

1;
