=head1 NAME

PhyloSkeleton::Annotate - automatic annnotation of prokaryotic genomes

=head1 DEPENDENCIES

=over

=item prokka or prodigal

=item barrnap for rRNAs (optional)

=back

=head1 AUTHOR

Lionel Guy (L<<lionel.guy@imbim.uu.se>>)

=head1 DATE

Thu Nov 10 14:43:39 CET 2016

=head1 COPYRIGHT AND LICENSE

Copyright 2016 Lionel Guy

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

# Code begins here:

package PhyloSkeleton::Annotate;

# Libraries
use strict;
use warnings;
use Carp;

use File::Basename;
use List::Util 'shuffle';
use File::Copy;
use File::Path qw/make_path/;
use Scalar::Util qw(looks_like_number);
use Parallel::ForkManager;
use POSIX qw (floor);
use Bio::Tools::GFF;
use Bio::SeqIO;

use PhyloSkeleton qw/ sortTable /;
use PhyloSkeleton::TabFiles qw/ addColumn /;

# Export
require Exporter;
our @ISA = qw/Exporter/;
our @EXPORT_OK = qw/ annotate annotateCDS annotaterRNA /;

# Global constants
our %annotators = ( 'prokka'   => 1,
		    'prodigal' => 1);
our @suff = qw/fna faa/;


=head2 annotate

 Title   : annotate
 Usage   : annotate('gref'                => $gref,
                    'annotation_software' => 'prodigal',
                    'annotate_rrnas'      => 1);
 Function: Annotate a collection of genomes. The annotations files are placed 
           in a sbufolder in the same folder as the genome file and a copy
           of the annotated genes (faa, fasta) is copied in the same folder.
 Returns : A modified gref, with a completed 'proteomefile' column and
           the number of annotated genomes.
 Args    : Named parameters 
             gref                => Genome table. Mandatory.
             annotation_software => Either 'prodigal' (default) or 'prokka'
             annotate_rrnas      => Annotate the ribosomal rRNAs (16 and 23S)?
                                    Default is no.
             cpus                => Number of CPUs to use in total. Default 4.
             threads_per_process => Number of threads per process. Default 4.
             force               => Force reannotation.

=cut

sub annotate {
  my %args = ('gref'                => undef,
	      'annotation_software' => 'prodigal',
	      'annotate_rrnas'      => 0,
	      'cpus'                => 4,
	      'threads_per_process' => 4,
	      'force'               => 0,
	      @_);

  # Variables
  my %prefixes;
  #my @rows;
  my %toAdd;
  our $nannot = 0;

  # Options; validate
  my $gref = $args{'gref'};
  croak "No gref set" unless $gref;
  my $annotator = $args{'annotation_software'};
  croak "Annotation software can only be one of ". 
    join(", ", sort(keys %annotators)) unless ($annotators{$annotator});
  my $dorrnas = $args{'annotate_rrnas'};
  my $threads = $args{'cpus'};
  my $nthreads_per_process = $args{'threads_per_process'};
  my $force = $args{'force'};
  my $debug = $args{'debug'};

  # Start ForkManager; one wants to give 'threads_per_process' threads to 
  # each process, ergo divdide threads
  my $nprocesses = int($threads/$nthreads_per_process);
  if ($nprocesses < 1){
    $nprocesses = 1;
    $nthreads_per_process = $threads;
  }
  my $pm = new Parallel::ForkManager($nprocesses);

  # Prepare run on finish
  $pm->run_on_finish(sub {
		       my ($pid, $exit_code, $ident, $exit_signal, 
			   $core_dump, $res_ref) = @_;
		       if ($res_ref){
			 #push @rows, [ @$res_ref ];
			 $nannot += $res_ref->{'n'};
			 delete $res_ref->{'n'};
			 foreach my $key (keys %{ $res_ref }){
			   $toAdd{$key}{$res_ref->{'id'}} = $res_ref->{$key}
			     unless ($key eq 'id');
			 }
		       } else {
			 croak "No value returned from pm sub\n";
		       }
		     });

  # Go through assemblies
  my ($nrows, $nannotated) = (0, 0);
  foreach my $id (@{ $gref->{'rows'}}){
    my $shortName = $gref->{'byrow'}{$id}{'shortName'};
    croak "No shortName for $id" unless $shortName;

    my $genomefile = $gref->{'byrow'}{$id}{'genomeFile'};
    my $proteomefile = $gref->{'byrow'}{$id}{'proteomeFile'};
    my $rrnafile = $gref->{'byrow'}{$id}{'rRNA16S'};
    my $excluded = $gref->{'byrow'}{$id}{'excluded'};
    $proteomefile = '-' unless $proteomefile;
    my $prefix = $gref->{'byrow'}{$id}{'prefix'};
    $prefix = '-' unless $prefix;

    # If there is no genomefile, the assembly is not selected, "return" nothing
    if (!$genomefile || !($excluded && $excluded =~ /^0_/) 
	|| !(-e $genomefile)){
      #print STDERR "  $shortName: no genome file or not selected: skip\n";
      $toAdd{'proteomeFile'}{$id} = $proteomefile;
      $toAdd{'prefix'}{$id} = $prefix;
      if ($dorrnas){
	$toAdd{'rRNA16S'}{$id} = '-';
	$toAdd{'rRNA23S'}{$id} = '-';
      }
    } else {
      my $species = $gref->{'byrow'}{$id}{'species'};
      my $genus = $gref->{'byrow'}{$id}{'genus'};
      my $gcode = $gref->{'byrow'}{$id}{'gcode'};
      my $kingdom = $gref->{'byrow'}{$id}{'superkingdom'};
      # Give it a prefix here
      $prefix = _generatePrefix(\%prefixes, $shortName, $species, $genus, 
				$prefix);

      ## PM starts here
      $pm->start and next;
      # Annotate proteins if necessary
      my $annotated = 0;
      if ($proteomefile =~ /\.faa$/ && -e $proteomefile && !$force){
	print STDERR "  $shortName: found protein file\n";
      } else {
	print STDERR "  $shortName: annotate with $annotator\n";
	$proteomefile = annotateCDS('genomefile' => $genomefile, 
				    'prefix'     => $prefix, 
				    'shortName'  => $shortName, 
				    'kingdom'    => $kingdom, 
				    'gcode'      => $gcode, 
				    'annotator'  => $annotator,
				    'nthreads_per_process' =>
				    $nthreads_per_process,
				    'force'      => $force);
	$annotated++;
      } 
      my %add = ('id'           => $id,
		 'proteomeFile' => $proteomefile,
		 'prefix'       => $prefix,
		 'n'            => $annotated);
      # Annotate rRNAs if necessary
      my ($rRNA16S, $rRNA23S) = ('-', '-'); 
      if ($dorrnas){
	if ($rrnafile && -e $rrnafile) {
	  print STDERR "  $shortName: found rRNA file\n";
	} else {
	  print STDERR "  $shortName: annotate rRNAs\n";
	  ($rRNA16S, $rRNA23S) = 
	    annotaterRNA('genomefile' => $genomefile, 
			 'prefix'     => $prefix, 
			 'shortName'  => $shortName, 
			 'kingdom'    => $kingdom,
			 'nthreads_per_process' => $nthreads_per_process,
			 'force'      => $force);
	}
	$add{'rRNA16S'} = $rRNA16S;
	$add{'rRNA23S'} = $rRNA23S;
      }
      print STDERR "  $shortName: done\n";
      $pm->finish(0, \%add, $annotated);
      ## PM ends here
    } 
  }
  # Waits for all to return before going further
  $pm->wait_all_children;

  # Add columns
  foreach my $key (keys %toAdd){
    if ($gref->{'bycol'}{$key}){
      foreach my $id (@{ $gref->{'rows'} }){
	$gref->{'byrow'}{$id}{$key} = $toAdd{$key}{$id};
      } 
    } else {
      $gref = addColumn('table'   => $gref,
			'col'     => $toAdd{$key},
			'colname' => $key);
    }
  }

  # Return
  print STDERR "Annotated $nannot genomes. Done.\n";
  return (sortTable('table' => $gref), $nannot);
}

=head2 annotateCDS

 Title   : annotateCDS
 Usage   : annotateCDS('genomefile' => $fna,
                       'prefix' => 'SpnMX2',
                       'shortName' => 'Streptococcus_pneumo_MX2',
                       'kingdom'  => 'Bacteria');
 Function: Annotate one genome
 Returns : A fasta protein file
 Args    : Named parameters 
             genomefile => A fasta genome file
             prefix     => A unique prefix
             shortName  => Short name for the organism
             kingdom    => Domain or kingdom. Default: Bacteria
             gcode      => Genetic code. Default: 11
             annotator  => Annotation program. Either 'prodigal' or 'prokka'.
                          Must be available from $PATH
             nthreads_per_process => Number of threads to use in annotator. 
                                     Default 4.
             force      => Force reannotation of genomes already annotated with 
                          this package.


=cut

## Use prokka or prodigal directly
sub annotateCDS {
  my %args = ('genomefile' => undef, 
	      'prefix'     => undef,
	      'shortName'  => undef,
	      'kingdom'    => 'Bacteria',
	      'gcode'      => 11,
	      'annotator'  => 'prodigal',
	      'nthreads_per_process' => 4,
	      'force'      => 0,
	      @_);
  # Validate options
  my $genomefile = $args{'genomefile'};
  croak "Fasta file 'genomefile' $genomefile not set" unless $genomefile;
  croak "Fasta file $genomefile not existing" unless (-e $genomefile);
  my $prefix = $args{'prefix'};
  croak "No 'prefix' set" unless $prefix;
  my $shortName = $args{'shortName'};
  croak "No 'shortName' set" unless $shortName;
  my $kingdom = $args{'kingdom'};
  my $gcode = $args{'gcode'};
  unless (looks_like_number($gcode) && $gcode > 0 && $gcode < 31){
    carp "Genetic code $gcode for $shortName doesn't look valid. " . 
      "Using 11 instead";
    $gcode = 11;
  }
  my $annotator = $args{'annotator'};
  my $debug = $args{'debug'};
  my $nthreads_per_process = $args{'nthreads_per_process'};
  my $force = $args{'force'};

  my ($filnamefna, $folder, $suffix) = fileparse($genomefile);
  if (-e "$folder$shortName.faa" && !$force){
    print STDERR "  $shortName: found previous $annotator annotation, " . 
      "skipping\n";
  } else {
    my $cmd;
    if ($annotator eq 'prokka'){
      $cmd = "prokka --outdir ${folder}prokka --force --prefix $shortName --locustag $prefix --fast --increment 10 --kingdom $kingdom --gcode $gcode --cpus $nthreads_per_process --mincontiglen 200 $genomefile > /dev/null 2> /dev/null";
    } elsif ($annotator eq 'prodigal'){
      make_path("${folder}prodigal") unless (-d "${folder}prodigal");
      $cmd = "prodigal -i $genomefile -c -m -g $gcode -f gbk -a ${folder}prodigal/$shortName.faa -o ${folder}prodigal/$shortName.gbk -q"
    }
    print STDERR "$cmd\n" if $debug;
    system $cmd;
    # Copy file to the parent directory
    copy("$folder$annotator/$shortName.faa", "$folder$shortName.faa");
  }
  return "$folder$shortName.faa";
}


=head2 annotaterRNA

 Title   : annotaterRNA
 Usage   : annotaterRNA('genomefile' => $genomefile,
                        'prefix' => 'SpnMX2',
                        'shortName' => 'Streptococcus_pneumo_MX2',
                        'kingdom'  => 'Bacteria');
 Function: Annotates rRNAs in a genome, using barrnap
 Returns : An array of fasta rRNA file
 Args    : Named parameters 
             genomefile => A fasta genome file
             prefix     => A unique prefix
             shortName  => Short name for the organism
             kingdom    => Domain or kingdom. Default: Bacteria
             nthreads_per_process => Number of threads to use in annotator. 
                                     Default 4.
             force      => Force reannotation of the genome

=cut

sub annotaterRNA {
  my %args = ('genomefile' => undef,
	      'prefix'     => undef,
	      'shortName'  => undef,
	      'kingdom'    => 'Bacteria',
	      'nthreads_per_process' => 4,
	      'force'      => 0,
	      @_);

  # Validate options
  my $genomefile = $args{'genomefile'};
  croak "'genomefile' not set or not existing" 
    unless ($genomefile && -e $genomefile);
  my $prefix = $args{'prefix'};
  croak "No 'prefix' set" unless $prefix;
  my $shortName = $args{'shortName'};
  croak "No 'shortName' set" unless $shortName;
  my $kingdom = $args{'kingdom'};
  my $nthreads_per_process = $args{'nthreads_per_process'};
  my $force = $args{'force'};
  my $debug = $args{'debug'};

  my ($filnamefna, $folder, $suffix) = fileparse($genomefile);
  my %kingdoms = ( 'Bacteria'  => 'bac',
		   'Archaea'   => 'arc',
		   'Eukaryota' => 'euk',
		 );
  my %genes_to_record =  ('16S_rRNA' => 1, 
			  '23S_rRNA' => 1);
  my %molecules;
  my $fileroot = "$folder/$shortName";
  croak "Kingdom $kingdom could not be recognized: should be one of " . 
    join(", ", keys(%kingdoms)) . "\n" unless $kingdoms{$kingdom};
  my @fastas;
  # Check whether both are present, in that case skip running barnnap
  if (-e "$fileroot.16S_rRNA.fasta" && -e "$fileroot.23S_rRNA.fasta" 
      && !$force){
    @fastas = ("$fileroot.16S_rRNA.fasta", "$fileroot.23S_rRNA.fasta");
    print STDERR "  $shortName: both rRNA files present or force\n";
  } else {
    # Run barnnap, open the GFF, parse it, determine the best rRNAs and 
    # open the fasta file to retrieve the actual sequences
    my %rrnas;
    my $cmd = "barrnap --kingdom $kingdoms{$kingdom} --quiet --threads $nthreads_per_process --lencutoff 0.8 --reject 0.5 --evalue 1e-06 $genomefile ";
    print STDERR "  $shortName: running barrnap\n";
    print STDERR "$cmd\n" if $debug;
    open CMD, "-|", $cmd or croak "Cannot run barrnap: $!\n";
    my $gffin = Bio::Tools::GFF->new(-fh => \*CMD, -gff_version => 3);
    while (my $feature = $gffin->next_feature){
      # Check that the feature has a Name and score tag
      unless ($feature->has_tag('Name') && $feature->has_tag('score')){
	print STDERR "  $shortName: WARN: No name/score in barrnap result\n";
	next;
      }
      my ($name) = $feature->get_tag_values('Name');
      my ($score) = $feature->get_tag_values('score');
      next unless $genes_to_record{$name};
      # Now look whether a previous molecule with a better or equal score has
      # been found
      unless ($molecules{$name} && $molecules{$name}{'score'} <= $score){
	$molecules{$name}{'score'} = $score;
	$molecules{$name}{'feature'} = $feature;
      }
    }
    print STDERR "  $shortName: found " . join(", ", keys(%molecules)) . "\n";
    # Now get sequences
    my $rRNA_fastas_r = _retrieverRNA(\%molecules, $genomefile, $shortName, 
				      $fileroot, \%genes_to_record);
    # Return a sensitive result, with "-" instead of undefs
    foreach my $mol (sort keys %genes_to_record){
      my $fasta = '-';
      $fasta = $rRNA_fastas_r->{$mol} if $rRNA_fastas_r->{$mol};
      push @fastas, $fasta;
    }
  }
  return @fastas;
}

## Retrieve actual sequences from contigs
sub _retrieverRNA {
  my ($mol_r, $genomefile, $shortName, $fileroot, $targets_r) = @_;
  my %ids_to_parse;
  my %seqs;
  foreach my $mol (keys %{ $mol_r }){
    #$seqs{$mol} = '-';
    my $feat = $mol_r->{$mol}{'feature'};
    push @{ $ids_to_parse{$feat->seq_id} }, $mol;
  }
  my $fasta_in = Bio::SeqIO->new(-file => $genomefile, 
				 -format => 'fasta');
  while (my $contig = $fasta_in->next_seq){
    if ($ids_to_parse{$contig->id}){
      foreach my $mol (@{ $ids_to_parse{$contig->id} }){
	my $subseq = $contig->trunc($mol_r->{$mol}{'feature'}->start,
				    $mol_r->{$mol}{'feature'}->end);
	$subseq = $subseq->revcom if ($mol_r->{$mol}{'feature'}->strand == -1);
	$subseq->id("$shortName.$mol");
	my $fasta_out = Bio::SeqIO->new(-file => ">$fileroot.$mol.fasta",
					-format => 'fasta');
	$fasta_out->write_seq($subseq);
	$seqs{$mol} = "$fileroot.$mol.fasta";
      }
    }
    # If we've seen all ids, close the fasta file
    last if keys(%seqs) == keys(%{ $targets_r });
  }
  return \%seqs;
}

## generate prefixes
sub _generatePrefix {
  my ($pref_ref, $name, $species, $genus, $old_prefix) = @_;
  # First check if the old prefix is OK, if yes return it, otherwise 
  # get a fresh one (and warn)
  if ($old_prefix && $old_prefix ne '-'){
    if ($pref_ref->{$old_prefix}){
      carp "Prefix $old_prefix for $name is already present. A new one " . 
	"is generated"; 
    } else {
      croak "Existing prefix $old_prefix too long\n" if length($old_prefix) > 6;
      $pref_ref->{$old_prefix}++;
      return $old_prefix;
    }
  }
  # Trim candidatus
  $genus =~ s/Candidatus // if $genus;
  $species =~ s/Candidatus // if $species;
  $name =~ s/Candidatus //;
  # remove unwanted special characters
  $genus =~ s/[^a-zA-Z0-9 ]//g if $genus;
  $species =~ s/[^a-zA-Z0-9 ]//g if $species;
  $name =~ s/[^a-zA-Z0-9 ]//g;
  my $pref;
  # Tries the species first, if it is set
  if ($species && $species ne "NA" && $species ne '-'){
    croak "No space in $species\n" unless $species =~ / /;
    my ($gen, $spe) = split(/ /, $species);
    $gen = ucfirst $gen;
    $spe = ucfirst $spe;
    $pref = substr($gen, 0, 3) . substr($spe, 0, 3);
    my $cnt = 3;
    while ($cnt <= length($spe) && $pref_ref->{$pref}){
      $pref = substr($gen, 0, 3) . substr($spe, 0, 2) . substr($spe, $cnt++, 1);
    }
  }
  # Then tries genus
  elsif ($genus && $genus ne "NA"){
    $pref = substr $genus, 0, 6;
    my $cnt = 6;
    while ($cnt <= length($genus) && $pref_ref->{$pref}){
      $pref = substr($genus, 0, 5) . substr($genus, $cnt++, 1);
    }
    croak "No suitable prefix found for $species $genus $name\n" unless $pref;
  }
  # Finally tries name.
  else {
    my @a = split(" ", $name);
    my $str;
    if ($#a > 2){
      $str = join('', @a[2..$#a]);
    } else {
      $str = $a[-1];
    }
    $pref = substr $str, 0, 6;
  }
  # Constrain the length to 6 characters
  $pref .= 0 x (6 - length($pref));
  # Finally draw random combination of positions along the string until 
  # it works
  my $cnt = 0;
  while ($pref_ref->{$pref}){
    $cnt++;
    croak "No suitable prefix found for $species $genus $name\n" if $cnt > 1000;
    $name =~ s/\W//g;
    my @name = split('', $name);
    my @shuffled_idx = shuffle(0..$#name);
    my @idx = sort @shuffled_idx[0..5];
    $pref = join('', @name[(@idx)]);
  }
  # And checks that the prefix is the right length
  croak "Prefix $pref too long\n" if length($pref) > 6;
  croak "Prefix $pref still exists in previous assemblies\n" 
    if $pref_ref->{$pref};
  $pref_ref->{$pref}++,
  #print STDERR "$pref\n";
  return $pref;
}

1;

