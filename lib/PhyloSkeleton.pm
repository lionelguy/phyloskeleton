=head1 NAME

PhyloSkeleton - A phylogenomic data selection pipeline

=head1 SYNOPSIS



=head1 DESCRIPTION

=head2 Output

=head1 DEPENDENCIES

=head1 AUTHOR

Lionel Guy (L<<lionel.guy@imbim.uu.se>>)

=head1 DATE

26 Sep 2016 21:39:07 CEST

=head1 COPYRIGHT AND LICENSE

Copyright 2016-2018 Lionel Guy

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

# Code begins here:

package PhyloSkeleton;
our $VERSION = '1.1.1';

# Libraries
use strict;
use warnings;
use Carp;

use PhyloSkeleton::TabFiles qw/ newTable readTable addColumn addRow removeColumn removeRow /;
use PhyloSkeleton::Taxa qw/completeNameNCBI shortenName computerizeName/;
use PhyloSkeleton::Taxonomy qw/ hasTaxonomy /;

# Export
require Exporter;
our @ISA = qw/Exporter/;
our @EXPORT_OK = qw/ parseNCBIlist parseNCBIlproks parseJGIlist parseSelectionLevels selectAssemblies checkUserTable setShortName sortTable ref_rank ass_rank ranks version /;

# Defaults

=head2 Variable-returning functions

 Title   : ref_rank, ass_rank, ranks, column_order, version
 Usage   : my %ref_rank = ref_rank(); my @ranks = ranks();
 Function: Return default values for some important variables. 
 Returns : Hashes (ref_rank, ass_rank, column_order) or Array (ranks), 
           or scalar (version).
 Args    : None
 Values  : See below


  %ref_rank: ( 
     'reference genome'      => 1, # NCBI
     'representative genome' => 2,
     'na'                    => 9);
     
  %ass_rank: ( 
     'Complete Genome'       => 1, # NCBI
     'Chromosome'            => 2,
     'Chromosome with gaps'  => 4,
     'Scaffold'              => 5,
     'Contig'                => 6,
     'Finished'              => 3, # JGI
     'Permanent Draft'       => 7,
     'Draft'                 => 8);

  @ranks: qw/superkingdom phylum class order family genus species/;

  %column_order: ('assembly'        => 1,
                  'excluded'        => 2,
                  'selectionGroup'  => 3,
                  'source'          => 4,
                  'taxid'           => 5,
                  'name'            => 6,
                  'organism_groups' => 7,
                  'gcode'           => 9,
                  'superkingdom'    => 10,
                  'phylum'          => 11,
                  'class'           => 12,
                  'order'           => 13,
                  'family'          => 14,
                  'genus'           => 15,
                  'species'         => 16,
                  'taxostring'      => 17,
                  'size'            => 18,
                  'scaffolds'       => 19,
                  'cds'             => 20,
                  'refseq_category' => 21,
                  'assembly_level'  => 22,
                  'biosample'       => 23,
                  'bioproject'      => 24,
                  'wgs'             => 25,
                  'wgs_master'      => 26,
                  'is_public'       => 27,
                  'jgiid'           => 28,
                  'ftp_path'        => 29,
                  'shortName'       => 30,
                  'prefix'          => 31,
                  'genomeFile'      => 32,
                  'proteomeFile'    => 33,
                  'rRNA23S'         => 34,
                  'rRNA16S'         => 35,
                  'completeness'    => 36,
                  );

=cut

sub ref_rank {
  return ('reference genome'      => 1, # NCBI
	  'representative genome' => 2,
	  'na'                    => 9);
}

sub ass_rank {
  return ('Complete Genome'       => 1, # NCBI
	  'Chromosome'            => 2,
	  'Chromosome with gaps'  => 4,
	  'Scaffold'              => 5,
	  'Contig'                => 6,
	  'Finished'              => 3, # JGI
	  'Permanent Draft'       => 7,
	  'Draft'                 => 8);

}

sub ranks {
  return qw/superkingdom phylum class order family genus species/;
}

sub column_order {
  return ('assembly'        => 1,
	  'excluded'        => 2,
	  'selectionGroup'  => 3,
	  'source'          => 4,
	  'taxid'           => 5,
	  'name'            => 6,
	  'organism_groups' => 7,
	  'gcode'           => 9,
	  'superkingdom'    => 10,
	  'phylum'          => 11,
	  'class'           => 12,
	  'order'           => 13,
	  'family'          => 14,
	  'genus'           => 15,
	  'species'         => 16,
	  'taxostring'      => 17,
	  'size'            => 18,
	  'scaffolds'       => 19,
	  'cds'             => 20,
	  'refseq_category' => 21,
	  'assembly_level'  => 22,
	  'biosample'       => 23,
	  'bioproject'      => 24,
	  'wgs'             => 25,
	  'wgs_master'      => 26,
	  'is_public'       => 27,
	  'jgiid'           => 28,
	  'ftp_path'        => 29,
	  'shortName'       => 30,
	  'prefix'          => 31,
	  'genomeFile'      => 32,
	  'proteomeFile'    => 33,
	  'rRNA23S'         => 34,
	  'rRNA16S'         => 35,
	  'completeness'    => 36);
}

sub version {
  return $VERSION;
}

=head2 parseNCBIlist

 Title   : parseNCBIlist
 Usage   : parseNCBIlist('file' => $filename,
                         'gref' => $gref);
 Function: Parses the NCBI assembly list, available e.g. at 
           ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/assembly_summary_genbank.txt
 Returns : A list of genome assemblies (hashref).
 Args    : Named parameters 
             file     => File to parse
             gref     => A hashref, the product of parsed assemblies
                         from NCBI lproks list
             ass_rank => Assembly ranking, a hashref
             ref_rank => Reference ranking, a hashref

 See also: Variable-returning subroutines, which return default values for
          ass_rank and ref_rank.

=cut

sub parseNCBIlist {
  # Read options
  my %args = ( 'file' => undef,
	       'gref' => undef,
               'ref_rank' => { &ref_rank() },
               'ass_rank' => { &ass_rank() },
               @_ );
  my $gref = $args{'gref'};

  # Read file
  print STDERR "Parsing NCBI Genbank assembly list...";
  #my $ftpSite = "ftp://ftp.ncbi.nlm.nih.gov/genomes/all";
  my @columnsToAdd = qw/ bioproject biosample wgs_master refseq_category taxid assembly_level ftp_path /;
  my @columnsIn = ('assembly_accession', 'organism_name', 'infraspecific_name',
		   'isolate', @columnsToAdd);
  my $assref = readTable('file'     => $args{'file'}, 
                         'columns'  => \@columnsIn,
                         'rowname'  => 'assembly_accession',
			 #'quote'    => '"',
                         'skip'     => 1,
                         'titleChar'=> '# ',
			 'lenient'  => 1);

  my $nasstot = $assref->{'nrow'};
  my $nass = 0;
  my $notfound = 0;

  # Prepare columns to add; remove rows that are not present
  my %toAdd;
  foreach my $assembly (@{ $gref->{'rows'} }){
    # report if $assembly in $gref
    if ($assref->{'byrow'}{$assembly}){
      $nass++;
      # Fix the name
      my $name = 
	completeNameNCBI($assref->{'byrow'}{$assembly}{'organism_name'}, 
			 $assref->{'byrow'}{$assembly}{'infraspecific_name'},
			 $assref->{'byrow'}{$assembly}{'isolate'});
      $toAdd{'name'}{$assembly} = $name;

      # Add the other columns 
      foreach my $column (@columnsToAdd){
	$toAdd{$column}{$assembly} = $assref->{'byrow'}{$assembly}{$column}
      }

      # Check that assembly levels validates
      my $assembly_level = $assref->{'byrow'}{$assembly}{'assembly_level'};
      croak "Assembly level $assembly_level not in ass_rank hashref" 
	unless ($args{'ass_rank'}{$assembly_level});
    } else {
      # else delete the row
      $gref = removeRow('table'   => $gref, 
			'rowname' => $assembly);
      $notfound++;
    }
  }

  # Now add columns
  foreach my $col (keys %toAdd){
    $gref = addColumn('table' => $gref,
		      'colname' => $col, 
		      'col' => $toAdd{$col});
  }

  ### Return
  print STDERR " seen $nasstot in total, looked at $nass, removed $notfound ",
    "that were in list but not in assembly summary, in file $args{'file'}\n";
  return sortTable('table' => $gref);
}

=head2 parseNCBIlproks

 Title   : parseNCBIlproks
 Usage   : parseNCBIlproks('file' => $filename);
 Function: Parses a list of genomes as downloaded from http://ncbi.nlm.nih.gov/genome/browse
 Returns : A hashref containing one genome per key
 Args    : Named parameters 
             file   => File, as downloaded
             sep    => Value separator, "," by default
             quote  => Quote character protecting text values, '"' by default

=cut

sub parseNCBIlproks {
  # Options
  my %args = ( 'file' => undef,
               'sep'  => ',',
               'quote'=> '"',
	       'titleChar' => "#",
	       'titleLC' => 1,
	       'removeTrailingSpace' => 1,
               @_);
  croak "No file parameter passed" unless $args{'file'};

  print STDERR "Parsing NCBI selection list...";

  my @columnsIn = qw/ assembly size_mb_ organism_groups cds scaffolds wgs /;


  # Read the table
  my $gref = readTable( 'file'     => $args{'file'},
			'columns'  => \@columnsIn,
			'rowname'  => 'assembly',
                        'sep'      => $args{'sep'},
                        'quote'    => $args{'quote'},
			'titleChar'=> $args{'titleChar'},
			'titleLC'  => $args{'titleLC'},
			'removeTrailingSpace' => $args{'removeTrailingSpace'});
  #open TAB, '<', "$file" or die "Could not open $file: $!";
  my $nass = 0;

  # Add source column, size in nt, and parse replicons
  my %toAdd;
  foreach my $assid (@{ $gref->{'rows'} } ){
    $toAdd{'source'}{$assid} = 'genbank';
    $toAdd{'size'}{$assid} = $gref->{'byrow'}{$assid}{'size_mb_'}*10e6;
    $toAdd{'excluded'}{$assid} = '0_included' 
      unless (defined $gref->{'bycol'}{'excluded'});

    # # Not parsing replicons ATM
    # if ($gref->{'byrow'}{$assid}{'replicons'} =~ /:/){
    #   my @replicons = split(/; /, $gref->{'byrow'}{$assid}{'replicons'});
    #   foreach my $line (@replicons){
    #     chomp $line;
    #     my ($type, $ids) = split(/:/, $line);
    #     my ($refseq, $genbank) = split(/\//, $ids);
    #     $gref->{'byrow'}{$assid}{'replicons'}{$genbank} = $type if $genbank;
    #   }
    # }
  }

  print STDERR " seen $gref->{'nrow'} assemblies in file $args{'file'}\n";
  
  # Add source and size columns
  foreach my $col (keys %toAdd){
    $gref = addColumn('table' => $gref,
		      'colname' => $col, 
		      'col' => $toAdd{$col});
  }
  # Remove the ugly size in Mb
  $gref = removeColumn('table' => $gref, 
		       'colname' => 'size_mb_');

  # Return
  return sortTable('table' => $gref);
}

=head2 parseJGIlist

 Title   : parseJGIlist
 Usage   : parseJGIlist('file' => $filename);
 Function: Parses a genome list from JGI, available at 
         L<https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=FindGenomes&page=genomeSearch>. 
           Search for example by domain (e.g. Bacteria) or phylum. It is then
           possible to display more columns (Table Configuration at the bottom
           of the page) and refine the search. Then export the selection. 
           The following columns are required (in addition to the default ones):
             - NCBI Taxon ID
             - JGI Project ID / ITS PID
             - Is Public
             - Bioproject Accession
             - Biosample Accession
             - Scaffold count
 Returns : A hashref.
 Args    : Named parameters 
             file     => Filename 
             ass_rank => Assembly ranking, as in parseNCBIlist

 See also: parseNCBIlist, mergeLists,

=cut

sub parseJGIlist {
  # Required from printReport: name source taxid bioproject 
  # biosample size nprots ranking replicons wgsMaster ftpPath
  # Then all the ranks are gathered from parseTaxonomy
  # jgiId as assid
  # is public
  my %args = ( 'file' => undef,
               'ass_rank' => { &ass_rank() },
               @_ );
  
  croak "No file parameter passed" unless $args{'file'};

  print STDERR "Parsing JGI assembly selection...";

my @columns = qw/ img_genome_id jgi_project_id___its_sp_id genome_name___sample_name sequencing_status ncbi_taxon_id is_public ncbi_bioproject_accession ncbi_biosample_accession genome_size_____assembled gene_count_____assembled scaffold_count_____assembled /;
my %column_renames = ( 'genome_name___sample_name'    => 'name',
                         'sequencing_status'            => 'assembly_level',
                         'ncbi_taxon_id'                => 'taxid',
                         'jgi_project_id___its_sp_id'   => 'jgiid',
                         'genome_size_____assembled'    => 'size',
                         'gene_count_____assembled'     => 'cds',
                         'img_genome_id'                => 'assembly',
                         'ncbi_bioproject_accession'    => 'bioproject',
                         'ncbi_biosample_accession'     => 'biosample',
                         'scaffold_count_____assembled' => 'scaffolds');
                         
  my $gref = readTable('file'            => $args{'file'},
                       'columns'         => \@columns,
                       'columnNameMap'   => \%column_renames,
                       'rowname'         => 'assembly',
                       'titleLC'         => 1,
                       'removeTrailingSpace' => 1);
  my $nremoved++;
  my %toAdd;
  foreach my $id (@{ $gref->{'rows'}}){
    # Filter out those that have no JGI id (no uid, can't track them)
    unless ($gref->{'byrow'}{$id}{'jgiid'} > 0 ){
      $gref = removeRow('table'   => $gref,
			'rowname' => $id);
      $nremoved++
    } else {
      $toAdd{'source'}{$id} = 'jgi';
      $toAdd{'refseq_category'}{$id} = 'na';
      my $excluded = '0_included';
      $excluded = '1_jgiNotPublic'
	unless ($gref->{'byrow'}{$id}{'is_public'} &&
		$gref->{'byrow'}{$id}{'is_public'} eq 'Yes');
      if (defined $gref->{'bycol'}{'excluded'}){
	$gref->{'byrow'}{$id}{'excluded'} = $excluded
      } else {
	$toAdd{'excluded'}{$id} = $excluded;
      }
      croak "Assembly level $gref->{'byrow'}{$id}{'assembly_level'}" . 
        " not in ass_rank" 
        unless $args{'ass_rank'}{$gref->{'byrow'}{$id}{'assembly_level'}};
    }
  }
  
  # Now add columns
  foreach my $col (keys %toAdd){
    $gref = addColumn('table' => $gref,
		      'colname' => $col, 
		      'col' => $toAdd{$col});
  }

  
  print STDERR " recorded $gref->{'nrow'} in total\n";
  return sortTable('table' => $gref);
}

=head2 parseSelectionLevels

 Title   : parseSelectionLevels
 Usage   : parseSelectionLevels('file' => $filename);
           parseSelectionLevels('ingroupName'            => 'Legionellales',
                                'ingroupLevel'           => 'order',
                                'ingroupSelectionLevel'  => 'genus',
                                'outgroupSelectionLevel' => 'family');
 Function: Parse a tab file that contains selection levels for various groups.
           A simplified version works by choosing only an ingroup and an 
           outgroup. The second version is equivalent to the first one if
           $filename refers to a file with the following content:

             #group         groupLevel   selectionLevel
             Legionellales  order        genus
             other          -            family

 Returns : A hashref
 Args    : Named parameters 
             file                   => File containing the selection levels
                                       See README for more information
             ingroupName            => The name of the ingroup
             ingroupLevel           => The level of the ingroup (there must
                                       a cell with 'ingroupName' in the 
                                       'ingroupLevel' column)
             ingroupSelectionLevel  => Level at which to sample inside
                                       'ingroupName'
             outgroupSelectionLevel => Level at which to sample outside 
                                       'ingroupName'

=cut

# Parse a tab file that contains selection levels for various groups, returns
# a hash by ref
sub parseSelectionLevels {
  my %args = ('file'                   => undef, 
	      'ingroupName'            => undef,
	      'ingroupLevel'           => undef,
	      'ingroupSelectionLevel'  => 'genus',
	      'outgroupSelectionLevel' => 'family',
	      'ranks'                  => [ &ranks() ],
	      @_);

  # Validate options
  croak "file or (ingroup name and ingroupLevel) must be defined" 
    unless ($args{'file'} || ($args{'ingroupName'} && $args{'ingroupLevel'}));

  my @columns = qw/ group groupLevel selectionLevel/;

  my $selLevels = newTable('columns' => \@columns);
  my $defaultSelLev = $args{'outgroupSelectionLevel'};

  # Read the file if there is one
  if ($args{'file'}){
    $selLevels = readTable('file'      => $args{'file'},
			   'title'     => 1,
			   'titleChar' => '#',
			   'columns'   => \@columns,
			   'rowname'   => 'group');

    # Validate
    # Make sure all columns are there
    foreach my $col (@columns) {
      croak "Column $col not present in table $args{'file'}" 
	unless $selLevels->{'bycol'}{$col};
    }
    foreach my $group (@{ $selLevels->{'rows'} }){
      # Make sure all elements are defined and that the group rank
      # and selection rank are in our scope ('ranks') or is 'name'
      croak "Selection rank $selLevels->{'byrow'}{$group}{'selectionLevel'} " .
	"not in rank array\n" 
	  unless (grep { $_ eq $selLevels->{'byrow'}{$group}{'selectionLevel'} }
		  (@{ $args{'ranks'} }, 'name'));
      if ($group eq 'default' || $group eq 'other'){
	  if ($group eq 'default'){
	      my %defRow;
	      foreach my $col (@{ $selLevels->{ 'columns' } }){
		  $defRow{$col} = $selLevels->{'byrow'}{'default'}{$col};
	      }
	      $defRow{'group'} = 'other';
	      $selLevels = removeRow('table' => $selLevels,
				     'rowname' => 'default');
	      $selLevels = addRow('table' => $selLevels,
				  'row' => \%defRow)
	  }
	  $defaultSelLev = $selLevels->{'byrow'}{'other'}{'selectionLevel'};

      } else {
	  croak "Group rank $selLevels->{'byrow'}{$group}{'groupLevel'} " . 
	      "not in rank array\n"
	      unless (grep { $_ eq $selLevels->{'byrow'}{$group}{'groupLevel'} } 
		      @{ $args{'ranks'} });
      }
    }
  } else {
      # If no file is defined, just define two groups
      my %row = ( 'group' => $args{'ingroupName'},
		'groupLevel' => $args{'ingroupLevel'},
		'selectionLevel' => $args{'ingroupSelectionLevel'});
      $selLevels = addRow('table' => $selLevels, 
			  'row' => \%row);
  }
  # Add default if necessary
  unless ($selLevels->{'byrow'}{'other'} || $selLevels->{'byrow'}{'default'}){
    my %row = ('group' => 'other', 'groupLevel' => '-', 
	       'selectionLevel' => $defaultSelLev);
    $selLevels = addRow('table' => $selLevels,
			'row' => \%row);
  }

  # Communicate:
  print STDERR "Parsing selection groups and levels.\n",
    "  For each of the following groups (rank), selects at:\n";
  foreach my $key (@{ $selLevels->{'rows'} }){
    print STDERR "    $key ($selLevels->{'byrow'}{$key}{'groupLevel'}) -> ", 
      "$selLevels->{'byrow'}{$key}{'selectionLevel'}\n";
  }

  # Return
  return $selLevels;
}

=head2 selectAssemblies

 Title   : selectAssemblies
 Usage   : selectAssemblies('genomes'         => $gref,
                            'selectionLevels' => $sref,
                            'taxonomy'        => $tref);
 Function: From a list of assemblies and corresponding taxonomy, find
           appropriate assemblies at each selection level. This is the
           core of phyloSkeleton.
           First get the 'reference genome' and then 'representative genome'
           from 'refseq_category'. Sorting among the assemblies that come at the  
           same time, i.e. same status and same genus: 
             1. reference and representative genomes  (see hash ref_rank)
             2. level of completion (see hash ass_level)
             3. size of the genome
             4. alphabetically
 Returns : A href to the updated genome table and a href to the sampled 
           categories and the corresponding assembly ids.
 Args    : Named parameters 
             genomes         => A href containing assemblies, as returned by
                                parse{NCBI/JGI}list.
             selectionLevels => A href to selection levels as returned by
                                parseSelectionLevels
             taxonomy        => A hashref containing taxonomy info, as output
                                by parseTaxonomy.

  See also: parseNCBIlist, parseNCBIlproks, parseJGIlist, parseTaxonomy,


=cut

sub selectAssemblies {
  my %args = ( 'genomes'         => undef,
               'selectionLevels' => undef,
               'taxonomy'        => undef,
               'ref_rank'        => { &ref_rank },
               'ass_rank'        => { &ass_rank },
               'ranks'           => [ &ranks ],
               @_);

  # Validate
  croak "No genomes parameter passed" unless $args{'genomes'};
  croak "No selectionLevels parameter passed" unless $args{'selectionLevels'};
  croak "No taxonomy parameter passed" unless $args{'taxonomy'};
 
  my $gref = $args{'genomes'};
  my $selLevRef = $args{'selectionLevels'};
  my %ref_rank = %{ $args{'ref_rank'} };
  my %ass_rank = %{ $args{'ass_rank'} };
  my @ranks = @{ $args{'ranks'} };
  
  print STDERR "Selecting representative assemblies...\n";
  my %sampledTaxa;
  my %samplingGroups;
  my %selLevelCount;
  my $totalSelected;
  # Sort keys for reproducibility; loop over assemblies
  foreach my $assid (@{ $gref->{'rows'} }){
    my $taxid = $gref->{'byrow'}{$assid}{'taxid'};
    croak "No taxid found for $assid" unless $taxid;
    # Find the correct sampling rank for this assembly
    my ($selectionTaxon, $samplingGroup, $samplingRank) = 
      PhyloSkeleton::Taxonomy::_findSamplingLevel(
        'taxid'           => $taxid, 
        'selectionLevels' => $selLevRef, 
        'taxonomy'        => $args{'taxonomy'},
	'ranks'           => \@ranks);
    my $isAdded;
    # Add the selection group to gref
    $samplingGroups{$assid} = $samplingGroup;
    # Have I seen this taxon before? Yes: is the previous better ranked?
    # No: record it.
    if ($sampledTaxa{$selectionTaxon}){
      my $prevAssid = $sampledTaxa{$selectionTaxon};
      # Here is the sorting done: favor reference level if set,
      # otherwise go for assembly level
      my $ref_rank = $ref_rank{$gref->{'byrow'}{$assid}{'refseq_category'}};
      my $prev_ref_rank = 
        $ref_rank{$gref->{'byrow'}{$prevAssid}{'refseq_category'}};
      my $ass_level = $ass_rank{$gref->{'byrow'}{$assid}{'assembly_level'}};
      my $prev_ass_level = 
        $ass_rank{$gref->{'byrow'}{$prevAssid}{'assembly_level'}};
      if ($ref_rank < $prev_ref_rank){ # Does current id have better status?
	      $sampledTaxa{$selectionTaxon} = $assid;
      } elsif ($ref_rank == $prev_ref_rank){ # or the same?
	      if ($ass_level < $prev_ass_level){ # Does it have a better assembly
	        $sampledTaxa{$selectionTaxon} = $assid;
	    } elsif ($ass_level == $prev_ass_level) { # or the same
	      if ($gref->{'byrow'}{$assid}{'size'} > 
		  $gref->{'byrow'}{$prevAssid}{'size'}){ # bigger?
	        $sampledTaxa{$selectionTaxon} = $assid;
	  } # else do nothing
	} # else do nothing
      } # else do nothing
    } else { # Haven't seen it, record
      $sampledTaxa{$selectionTaxon} = $assid;
      $isAdded++;
    }
    # Count
    if ($isAdded){
      $selLevelCount{$samplingGroup}++;
      $totalSelected++;
    }
  }

  # Add samplingGroups column
  $gref = addColumn('table'   => $gref,
		    'col'     => \%samplingGroups,
		    'colname' => 'selectionGroup');

  # Add excluded column or annoate existing
  my %excluded = ();
  foreach my $assid (@{ $gref->{'rows'} }){
    my $excluded = '1_select';
    $excluded = '0_included' if (grep(/^$assid$/, values %sampledTaxa));
    if (defined $gref->{'bycol'}{'excluded'}){
      $gref->{'byrow'}{$assid}{'excluded'} = $excluded;
    } else {
      $excluded{$assid} = $excluded;
    }
  }
  if (%excluded){
    $gref = addColumn('table'  => $gref,
		      'col'    => \%excluded,
		      'colname'=> 'excluded');
  }

  # Print report
  print STDERR "  selected $totalSelected assemblies in the following groups\n";
  foreach my $selLevel (sort @{ $selLevRef->{'rows'} }){
    my $count = $selLevelCount{$selLevel}; 
    $count = 0 unless $count;
    print STDERR "    $selLevel (selection at ", 
      $selLevRef->{'byrow'}{$selLevel}{'selectionLevel'}, " level): $count\n";
  }

  # Return
  return sortTable('table' => $gref);
}

=head2 setShortName

 Title   : setShortName
 Usage   : setShortName('gref' => $gref)
 Function: Sets a computer-friendly, unique shortName
 Returns : An updated gref
 Args    : Named parameters 
             gref   => A genome table hashref


=cut

sub setShortName {
  my %args = ('gref'    => undef,
	      'maxchar' => 30,
	      @_);
  croak "No 'gref' argument set" unless $args{'gref'};
  my $gref = $args{'gref'};
  my $maxchar= $args{'maxchar'};

  my %shortNames;
  my %seen;

  # Has the gref a shortname column?
  my $hasShortName = defined($gref->{'bycol'}{'shortName'});

  foreach my $id (@{ $gref->{'rows'} }){
    # If shortName is already set, keep it.
    next if ($hasShortName && $gref->{'byrow'}{$id}{'shortName'} ne '-');
    # Else get the name
    my $name = $gref->{'byrow'}{$id}{'name'};
    croak "No 'name' column in 'gref' for row $id" unless $name;
    $name = computerizeName($name);
    # Else if not selected, do not attribute a short name
    my $shortName = '-';
    if ($gref->{'byrow'}{$id}{'excluded'} =~ /^0_/ ){
      $shortName = shortenName($name, $args{'maxchar'});
      if ($seen{$shortName}){
	#print STDERR "$shortName\n";
	my $newShortName = $shortName;
	foreach my $i (1..99999){
	  my $nd = length($i);
	  if (length($newShortName) >= ($maxchar-$nd-1)){
	    $newShortName = substr($newShortName, 0, $maxchar-$nd);
	  }
	  $newShortName .= sprintf("_%0${nd}d", $i);
	  last unless ($seen{$newShortName});
	}
	$shortName = $newShortName;
      }
      # Just to make sure that we've sorted out that
      croak "Could not find a unique short name" if ($seen{$shortName});
      $seen{$shortName}++;
    }
    if ($hasShortName){
      $gref->{'byrow'}{$id}{'shortName'} = $shortName
    } else {
      $shortNames{$id} = $shortName;
    }
  }
  unless ($hasShortName){
    $gref = addColumn('table'   => $gref,
		      'colname' => 'shortName',
		      'col'     => \%shortNames);
  }
  return sortTable('table' => $gref);
}

=head2 checkUserTable

 Title   : checkUserTable
 Usage   : checkUserTable('table' => $userGref);
 Function: Looks that all rows have enough information to go further in the 
           pipeline.
           Necessary columns in the table: assembly, source, name
           For data, four solutions:
            - a proteome file
            - a genome file with either taxid or gcode and superkingdom
            - if source is jgi, the assembly id is enough
            - if source is genbank, the assembly id and a ftpPath
 Returns : A hashref to the modified table, and three booleans: whether 
           taxonomy, data gathering and annoation are necessary.
 Args    : Named parameters
             table   => table to check

 See also:

=cut

sub checkUserTable {
  my %args = ('table' => undef,
	      'ranks' => [ ranks() ],
	      'annotate_rrnas' => 0,
	      @_);

  # Arguments
  my $gref = $args{'table'};
  my $annotate_rrnas = $args{'annotate_rrnas'};
  croak "No 'table' argument" unless $gref;

  my %neededCols = ('assembly' => 1,
		    'source'   => 1, 
		    'name'     => 1);
  
  # Variables
  my ($taxoNeeded, $gatherNeeded, $annotNeeded);
  my %toAdd;

  # Go through assemblies
  foreach my $id (@{ $gref->{'rows'} }){
    # Test whether required columns are present
    foreach my $col (keys %neededCols){
      croak "User table does not contain column $col for row $id" 
	unless $gref->{'byrow'}{$id}{$col};
      croak "User table has value '-' for column $col for row $id" 
	if $gref->{'byrow'}{$id}{$col} eq '-';
    }
    # Test if it genomic and/or proteomic data is present or is obtainable
    my $proteomeFile = $gref->{'byrow'}{$id}{'proteomeFile'};
    my $genomeFile =   $gref->{'byrow'}{$id}{'genomeFile'};
    my $source =       $gref->{'byrow'}{$id}{'source'};
    my $ftpPath =      $gref->{'byrow'}{$id}{'ftp_path'};
    my $taxid =        $gref->{'byrow'}{$id}{'taxid'};
    my $gcode =        $gref->{'byrow'}{$id}{'gcode'};
    my $superkingdom = $gref->{'byrow'}{$id}{'superkingdom'};
    my $excluded =     $gref->{'byrow'}{$id}{'excluded'};
    
    # Test several options
    my $ok;
    # 0. It is excluded by the user, do nothing
    if ($excluded && !($excluded =~ /^0_/)){
      # Do nothing
    }
    # 1. We have a proteome or a genome file, do we need more ?
    elsif (($proteomeFile && $proteomeFile ne '-') || 
	   ($genomeFile && $genomeFile ne '-')){
      # Do we need genetic code ?
      if (!($proteomeFile && $proteomeFile ne '-') || $annotate_rrnas){
	unless (($taxid && $taxid ne '-') || ($gcode && $gcode ne '-')) {
	  carp "Assembly $id has no genetic code set. Using default (11).";
	  if ($gref->{'bycol'}{'gcode'}){
	    $gref->{'byrow'}{$id}{'gcode'} = 11;
	  } else {
	    $toAdd{'gcode'}{$id} = 11;
	  }
	}
	unless (($taxid && $taxid ne '-') || 
		($superkingdom && $superkingdom ne '-')){
	  carp "Assembly $id has no superkingdom (domain) set. Using Bacteria.";
	  if ($gref->{'bycol'}{'superkingdom'}){
	    $gref->{'byrow'}{$id}{'superkingdom'} = 'Bacteria';
	  } else {
	    $toAdd{'superkingdom'}{$id} = 'Bacteria';
	  }
	}
      }
      $ok++;
      # 1b: We'll need to annotate unless we have a proteomeFile
      $annotNeeded++ unless ($proteomeFile && $proteomeFile ne '-');
    }
    # 2. We have no genomic information, can it be retrieved?
    elsif ($source && (($source eq 'genbank' && $ftpPath && $ftpPath ne '-') ||
		       $source eq 'jgi')) {
      $ok++;
      $gatherNeeded++;
      $annotNeeded++;
    }
    # Are we OK? If so, include the assembly in the selection, otherwise exclude
    #if ($excluded && $excluded
    if ($excluded && $excluded =~ /^0_/){
      $gref->{'byrow'}{$id}{'excluded'} = 
	$ok ? '0_included' : '9_notEnoughInfo';
    } elsif (!$excluded){
      $toAdd{'excluded'}{$id} = $ok ? '0_included' : '9_notEnoughInfo';
    }
    # Do we want to add taxonomy info?
    if ($taxid && $taxid ne '-' && !hasTaxonomy('table' => $gref, 
						'id' => $id, 
						'ranks' => $args{'ranks'},
						'any' => 1)){
      $taxoNeeded++;
    }
  }
  
  # Make sure we have values for everyone
      foreach my $id (@{ $gref->{'rows'} }){
    # Now before leaving check that we add uniform information
    foreach my $key (keys %toAdd){
      $toAdd{$key}{$id} = '-' unless $toAdd{$key}{$id};
    }
  }

  # Add columns if necessary
  foreach my $key (keys %toAdd){
    $gref = addColumn('table'   => $gref,
		      'colname' => $key,
		      'col'     => $toAdd{$key});
  }

  # Return
  return (sortTable('table' => $gref), 
	  $taxoNeeded, $gatherNeeded, $annotNeeded);
}


=head2 sortTable

 Title   : sortTable
 Usage   : sortTable('table'  => $hashref,
                    'columns' => \%column_order,
                    'rows'    => 1);

 Function: Sorts table, both columns according to column ranking,
           and rows, according to a standard sorting: first by 'excluded',
           then by 'selectionGroup' and finally by taxostring. If none of the 
           latter two are present, sort by assembly id.
 Returns : Hashref to the sorted table.
 Args    : Named parameters 
             table   => Hashref to the assembly table.   
             columns => Hashref to a hash with column names as keys and
                        rank as values, or 0 if no sorting. Default is 
                        a default sorting scheme, accessible by 
                        PhyloSkeleton::column_sortinge
             rows    => Should rows be sorted? 0 or 1 (default)

=cut

sub sortTable {
  my %args = ('table'    => undef,
	      'columns' => { &column_order() },
	      'rows'    => 1,
	      @_);
  my %order = %{ $args{'columns'} };
  my $gref = $args{'table'};
  croak "No table provided" unless $gref;
  
    # Sorting algorithms
  local *ExcSelTaxo = sub { $gref->{'byrow'}{$a}{'excluded'} cmp 
			      $gref->{'byrow'}{$b}{'excluded'} or 
				$gref->{'byrow'}{$a}{'selectionGroup'} cmp 
				  $gref->{'byrow'}{$b}{'selectionGroup'} or 
				    $gref->{'byrow'}{$a}{'taxostring'} cmp 
				      $gref->{'byrow'}{$b}{'taxostring'} };
  local *ExcTaxo = sub { $gref->{'byrow'}{$a}{'excluded'} cmp 
			   $gref->{'byrow'}{$b}{'excluded'} or 
			     $gref->{'byrow'}{$a}{'taxostring'} cmp 
			       $gref->{'byrow'}{$b}{'taxostring'} };
  local *ExcSel = sub { $gref->{'byrow'}{$a}{'excluded'} cmp 
			  $gref->{'byrow'}{$b}{'excluded'} or 
			    $gref->{'byrow'}{$a}{'selectionGroup'} cmp 
			      $gref->{'byrow'}{$b}{'selectionGroup'}};
  local *ExcAss = sub { $gref->{'byrow'}{$a}{'excluded'} cmp 
			  $gref->{'byrow'}{$b}{'excluded'} or 
			    $gref->{'byrow'}{$a}{'assembly'} cmp 
			      $gref->{'byrow'}{$b}{'assembly'} } ;
  
  # Sort rows
  if ($args{'rows'}){
    my @ids = @{ $gref->{'rows'} };
    my @sortedIds = @ids;
    if ($gref->{'bycol'}{'excluded'}){ 
      if ($gref->{'bycol'}{'selectionGroup'}){
        if ($gref->{'bycol'}{'taxostring'}){
          @sortedIds = sort ExcSelTaxo @ids;
        } else {
          @sortedIds = sort ExcSel @ids;
        }
      } else {
        if ($gref->{'bycol'}{'taxostring'}){
          @sortedIds = sort ExcTaxo @ids;
        } else {
          @sortedIds = sort ExcAss @ids;
        }
      }
    } else {
      carp "Assembly table has no 'excluded' column, skipping row sorting"
    }
    @{ $gref->{'rows'} } = @sortedIds;
    confess "Not the same amount of rows before and after sorting" 
      unless (@ids == @{ $gref->{'rows'} });
  }
  
  # Sort columns
  if ($args{'columns'}){
    my @cols = @{ $gref->{'columns'} };
    # Find and rank columns present in the order hash
    my @rankedCols = sort { $order{$a} <=> $order{$b} } 
      grep { $order{$_} } @cols;
    # Get the rest of columns in the order they came
    foreach my $col (@cols){
      push @rankedCols, $col unless $order{$col};
    }
    # Make sure we have the same number of cols
    @{ $gref->{'columns'} } = @rankedCols;
    confess "Not the same amount of columns before and after sorting" 
      unless (@cols == @{ $gref->{'columns'} });
  }
  
  # Return
  return $gref;

}

1;
